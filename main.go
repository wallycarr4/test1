package main

import (
	// "bytes"
	// "crypto/tls"
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"unicode"

	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"

	// "reflect"
	"strconv"
	"strings"
	"time"

	//"github.com/go-vgo/robotgo"

	// "image"

	"image"
	"image/png"
	// "bufio"
)

func databaseQuery(w http.ResponseWriter, req *http.Request) {

	fmt.Println("hi")

	var databaseQuery DatabaseQuery

	// fmt.Println(req)
	err := json.NewDecoder(req.Body).Decode(&databaseQuery)
	check(err)

	// fmt.Println(databaseQuery)
	requestType := databaseQuery.RequestType
	// data := databaseQuery.Data
	// range1 := databaseQuery.Range1
	// range2 := databaseQuery.Range2

	// fmt.Println(requestType)
	// fmt.Println(data)

	if requestType == "fuvrFix" {
		// fmt.Println("callbackReadCSVPixels inside")
		response := "1"
		// callbackReadCSVPixels()
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

	if requestType == "selectMemory" {
		dataList := databaseQuery.Data
		fmt.Println("memory")
		fmt.Println("dataList")
		fmt.Println(dataList)
		response := "empty"
		// listMemory := selectAllMemory()
		// if len(listMemory) != 0 {
		// 	response = "entries"
		// }
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

	if requestType == "callbackReadCSVPixels" {
		fmt.Println("callbackReadCSVPixels inside")
		response := "empty"
		callbackReadCSVPixels()
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

	if requestType == "callbackReadCSVBlackArrow" {
		fmt.Println("callbackReadCSVBlackArrow inside")
		response := ""
		callbackReadCSVBlackArrow()
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

	if requestType == "renameFile" {
		//fmt.Println("callbackPrice inside")
		//response := ""
		dataList := databaseQuery.Data
		// fmt.Println("memory")
		fmt.Println("dataList")
		fmt.Println(dataList)

		fileName := renameFile(dataList[0])
		//phase2PriceSearch()
		fileName = fileName //+","+ dataList[1]
		js, err := json.Marshal(fileName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

	if requestType == "callbackPrice" {
		//fmt.Println("callbackPrice inside")
		response := ""
		// dataList := databaseQuery.Data
		// fmt.Println("memory")
		// fmt.Println("dataList")
		// fmt.Println(dataList)
		// phase2PriceSearch()

		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

	if requestType == "callSnip" {
		fmt.Println("callSnip inside")
		response := ""
		callSnip()
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
	//#IR API

	//#IR API end
}
func check(e error) {
	if e != nil {
		panic(e)
	}
}
func readFile(fileName string) string {
	contents, _ := ioutil.ReadFile(fileName)
	stringValue := string(contents)
	return stringValue
}

func handleRequests() {
	http.HandleFunc("/databaseQuery", databaseQuery)
	log.Fatal(http.ListenAndServe(":12000", nil))
}

func readCSVOfPixelsReturningString() string {
	fileString := readFile("/Users/CommanderCarr/PycharmProjects/lily_vr/csvRead.txt")
	return fileString
}

func readCSVOfPixelStringNonColorSpecific() string {
	fileString := readFile("/Users/CommanderCarr/PycharmProjects/lily_vr/csvRead1.txt")
	return fileString
}

func readCSVOfPixelStringBlackArrow() string {
	fileString := readFile("/Users/CommanderCarr/PycharmProjects/fuvr_py/csvReadBlackArrowPrice.txt")
	return fileString
}
func readCSVOfPixelStringByFileName(fileName string) string {
	//fileString := readFile("/Users/CommanderCarr/PycharmProjects/fuvr_py/"+fileName)
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr_py/translatedPictures/` + fileName + `.txt`)
	//fileString := readFile(`C:/test.txt`)
	//fmt.Println(fileString)
	return fileString
}

func readCSVOfPixelStringByFileName1(fileName string) string {
	//fileString := readFile("/Users/CommanderCarr/PycharmProjects/fuvr_py/"+fileName)
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr_py/translatedPictures1/` + fileName + `.txt`)
	//fmt.Println(fileString)
	return fileString
}

func readCSVOfPixelStringByFileName2(fileName string) string {
	//fileString := readFile("/Users/CommanderCarr/PycharmProjects/fuvr_py/"+fileName)
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr_py/translatedPictures2/` + fileName + `.txt`)
	//fmt.Println(fileString)
	return fileString
}

func readCSVOfPixelStringByFileName3(fileName string) string {
	//fileString := readFile("/Users/CommanderCarr/PycharmProjects/fuvr_py/"+fileName)
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr_py/translatedPictures3/` + fileName + `.txt`)
	//fmt.Println(fileString)
	return fileString
}

//model
type DatabaseQuery struct {
	RequestType string
	Data        []string
	Range1      string
	Range2      string
}
type PixelObject struct {
	XValue                   string
	YValue                   string
	RedValue                 string
	GreenValue               string
	BlueValue                string
	ColorationIdentification string
}
type StructureObject struct {
	Classification  string
	ListDimensions  []string
	ListPixelObject []PixelObject
}

type FeatureSetIdentified struct {
	TypeCharacteristic string
	ListPixelObject    []PixelObject
}

type ContinuumObject struct {
	TypeCharacteristic       string
	ListFeatureSetIdentified []FeatureSetIdentified
}

type ConsiderationContinuumObject struct {
	TypeCharacteristic         string
	SoughtFeatureSetIdentified FeatureSetIdentified
	IsXDirectionPresent        bool
	IsYDirectionPresent        bool
}

type Character struct {
	HitPoints int
	Name      string
	xValue    int
	yValue    int
	zValue    int
}
type Character1 struct {
	Dialogue  string
	HitPoints int
	Name      string
	xValue    int
	yValue    int
	zValue    int
}
type Character2 struct {
	CutSequence string
	HitPoints   int
	Name        string
	xValue      int
	yValue      int
	zValue      int
}

func forLooptest1() {
	fmt.Println("hit")
	time.Sleep(time.Duration(5) * time.Second)
	forLooptest1()
}

var isTestLoop2 = true

func forLooptest2() {
	for isTestLoop2 {
		fmt.Println("hit2")
		time.Sleep(time.Duration(5) * time.Second)
	}
}
func doFuvrRestart() {
	// robotgo.MoveMouse(840, 100)
	// robotgo.Click()

	// time.Sleep(time.Duration(2) * time.Second)
	// robotgo.TypeStr("go run main.go")
	// robotgo.KeyTap("enter")
}

var isTradeWindowOpen = false

func main() {
	// go handleRequests()
	fmt.Println("Init")
	// boughtPriceSecondChain := strconv.FormatFloat(2.23, 'E', -1, 64)
	// boughtPriceSecondChain := fmt.Sprintf("%.2f", 12.3456)
	// fmt.Println(boughtPriceSecondChain)
	// handleOnIsTradeWindowOpen()
	// checkPullIsSecondChainTrade()
	// setPullIsSecondChainTradeValuesFromCache()
	// setPullIsThirdChainTradeValuesFromCache()
	// isInDayRangeDoLesserTrade()

	// checkIsConnectTime()
	// doOperationFUVRHead()

	// 	i++
	// }

	// doSectorHandling()
	// doFuvrRestart()
	// doCandleReadVR()
	// parseIntoVRCandleObjects()
	// calculateIsBlackLineInRange()
	// doCalculateTrend()

	// dayOfWeek := getDayOfWeek()
	// fmt.Println("dayOfWeek")
	// fmt.Println(dayOfWeek)

	parseQuestionsIntoObjects()
	createTest()
	// for i, v := range listQuestionObject {
	// 	fmt.Println("question")
	// 	fmt.Println(v.QuestionPart)

	// 	fmt.Println(v.AnswerObjectList)
	// 	if i == 4 {
	// 		break
	// 	}
	// 	i++
	// }
	// fmt.Println()
	// createTest()

	// fmt.Println("hey"[0])

	fmt.Scanln()
	fmt.Println("done")

	// fmt.Scanln()
	// fmt.Println("cool")

	// robotgo.MoveMouse(140, 780)
	// robotgo.Click()
}

type AnswerObject struct {
	//part where A. and question
	//then to have these seperated and translated,
	//where answer is A. but identified in question as answer 0
	//care taken cause can cause faults
	Letter        string
	StringAnswer  string
	AssignedValue string
	IsCorrect     bool
}
type QuestionObject struct {
	QuestionPart     string
	AnswerObjectList []AnswerObject
	ListCorrect      []string
}

var listQuestionObject = []QuestionObject{}

func parseQuestionsIntoObjects() {
	//read text
	fileString := readFile(`./test.txt`)
	listResults0 := strings.Split(fileString, "QUESTION")
	//handle on question part
	// fmt.Println(len(listResults0))

	listPossibleMatches := []string{}
	listPossibleMatches = append(listPossibleMatches, "A.")
	listPossibleMatches = append(listPossibleMatches, "B.")
	listPossibleMatches = append(listPossibleMatches, "C.")
	listPossibleMatches = append(listPossibleMatches, "D.")
	listPossibleMatches = append(listPossibleMatches, "E.")
	listPossibleMatches = append(listPossibleMatches, "F.")
	listPossibleMatches = append(listPossibleMatches, "G.")
	listPossibleMatches = append(listPossibleMatches, "H.")
	listPossibleMatches = append(listPossibleMatches, "I.")
	listPossibleMatches = append(listPossibleMatches, "J.")
	listPossibleMatches = append(listPossibleMatches, "K.")

	for i, v := range listResults0 {
		listAnswerObject := []AnswerObject{}
		if i != 0 {
			questionObject := QuestionObject{}
			//question
			valueList := strings.Split(v, "A.")
			questionObject.QuestionPart = valueList[0]

			//correct answer list
			// correctAnswerList := []string{}

			//getting
			valueList1Period := strings.Split(v, "\n")
			stringToAppend := ""

			for i1, v1 := range valueList1Period {
				result := []rune(v1) // convert string to runes
				firstCharacter := string(result[0:2])
				for i3, v3 := range listPossibleMatches {
					if firstCharacter == v3 {
						answerObject := AnswerObject{}
						answerObject.Letter = strings.Split(v1, ".")[0]
						answerObject.StringAnswer = strings.Split(v1, firstCharacter)[1]
						listAnswerObject = append(listAnswerObject, answerObject)
					}
					i3++
				}
				stringToAppend += v1

				//if correct answer line
				//Correct Answer:
				if strings.Contains(v1, "Correct Answer:") {
					correctAnswerSplit := strings.Split(v1, "Correct Answer:")[1]
					correctAnswerSplit1 := strings.Split(correctAnswerSplit, "Section:")[0]

					stringSeeking := SpaceMap(correctAnswerSplit1)

					index := 0
					if len(stringSeeking) != 0 {
						for index < len(stringSeeking) {
							// fmt.Println(stringSeeking)
							// fmt.Println(len(stringSeeking))
							// fmt.Println(index)
							questionObject.ListCorrect = append(questionObject.ListCorrect, string(stringSeeking[index]))
							index++
						}
					}
				}
				i1++
			}
			questionObject.AnswerObjectList = listAnswerObject

			// fmt.Println("check")
			// for i3, v3 := range listAnswerObject {
			// 	fmt.Println()
			// 	fmt.Println(v3.Letter)
			// 	fmt.Println(v3.StringAnswer)
			// 	i3++
			// }
			// break
			listQuestionObject = append(listQuestionObject, questionObject)
		}
		// if i == 15 {
		// 	for iw, vw := range listQuestionObject[0].ListCorrect {
		// 		fmt.Println(vw)
		// 		iw++
		// 	}
		// 	break
		// }
		i++
	}
}

func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)
}

func IsUpper(s string) bool {
	for _, r := range s {
		if !unicode.IsUpper(r) && unicode.IsLetter(r) {
			return false
		}
	}
	return true
}

func IsLower(s string) bool {
	for _, r := range s {
		if !unicode.IsLower(r) && unicode.IsLetter(r) {
			return false
		}
	}
	return true
}
func createTest() {
	listValues := []string{}
	listValues = append(listValues, "A")
	listValues = append(listValues, "B")
	listValues = append(listValues, "C")
	listValues = append(listValues, "D")
	listValues = append(listValues, "E")
	listValues = append(listValues, "F")
	listValues = append(listValues, "G")
	listValues = append(listValues, "H")
	listValues = append(listValues, "I")
	listValues = append(listValues, "J")
	listValues = append(listValues, "K")
	for i1, v := range listQuestionObject {

		//set correct answers
		for i2, v2 := range v.AnswerObjectList {
			for i3, v3 := range v.ListCorrect {
				if v2.Letter == v3 {
					listQuestionObject[i1].AnswerObjectList[i2].IsCorrect = true
				}
				i3++
			}
			//
			i2++
		}
		rand.Seed(time.Now().UnixNano())

		rand.Shuffle(len(v.AnswerObjectList), func(i, j int) {
			v.AnswerObjectList[i], v.AnswerObjectList[j] = v.AnswerObjectList[j], v.AnswerObjectList[i]
		})
		// fmt.Println(listValues)
		for index2, vAnswer := range v.AnswerObjectList {
			listQuestionObject[i1].AnswerObjectList[index2] = vAnswer
			listQuestionObject[i1].AnswerObjectList[index2].AssignedValue = listValues[index2]
			index2++
		}
		fmt.Println(v.QuestionPart)
		for index3, vAnswer1 := range v.AnswerObjectList {
			stringToPrint := vAnswer1.AssignedValue+" "+vAnswer1.StringAnswer
			fmt.Println(stringToPrint)
			index3++
		}

		fmt.Scanln()
		for iAnswer, vAnswer := range v.AnswerObjectList {
			if vAnswer.IsCorrect {
				fmt.Println(vAnswer.AssignedValue)
			}
			iAnswer++
		}

		fmt.Scanln()

		i1++
	}
}

var isOperateSectorBlotTestBuyReview = true

var isOperateSectorAnalytics = true
var isOperateSectorDataStore = true
var isOperateSectorBlotTests = true
var isBoughtAtSector = false

var latestCandleValueObject = VRCandleObject{}
var latestCandleValueObjectString = ""

func doSectorHandling() {
	// atCycleDoSectorAnalytics()
	// atCycleDoSectorDataStore()
	// atCycleDoSectorBlotTests()
	atCycleDoSectorBlotTestBuyReview()
}

func atCycleDoSectorBlotTestBuyReview() {

	for isOperateSectorBlotTestBuyReview {
		time.Sleep(time.Duration(2) * time.Second)

		//each of these functinos are warpfields
		//not handled in this code... but to be added and maintained in creation warpfields

		//picture analysis done and //stored
		atCycleSectorPictureReview()

		//blot tests updated, data //stored of results
		atCycleSectorUpdateBlotTests()

		//do buy
		if isBoughtAtSector == false {
			handleSectorBuyChoiceProbability()
		}

	}

}

func atCycleSectorPictureReview() {
	//handle on picture,

	//do pixel analysis copy from source

	//store...?
	//check if new candle

	//

	doCandleReadVR1()
	parseIntoVRCandleObjects()
	stringValuesCandles := ""
	for i, v := range listVRCandleObject {
		stringValuesCandles += v.Coloration + "~" + strconv.Itoa(v.DimensionXHigh) + "~" + strconv.Itoa(v.DimensionXLow) + "~" + strconv.Itoa(v.DimensionYHigh) + "~" + strconv.Itoa(v.DimensionYLow)
		if i == (len(listVRCandleObject) - 1) {
			latestCandleValueObjectString = v.Coloration + "~" + strconv.Itoa(v.DimensionXHigh) + "~" + strconv.Itoa(v.DimensionXLow) + "~" + strconv.Itoa(v.DimensionYHigh) + "~" + strconv.Itoa(v.DimensionYLow)
		}
		if i != (len(listVRCandleObject) - 1) {
			stringValuesCandles += ","
		}
		i++
	}
	latestCandleValueObject = listVRCandleObject[len(listVRCandleObject)-1]

	isNewCandle = calculateIsNewCandle()
}

var isNewCandle = false
var calculateIsNewCandleIndex = 0
var candlePattern1 = VRCandleObject{}
var candlePattern2 = VRCandleObject{}
var candlePattern3 = VRCandleObject{}

func calculateIsNewCandle() bool {
	isNewCandle = false
	if calculateIsNewCandleIndex == 0 {
		calculateIsNewCandleIndex = 1
		//set candle pattern
		candlePattern1 = listVRCandleObject[len(listVRCandleObject)-2]
		candlePattern2 = listVRCandleObject[len(listVRCandleObject)-3]
		candlePattern3 = listVRCandleObject[len(listVRCandleObject)-4]
		return isNewCandle
	}
	if candlePattern1.DimensionYLow != listVRCandleObject[len(listVRCandleObject)-2].DimensionYLow && candlePattern1.DimensionYHigh != listVRCandleObject[len(listVRCandleObject)-2].DimensionYHigh {
		if candlePattern2.DimensionYLow != listVRCandleObject[len(listVRCandleObject)-3].DimensionYLow && candlePattern2.DimensionYHigh != listVRCandleObject[len(listVRCandleObject)-3].DimensionYHigh {
			if candlePattern3.DimensionYLow != listVRCandleObject[len(listVRCandleObject)-4].DimensionYLow && candlePattern3.DimensionYHigh != listVRCandleObject[len(listVRCandleObject)-4].DimensionYHigh {
				isNewCandle = true
				candlePattern1 = listVRCandleObject[len(listVRCandleObject)-2]
				candlePattern2 = listVRCandleObject[len(listVRCandleObject)-3]
				candlePattern3 = listVRCandleObject[len(listVRCandleObject)-4]
			}
		}
	}
	return isNewCandle
}
func atCycleSectorUpdateBlotTests() {

	//do all warpanalysis.
	//recording data of candles not really needed... unless want to be efficient with replacing space for pictures

	warpFieldTest1()
	warpFieldTest2()
	warpFieldTest3()
}

var listWarpFieldTest1 = []WarpFieldDataObject{}
var listWarpFieldTest2 = []WarpFieldDataObject{}
var listWarpFieldTest3 = []WarpFieldDataObject{}

func warpFieldTest1() {
	//create test for active environment,
	//set paramaters and conditions WarpFieldDataObject

	activeWarpFieldDataObjectToAdd := WarpFieldDataObject{}

	// activeWarpFieldDataObjectToAdd.VariableString = stringValuesCandles
	// activeWarpFieldDataObjectToAdd
	// for i, v := range listWarpFieldTest1 {
	// 	//handle on latest candle,
	// 	// latestCandleValueObject
	// 	i++
	// }
	listWarpFieldTest1 = append(listWarpFieldTest1, activeWarpFieldDataObjectToAdd)
}

func warpFieldTest2() {

	// for i, v := range listWarpFieldTest2 {
	// 	i++
	// }
}

func warpFieldTest3() {
	// for i, v := range listWarpFieldTest3 {
	// 	i++
	// }
}

func handleSectorBuyChoiceProbability() {

}

// func atCycleDoSectorAnalytics() {
// 	time.Sleep(time.Duration(10) * time.Second)
// 	for isOperateSectorAnalytics {
// 		time.Sleep(time.Duration(2) * time.Second)
// 		// doReadAnalyticsHead()
// 		// doReadAnalyticsBlotTests()
// 		// doReadAnalyticsCurrents()
// 	}
// }
// func atCycleDoSectorDataStore() {
// 	time.Sleep(time.Duration(10) * time.Second)
// 	for isOperateSectorDataStore {
// 		time.Sleep(time.Duration(2) * time.Second)
// 		storeDataFromPicture()
// 		//handle time conditions and tagging
// 	}
// }
// func atCycleDoSectorBlotTests() {
// 	time.Sleep(time.Duration(10) * time.Second)
// 	for isOperateSectorBlotTests {
// 		time.Sleep(time.Duration(2) * time.Second)
// 		handleAllActiveWarpCaculations()
// 	}
// }
func doReadAnalyticsBlotTests() {

}
func storeDataFromPicture() {

}
func handleAllActiveWarpCaculations() {
	for i, v := range activeWarpList {
		isComplete := false

		//remove at index
		if isComplete {
			inactiveWarpList = append(inactiveWarpList, v)
		}
		i++
	}
}

var activeWarpList = []WarpFieldDataObject{}
var inactiveWarpList = []WarpFieldDataObject{}

type WarpFieldDataObject struct {
	Name string
	// ListSeriesDataObjectResultant []SeriesDataObjectResultant
	VariableString string
	// FunctionString                   string
	// SubscribedToSectorNameString     string
	// PersonalEnvelopeSectorNameString string

	TimeInstantiatedString string
	// TimeOriginallyCreatedString string

	// SecInstantiated   string
	// MinInstantiated   string
	// HourInstantiated  string
	// DayInstantiated   string
	// MonthInstantiated string
	// YearInstantiated  string

	// SecOriginallyCreated   string
	// MinOriginallyCreated   string
	// HourOriginallyCreated  string
	// DayOriginallyCreated   string
	// MonthOriginallyCreated string
	// YearOriginallyCreated  string

	// ListUpdateVariablesOfWarpObject              []string
	// ListUpdateFunctionsOfWarpObject              []string
	// ListUpdateSubscribedToSectorNameOfWarpObject []string
	// ListUpdatePersonalEnvelopeSectorOfWarpObject []string
}

func checkIsClosePictureOnScreen() bool {
	//check if picture is on screen, and if it is return bool
	isBool := false

	//have where check position for something nad if none object returned or something like that.
	//then have default return false.

	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	// nameOfTest := ",itterateCacheValueIndexEnd:" + globalBuyProcessVRItterate + "~" + "dateAndTime:" + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringMinute + "~" + stringHour + "EndDateAndTime"

	// stringMinuteBought := strconv.Itoa(minuteBought)

	informationString := "~" + "stringYear:" + stringYear + "~" + "stringMonth:" + stringMonth + "~" + "stringDay:" + stringDay + "~" + "hour:" + stringHour + "~" + "minute:" + stringMinute + "~"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "isOkayOrClosePicturePresentErrorFile.txt", informationString)

	//write to cache the picture when found,

	//don't need to write to file, just write timestamp, can just take picture and name it
	//error and have an error iteration file to read from.

	//

	//
	return isBool
}

// func checkIsOkayPictureOnScreen() bool {
// 	//check if picture is on screen, and if it is return bool
// 	isBool := false

// 	//have where check position for something nad if none object returned or something like that.
// 	//then have default return false.

// 	informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
// 	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

// 	//
// 	return isBool
// }

//

//

var isTradeDay = false

func checkIsTradeDay() {
	// have a defualt false
	dayOfWeek := getDayOfWeek()
	if dayOfWeek == "Sunday" {
		doConnectionCorrection()
	}
	if dayOfWeek == "Sunday" || dayOfWeek == "Monday" || dayOfWeek == "Tuesday" || dayOfWeek == "Wednesday" || dayOfWeek == "Thursday" {
		isTradeDay = true
	}
}

func doConnectionCorrection() {
	//click at location, and do a click at the connection tab which is static and then click connect to live
	// robotgo.MoveMouse(773, 529)
	// robotgo.Click()

	// time.Sleep(time.Duration(2) * time.Second)
	// robotgo.MoveMouse(534, 142)
	// robotgo.Click()

	// //add the connect to live coordinates.
	// time.Sleep(time.Duration(1) * time.Second)
	// robotgo.MoveMouse(548, 192)
	// robotgo.Click()
}

// var isCalculateSecondTradeInChain = false

func setPullIsSecondChainTradeValuesFromCache() {
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr/secondChainFile.txt`)
	listResults := strings.Split(fileString, ",")
	listValues := strings.Split(listResults[len(listResults)-1], "~")

	isSecondChainTradeFromCache := listValues[0]
	directionTradeSecondChain = listValues[1]

	floatBoughtPrice := 0.0
	if s, err := strconv.ParseFloat(listValues[2], 64); err == nil {
		floatBoughtPrice = s
	}
	boughtPriceSecondChain = floatBoughtPrice

	// dayStringFromCache := listValues[3]
	// currentDay := getDay()
	// stringDay1 := strconv.Itoa(currentDay)

	// if stringDay1 == dayStringFromCache {
	// 	isSameDayValuesFromCache = true
	// }

	// if isSameDayValuesFromCache {
	if isSecondChainTradeFromCache == "1" {
		isSecondChainTrade = true
	}
	// }
}

var isSecondChainTrade = false
var directionTradeSecondChain = "negative"
var boughtPriceSecondChain = 0.0

// var isSameDayValuesFromCache = false

// var isCalculateThirdTradeInChain = false

func setPullIsThirdChainTradeValuesFromCache() {
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr/thirdChainFile.txt`)
	listResults := strings.Split(fileString, ",")
	listValues := strings.Split(listResults[len(listResults)-1], "~")

	isThirdChainTradeFromCache := listValues[0]
	directionTradeThirdChain = listValues[1]

	floatBoughtPrice := 0.0
	if s, err := strconv.ParseFloat(listValues[2], 64); err == nil {
		floatBoughtPrice = s
	}
	boughtPriceThirdChain = floatBoughtPrice

	// dayStringFromCache := listValues[3]
	// currentDay := getDay()
	// stringDay1 := strconv.Itoa(currentDay)

	// if stringDay1 == dayStringFromCache {
	// 	isSameDayValuesFromCache = true
	// }
	// if isSameDayValuesFromCache {
	if isThirdChainTradeFromCache == "1" {
		isThirdChainTrade = true
	}
	// }
}

var isThirdChainTrade = false
var directionTradeThirdChain = "negative"
var boughtPriceThirdChain = 0.0

// var isSameDayValuesFromCache = false

//

// func handleOnIsTradeWindowOpen() {
// 	hour := getCurrentHour()
// 	minute := getCurrentMinute()
// 	//
// 	//handle on day reset
// 	isDayReset = readIsDayReset()

// 	//in trade window
// 	if isDayReset == "true" {
// 		isTradeWindowOpen = true
// 	}

// 	//out trade window reset
// 	//conditional isDayReset set false upon chain end in sell wrapup
// 	if minute == 0 && hour == 23 && isDayReset == "false" {
// 		// isTradeWindowOpen = true
// 		writeIsDayReset("true")
// 		isTradeWindowOpen = true
// 	}
// }

var isFirstTimeBuyForDay = false

//upon bought price then that price set the values of each level.
// func setThresholdForBoughtPrice(boughtPrice float64) {
// 	readFileString := readDayBoughtFile()
// 	listValuesRead := parseDayBoughtFileLastString(readFileString)

// 	boughtPriceLevelObject.LastBoughtPrice = boughtPrice

// 	rangeIncrementValue := 5.0
// 	// positiveRange
// 	iPositive := 0
// 	valueCalculated := 0.0
// 	for iPositive < 6 {
// 		if iPositive == 0 {
// 			valueCalculated += boughtPriceLevelObject.LastBoughtPrice
// 		}
// 		valueCalculated += rangeIncrementValue

// 		LevelObject := LevelObject{}
// 		LevelObject.Price = valueCalculated
// 		boughtPriceLevelObject.ListLevelPositive = append(boughtPriceLevelObject.ListLevelPositive, LevelObject)
// 		iPositive++
// 	}

// 	iNegative := 0
// 	valueCalculated = 0.0
// 	for iNegative < 6 {
// 		if iNegative == 0 {
// 			valueCalculated += boughtPriceLevelObject.LastBoughtPrice
// 		}
// 		valueCalculated -= rangeIncrementValue

// 		LevelObject := LevelObject{}
// 		LevelObject.Price = valueCalculated
// 		boughtPriceLevelObject.ListLevelNegative = append(boughtPriceLevelObject.ListLevelNegative, LevelObject)
// 		iNegative++
// 	}
// 	//
// 	//
// 	return boughtPriceLevelObject
// }

func parseDayBoughtFileLastString(readFileString string) []string {
	listResults := strings.Split(readFileString, ",")
	lastListCommaValueString := listResults[len(listResults)-1]
	listValuesRead := strings.Split(lastListCommaValueString, "~")
	return listValuesRead
}

func readDayBoughtFile() string {
	// boughtPriceFile.txt
	fileString := readFile(`C:/Users/Bayliss Carr/Coding/fuvr/boughtPriceFile.txt`)
	return fileString
}

//where check if above or below any certain conditions,
//given certain levels reached.
//then have the levels reached and handled where set for each caldulation,
//have bools for levels reached
var boughtPriceLevelObject = BoughtPriceLevelObject{}

func checkIsConditionSellBought() {

}

type BoughtPriceLevelObject struct {
	LastBoughtPrice    float64
	CurrentBoughtPrice float64
	ListLevelPositive  []LevelObject
	ListLevelNegative  []LevelObject
}
type LevelObject struct {
	IsReachedPositiveDirection bool
	IsReachedNegativeDirection bool
	Price                      float64
}

// -------

var isDoLesserTrade = false

func isInDayRangeDoLesserTrade() {
	dayOfWeek := getDayOfWeek()
	if dayOfWeek == "Sunday" || dayOfWeek == "Thursday" {
		isDoLesserTrade = true
	}
}

type TimePriceObject struct {
	Year   string
	Month  string
	Day    string
	Hour   string
	Minute string
	Second string

	Price        string
	Ticker       string
	BuyPrice     string
	AccountValue string
}

var indexCalculateIsPreviousCandleIsDifferentThanCurrentCandle = 0
var previousTimePriceObject = TimePriceObject{}
var indexSameEntryCounter = 0
var isCheckForSameEntryHoldIsOver = false

var isHoldWhereCandleIsSame = false
var isNotOkayToBuyInTimeDelimOfMarketTempClose = false
var conditionTimeMinuteMarketTempClose = 30
var conditionTimeHoursMarketTempClose = 13

func calculateIsNotOkayToBuyInTimeDelimOfMarketTempClose() {
	currentTime := time.Now()
	if currentTime.Minute() == conditionTimeMinuteMarketTempClose && currentTime.Hour() == conditionTimeHoursMarketTempClose {
		isNotOkayToBuyInTimeDelimOfMarketTempClose = true
	}
}
func calculateIsHoldWhereCandleIsSame() {
	//iterrate backwards
	calculateIsNotOkayToBuyInTimeDelimOfMarketTempClose()
	readCache := readCache()
	sum := 1
	// indexToSplit := 0
	stringSearching := ""
	listStringValues := []string{}
	for sum <= 100 {
		s := string([]byte{readCache[len(readCache)-sum]})
		if s == "," {
			break
		}
		listStringValues = append(listStringValues, s)
		sum += 1
	}
	sum = 1
	for sum <= 100 {
		stringSearching += listStringValues[len(listStringValues)-sum]
		if sum == len(listStringValues) {
			break
		}
		sum += 1
	}
	listResults := strings.Split(stringSearching, "~")
	second := listResults[6]
	minute := listResults[5]
	hour := listResults[4]
	//current candle value should be passed and compared with the read.
	//well this would work if put a 20 second itterate
	//consider every 50 minute consideration for trading.
	isDifferentEntry := true
	if indexCalculateIsPreviousCandleIsDifferentThanCurrentCandle != 0 {
		if hour == previousTimePriceObject.Hour && minute == previousTimePriceObject.Minute && second == previousTimePriceObject.Second {
			isDifferentEntry = false
		}
		if isCheckForSameEntryHoldIsOver {
			if isDifferentEntry {
				isCheckForSameEntryHoldIsOver = false
				isNotOkayToBuyInTimeDelimOfMarketTempClose = false
			}
		}
		previousTimePriceObject.Second = second
		previousTimePriceObject.Minute = minute
		previousTimePriceObject.Hour = hour
	}
	if indexCalculateIsPreviousCandleIsDifferentThanCurrentCandle == 0 {
		previousTimePriceObject.Second = second
		previousTimePriceObject.Minute = minute
		previousTimePriceObject.Hour = hour
		indexCalculateIsPreviousCandleIsDifferentThanCurrentCandle = 1
	}
	if isCheckForSameEntryHoldIsOver == false {
		if isDifferentEntry == false {
			indexSameEntryCounter++
			if indexSameEntryCounter == 3 {
				isCheckForSameEntryHoldIsOver = true
			}
		}
	}
	time.Sleep(time.Duration(20) * time.Second)
	calculateIsHoldWhereCandleIsSame()
}

var isOperateMonitorHolding = true

func beginCycleMonitorHolding() {
	time.Sleep(time.Duration(10) * time.Second)
	for isOperateMonitorHolding {
		time.Sleep(time.Duration(2) * time.Second)
		// fmt.Println("yas4")
		doDigitReadForSellProcedure()
	}
}

var isOperateMonitorHoldingSecondChain = true

func beginCycleMonitorHoldingSecondChain() {
	for isOperateMonitorHoldingSecondChain {
		time.Sleep(time.Duration(2) * time.Second)
		// fmt.Println("yas4")
		doDigitReadForSellProcedureSecondChain()
	}
}

var isOperateMonitorHoldingThirdChain = true

func beginCycleMonitorHoldingThirdChain() {
	for isOperateMonitorHoldingThirdChain {
		time.Sleep(time.Duration(2) * time.Second)
		// fmt.Println("yas4")
		doDigitReadForSellProcedureThirdChain()
	}
}

func readIsDayReset() string {
	fileString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\isDayReset.txt`)
	return fileString
}

func writeIsDayReset(stringValue string) {
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr\`, "isDayReset.txt", stringValue)
}

var isDayReset = "false"

//

var tickPixelLength = 23
var isOperateItterateCalculateBuy = true

func doOperateItterateCalculateBuy() {
	for isOperateItterateCalculateBuy {
		hour := getCurrentHour()
		minute := getCurrentMinute()
		fmt.Println("inside doOperateItterateCalculateBuy")
		time.Sleep(time.Duration(1) * time.Second)

		if minute == 0 && hour == 22 {
			isOkayToTradeNotSecondTrade := readIsOkayToTradeNotSecondTrade()
			if isOkayToTradeNotSecondTrade == "negative" {
				writePositiveIsOkayToTradeNotSecondTrade()
			}
			// if minute == 0 && hour == 22 {
		}

		// if minute == 45 && hour == 19
		// if minute == 54 && hour == 20 {
		if minute == 0 && hour == 23 {
			checkIsTradeDay()
			// fmt.Println()
			if isTradeDay {
				fmt.Println(isTradeDay)
				isOkayToTradeNotSecondTrade := readIsOkayToTradeNotSecondTrade()
				if isOkayToTradeNotSecondTrade == "positive" {
					writeNegativeIsOkayToTradeNotSecondTrade()
					//
					isOperateMonitorHolding = true
					itterateItterateCacheValue()
					time.Sleep(time.Duration(1) * time.Second)
					//bayfork
					// robotgo.SaveCapture("./pictureBuyReviewStore/saveCaptureStore" + globalBuyProcessVRItterate + ".png")
					// robotgo.SaveCapture("./saveCapture1.png")
					fmt.Println("yas")
					time.Sleep(time.Duration(1) * time.Second)
					accountValueBefore = accountIdentifyProcedureBuyProcess()
					//
					doCandleReadVR()
					parseIntoVRCandleObjects()
					calculateIsBlackLineInRange()
					doCalculateTrend()
					fmt.Println("yas1")
					if buyRecomendation == "null" {
						fmt.Println("buy recommendation null trying again...")
						writeToBuyAnalysisFailOrSucceedFile("fail")
						//this is a continuition if the problem is not found where candle does not make sense.
					}
					if buyRecomendation != "null" {
						doBuyProcedure()
						writeToBuyAnalysisFailOrSucceedFile("succeed")
						break
					}
				}
			}
		}
	}
}

func readIsOkayToTradeNotSecondTrade() string {
	fileToReadString := `C:\Users\Bayliss Carr\Coding\fuvr1\isOkayToTradeNotSecondTrade.txt`
	fileString := readFile(fileToReadString)
	return fileString
}

func writePositiveIsOkayToTradeNotSecondTrade() {
	// fileToReadString := `C:\Users\Bayliss Carr\Coding\fuvr\isOkayToTradeNotSecondTrade.txt`
	fileString := "positive"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr1`, "isOkayToTradeNotSecondTrade.txt", fileString)
}
func writeNegativeIsOkayToTradeNotSecondTrade() {
	// fileToReadString := `C:\Users\Bayliss Carr\Coding\fuvr\isOkayToTradeNotSecondTrade.txt`
	fileString := "negative"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr1`, "isOkayToTradeNotSecondTrade.txt", fileString)
}

// if isSecondChainTrade {
// 	fmt.Println("inside isSecondChainTrade")
// 	fmt.Println(boughtPriceSecondChain)
// 	//check for time is in range or out of range,
// 	//and check if bought price condition is met,
// 	//do splint analysis of current price.
// 	calculateIsBoughtPriceLevelMetSecondChain()
// 	if isSecondChainBoughtPriceLevelMet {
// 		fmt.Println("met")
// 		doBuyProcedureSecondChain()
// 		isSecondChainTrade = false
// 	}
// 	//tentatively 4 am
// 	if minute == 0 && hour == 4 {
// 		isSecondChainTrade = false
// 	}
// }

// if isThirdChainTrade {
// 	fmt.Println("inside isThirdChainTrade")
// 	fmt.Println(boughtPriceThirdChain)
// 	//check for time is in range or out of range,
// 	//and check if bought price condition is met,
// 	//do splint analysis of current price.
// 	calculateIsBoughtPriceLevelMetThirdChain()
// 	if isThirdChainBoughtPriceLevelMet {
// 		doBuyProcedureThirdChain()
// 		isThirdChainTrade = false
// 	}
// 	//tentatively 4 am
// 	if minute == 0 && hour == 4 {
// 		isThirdChainTrade = false
// 	}
// }

func calculateIsBoughtPriceLevelMetSecondChain() {
	//calculate level met in price range.
	// robotgo.SaveCapture("saveCapture1.png")
	time.Sleep(time.Duration(2) * time.Second)
	listPixelListYInQuestion := calculateDimensionsPixelObjectDigits3()
	listCharacterPixelListsInQuestionPrice := seperateCharactersLastPrice(listPixelListYInQuestion)
	currentPriceGlobal = identifyPriceDigits(listCharacterPixelListsInQuestionPrice)
	floatCurrentPrice := 0.0
	if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
		floatCurrentPrice = s
	}
	if directionTradeSecondChain == "positive" {
		if floatCurrentPrice >= boughtPriceSecondChain {
			fmt.Println("second positive")
			isSecondChainBoughtPriceLevelMet = true
		}
	}
	if directionTradeSecondChain == "negative" {
		if floatCurrentPrice <= boughtPriceSecondChain {
			fmt.Println("second negitive")
			isSecondChainBoughtPriceLevelMet = true
		}
	}
}

var isSecondChainBoughtPriceLevelMet = false

func calculateIsBoughtPriceLevelMetThirdChain() {
	//calculate level met in price range.
	// robotgo.SaveCapture("saveCapture1.png")
	time.Sleep(time.Duration(2) * time.Second)
	listPixelListYInQuestion := calculateDimensionsPixelObjectDigits3()
	listCharacterPixelListsInQuestionPrice := seperateCharactersLastPrice(listPixelListYInQuestion)
	currentPriceGlobal = identifyPriceDigits(listCharacterPixelListsInQuestionPrice)
	floatCurrentPrice := 0.0
	if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
		floatCurrentPrice = s
	}
	if directionTradeThirdChain == "positive" {
		if floatCurrentPrice >= boughtPriceThirdChain {
			isThirdChainBoughtPriceLevelMet = true
		}
	}
	if directionTradeThirdChain == "negative" {
		if floatCurrentPrice <= boughtPriceThirdChain {
			isThirdChainBoughtPriceLevelMet = true
		}
	}
}

var isThirdChainBoughtPriceLevelMet = false

//

// func doOperateItterateCalculateBuyChain2() {
// 	doBuyProcedureSecondChain()
// }

var isOperateItterateCalculateBuyIntervalStore = true

func doCycleStorePictureAtInterval() {
	for isOperateItterateCalculateBuyIntervalStore {
		fmt.Println("inside doOperateItterateCalculateBuy")
		// time.Sleep(time.Duration(300) * time.Second)
		time.Sleep(time.Duration(10) * time.Second)
		isOperateMonitorHolding = true
		itterateItterateCacheValue()
		time.Sleep(time.Duration(1) * time.Second)
		// robotgo.SaveCapture("./pictureBuyReviewStore/saveCaptureStore" + globalBuyProcessVRItterate + ".png")
	}
}

var isHoldingPosition = false
var isNextMinuteCanTradeHasPassed = false

func doBuyProcedure() {
	fmt.Println("yas2")
	buyRecomendation = "positive"
	if buyRecomendation == "positive" {
		fmt.Println("BUYYYYY command read")
		buyOperation()
		fmt.Println("yas3")
		go beginCycleMonitorHolding()
	}
	if buyRecomendation == "negative" {
		sellOperation()
		fmt.Println("yas3")
		go beginCycleMonitorHolding()
	}
	isHoldingPosition = true
}

func doBuyProcedureSecondChain() {
	// fmt.Println("yas2")
	if directionTradeSecondChain == "positive" {
		// fmt.Println("BUYYYYY command read")
		buyOperation()
		// fmt.Println("yas3")
		go beginCycleMonitorHoldingSecondChain()
	}
	if directionTradeSecondChain == "negative" {
		sellOperation()
		// fmt.Println("yas3")
		go beginCycleMonitorHoldingSecondChain()
	}
	isHoldingPosition = true
}

func doBuyProcedureThirdChain() {
	// fmt.Println("yas2")
	if directionTradeThirdChain == "positive" {
		// fmt.Println("BUYYYYY command read")
		buyOperation()
		// fmt.Println("yas3")
		go beginCycleMonitorHoldingThirdChain()
	}
	if directionTradeThirdChain == "negative" {
		sellOperation()
		// fmt.Println("yas3")
		go beginCycleMonitorHoldingThirdChain()
	}
	isHoldingPosition = true
}

var isContainBlackLine = false
var blackLineXDimension = 0

func calculateIsBlackLineInRange() {
	for i, v := range listPixelObjectList {
		//column
		if i == 1208 {
			break
		}
		if i > 384 {
			for i2, pixelObject := range v {
				//y value
				if i2 >= 876 {
					redValueInt := 0
					if s, err := strconv.Atoi(pixelObject.RedValue); err == nil {
						redValueInt = s
					}
					blueValueInt := 0
					if s, err := strconv.Atoi(pixelObject.BlueValue); err == nil {
						blueValueInt = s
					}
					greenValueInt := 0
					if s, err := strconv.Atoi(pixelObject.GreenValue); err == nil {
						greenValueInt = s
					}
					// grey
					if redValueInt != 255 && blueValueInt != 255 && greenValueInt != 255 {
						// if redValueInt == 48 && blueValueInt == 50 && greenValueInt == 48 {
						fmt.Println("hit")
						isContainBlackLine = true
						blackLineXDimension = i
						break
					}
				}
				if i2 == 878 {
					break
				}
				i2++
			}
		}
		i++
	}
	isContainBlackLine = false
}

var buyRecomendation = "null"

func doCalculateTrend() {
	listSampleVRCandleObject := listVRCandleObject

	if isContainBlackLine {
		fmt.Println("inside isContainBlackLine")
		fmt.Println(blackLineXDimension)
		fmt.Println("candle past black line")
		//handle grey candles for now
		fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-3].DimensionYHigh)
		if listSampleVRCandleObject[len(listSampleVRCandleObject)-3].DimensionYHigh != 0 {
			fmt.Println("end see")
			if listSampleVRCandleObject[len(listSampleVRCandleObject)-3].DimensionYHigh > listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh {
				// buyRecomendation = "positive"
				buyRecomendation = "negative"
			}
			if listSampleVRCandleObject[len(listSampleVRCandleObject)-3].DimensionYHigh < listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh {
				// buyRecomendation = "negative"
				buyRecomendation = "positive"
			}
		}
	}

	//
	// for i, v := range listSampleVRCandleObject {
	// 	fmt.Println("dimensions")
	// 	fmt.Println(v.DimensionYHigh)
	// 	// fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-5].DimensionYHigh)
	// 	fmt.Println(v.DimensionYLow)
	// 	// fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-5].DimensionYLow)
	// 	fmt.Println(v.DimensionXLow)
	// 	fmt.Println(v.DimensionXHigh)
	// 	// fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-5].DimensionXLow)
	// 	fmt.Println(v.Coloration)
	// 	i++
	// }
	if isContainBlackLine == false {
		fmt.Println("inside isContainBlackLine == false")
		fmt.Println(blackLineXDimension)
		fmt.Println("candle past black line")
		for i, v := range listSampleVRCandleObject {
			if v.DimensionXLow > blackLineXDimension {
				if v.Coloration != "grey" {
					if v.DimensionYHigh != 0 {
						fmt.Println("dimensions")
						fmt.Println("DimensionYHigh")
						fmt.Println(v.DimensionYHigh)
						fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh)
						fmt.Println("DimensionYLow")
						fmt.Println(v.DimensionYLow)
						fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow)
						fmt.Println("DimensionXLow")
						fmt.Println(v.DimensionXLow)
						fmt.Println(listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionXLow)

						// if listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh == 0 || listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow == 0 {
						// 	buyRecomendation = "null"
						// 	break
						// }
						// if v.DimensionYHigh > listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh {
						// 	buyRecomendation = "positive"
						// 	// buyRecomendation = "negative"
						// 	break
						// }
						// if v.DimensionYHigh < listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh {
						// 	buyRecomendation = "negative"
						// 	// buyRecomendation = "positive"
						// 	break
						// }
						// if v.DimensionYHigh == listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYHigh {

						if listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow == 0 { //|| listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow == 0 {
							buyRecomendation = "null"
							break
						}
						if v.DimensionYLow > listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow {
							// buyRecomendation = "positive"
							buyRecomendation = "negative"
							break
						}
						if v.DimensionYLow < listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow {
							// buyRecomendation = "negative"
							buyRecomendation = "positive"
							break
						}
						if v.DimensionYLow == listSampleVRCandleObject[len(listSampleVRCandleObject)-1].DimensionYLow {
							if listSampleVRCandleObject[len(listSampleVRCandleObject)-1].Coloration == "red" {
								fmt.Println("color red")
								buyRecomendation = "negative"
							}

							if listSampleVRCandleObject[len(listSampleVRCandleObject)-1].Coloration == "green" {
								fmt.Println("color green")
								buyRecomendation = "positive"
							}
							// buyRecomendation = "positive"
							break
						}
					}
				}
			}
			i++
		}
	}
	if buyRecomendation == "null" {

		fmt.Println("fail buy recording null")

		// year, month, day := getDate()
		// stringDay := strconv.Itoa(day)
		// stringMonth := strconv.Itoa(month)
		// stringYear := strconv.Itoa(year)

		// hour := getCurrentHour()
		// minute := getCurrentMinute()
		// second := getCurrentSecond()
		// stringHour := strconv.Itoa(hour)
		// stringMinute := strconv.Itoa(minute)
		// stringSecond := strconv.Itoa(second)
		// priceFileName := "date~" + stringYear + "~" + stringMonth + "~" + stringDay + "~"
		// priceFileName += stringHour + "~" + stringMinute + "~" + stringSecond + "~"

		// valueIterrate3 := len(listSampleVRCandleObject) - 3
		// valueIterrate2 := len(listSampleVRCandleObject) - 2
		// valueIterrate1 := len(listSampleVRCandleObject) - 1

		// //convert each int into strings
		// stringToAppend := listSampleVRCandleObject[valueIterrate3].Coloration + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate3].DimensionXLow) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate3].DimensionXHigh) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate3].DimensionYLow) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate3].DimensionYHigh)
		// stringToAppend += "!" + listSampleVRCandleObject[valueIterrate2].Coloration + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate2].DimensionXLow) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate2].DimensionXHigh) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate2].DimensionYLow) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate2].DimensionYHigh)
		// stringToAppend += "!" + listSampleVRCandleObject[valueIterrate1].Coloration + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate1].DimensionXLow) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate1].DimensionXHigh) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate1].DimensionYLow) + "~" + strconv.Itoa(listSampleVRCandleObject[valueIterrate1].DimensionYHigh)

		// nameOfFile := priceFileName + ".txt"
		// robotgo.SaveCapture(`C:\Users\Bayliss Carr\Coding\fuvr\cacheGreyError\` + priceFileName + `.png`)
		// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr\cacheGreyError\`, nameOfFile, stringToAppend)

		// doFuvrRestart()
		// os.Exit(1)
	}
	fmt.Println(buyRecomendation)
}

var listPixelObjectList = [][]PixelObject{}

func doCandleReadVR() {
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	// file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + "4345" + ".PNG")
	// file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + "5167" + ".PNG")
	// file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + "6130" + ".PNG")
	file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + globalBuyProcessVRItterate + ".PNG")

	fmt.Println("opening" + globalBuyProcessVRItterate)
	// file, err := os.Open("candleVRPic.PNG")
	// file, err := os.Open("saveCapture33.PNG")
	// file, err := os.Open("saveCapture88.PNG")
	// file, err := os.Open("saveCapture75.PNG")f
	if err != nil {
		fmt.Println("Error: File could not be opened")
		os.Exit(1)
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		fmt.Println(err)
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	var listPixels []PixelObject
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			value1, v2, v3, v4 := img.At(x, y).RGBA()
			listPixels = append(listPixels, rgbaToPixel(value1, v2, v3, v4, x, y))
		}
	}
	xLim := width
	//coordinate x values into pixel value containers.

	sum := 0
	for sum <= xLim {
		listPixelObjectList = append(listPixelObjectList, []PixelObject{})
		sum += 1
	}
	//for every pixel
	for indexPixel, pixelObject := range listPixels {
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}
		//append pixel into appropriate x value list, list contains lists of x coordinates.
		listPixelObjectList[intXValue] = append(listPixelObjectList[intXValue], pixelObject)
		indexPixel += 1
	}
}

func doCandleReadVR1() {
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	// file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + "4345" + ".PNG")
	// file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + "5167" + ".PNG")
	// file, err := os.Open("./pictureBuyReviewStore/saveCaptureStore" + "6130" + ".PNG")
	file, err := os.Open("./saveCapture11.PNG")

	fmt.Println("opening" + globalBuyProcessVRItterate)
	// file, err := os.Open("candleVRPic.PNG")
	// file, err := os.Open("saveCapture33.PNG")
	// file, err := os.Open("saveCapture88.PNG")
	// file, err := os.Open("saveCapture75.PNG")f
	if err != nil {
		fmt.Println("Error: File could not be opened")
		os.Exit(1)
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		fmt.Println(err)
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	var listPixels []PixelObject
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			value1, v2, v3, v4 := img.At(x, y).RGBA()
			listPixels = append(listPixels, rgbaToPixel(value1, v2, v3, v4, x, y))
		}
	}
	xLim := width
	//coordinate x values into pixel value containers.

	sum := 0
	for sum <= xLim {
		listPixelObjectList = append(listPixelObjectList, []PixelObject{})
		sum += 1
	}
	//for every pixel
	for indexPixel, pixelObject := range listPixels {
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}
		//append pixel into appropriate x value list, list contains lists of x coordinates.
		listPixelObjectList[intXValue] = append(listPixelObjectList[intXValue], pixelObject)
		indexPixel += 1
	}
}

//candle global vars
var isWhiteMeasurementActive = false
var isGreyMeasurementActive = false
var isColorFoundCandle = false
var indexGreyCounter = 0
var indexColorCounter = 0
var candleDelimXDimension = 0
var isEndDelimCalculation = false
var isWhiteSpaceEncounteredBeforeOtherCandle = false
var listVRCandleObject = []VRCandleObject{}
var index1st = true

func parseIntoVRCandleObjects() {
	for i, v := range listPixelObjectList {
		//column
		isColorFoundColumn := false
		isContainGreyColumn := false
		if i > 381 { //396 { //381 {
			for i2, pixelObject := range v {
				//y value
				if i2 > 357 {
					redValueInt := 0
					if s, err := strconv.Atoi(pixelObject.RedValue); err == nil {
						redValueInt = s
					}
					blueValueInt := 0
					if s, err := strconv.Atoi(pixelObject.BlueValue); err == nil {
						blueValueInt = s
					}
					greenValueInt := 0
					if s, err := strconv.Atoi(pixelObject.GreenValue); err == nil {
						greenValueInt = s
					}
					// grey
					if redValueInt == 205 && blueValueInt == 205 && greenValueInt == 205 {
						isContainGreyColumn = true
					}
					//red
					if redValueInt == 255 && blueValueInt == 4 && greenValueInt == 4 {
						isColorFoundColumn = true
					}
					//green
					if redValueInt == 53 && blueValueInt == 206 && greenValueInt == 53 {
						isColorFoundColumn = true
					}
					if isColorFoundColumn {
						isColorFoundCandle = true
					}
				}
				//if end of column, if las pixel of column
				if i2 == 887 {
					//handle where white
					if isGreyMeasurementActive {
						if isContainGreyColumn == false && isColorFoundColumn == false {
							isGreyMeasurementActive = false
							isWhiteSpaceEncounteredBeforeOtherCandle = true
							doCalculationCandleWidth()
							isEndDelimCalculation = true
						}
					}
					//remember for each calculation, have a break, but this is handled in the end column section.
					if isColorFoundCandle && isColorFoundColumn {
						if isGreyMeasurementActive {
							// isWhiteMeasurementActive = true
							isGreyMeasurementActive = false
							doCalculationCandleWidth()
							isEndDelimCalculation = true
							break
						}
						if isGreyMeasurementActive == false {
							indexColorCounter++
						}
					}
					//handle where grey no color, after color found
					if isColorFoundCandle {
						if isContainGreyColumn && isColorFoundColumn == false {
							stringIndex := strconv.Itoa(i)
							fmt.Println("grey is found where no column, at index:" + stringIndex)
							isGreyMeasurementActive = true
							indexGreyCounter++
						}
					}
					break
				}
				i2++
			}
		}
		i++
		if isEndDelimCalculation {
			break
		}
	}
	isOperate := true
	indexOperate := 0
	isCandleContainGrey := false
	isColorFoundCandle1 := false
	//column
	for isOperate {
		if indexOperate == 1209 {
			break
		}
		if indexOperate > 381 {
			coloration := "null"
			isColumnColored := false
			// isFirstPixelColoredFound := false
			dimensionXHigh := 0
			dimensionXLow := 0
			dimensionYHigh := 0
			dimensionYLow := 0
			isPreviousPixelColored := false
			isPreviousPixelGrey := false

			isEndGreyFoundForYDelim := false
			isColumnGrey := false

			isFirstPixel := false

			for interval2, pixelObject := range listPixelObjectList[indexOperate] {
				isPixelContainGrey1 := false
				isPixelContainColor1 := false
				//y
				if interval2 > 341 {
					// if pixelObject is grey area, without red or green
					redValueInt := 0
					if s, err := strconv.Atoi(pixelObject.RedValue); err == nil {
						redValueInt = s
					}
					blueValueInt := 0
					if s, err := strconv.Atoi(pixelObject.BlueValue); err == nil {
						blueValueInt = s
					}
					greenValueInt := 0
					if s, err := strconv.Atoi(pixelObject.GreenValue); err == nil {
						greenValueInt = s
					}
					//
					//grey
					if redValueInt == 205 && blueValueInt == 205 && greenValueInt == 205 {
						isPixelContainGrey1 = true
						//any grey found for candle
						isCandleContainGrey = true
						isColumnGrey = true
					}
					//red
					if redValueInt == 255 && blueValueInt == 4 && greenValueInt == 4 {
						coloration = "red"
						isColumnColored = true
						isPixelContainColor1 = true
						isColorFoundCandle1 = true
					}
					//green
					if redValueInt == 53 && blueValueInt == 206 && greenValueInt == 53 {
						coloration = "green"
						isColumnColored = true
						isPixelContainColor1 = true
						isColorFoundCandle1 = true
					}
					if isFirstPixel == false {
						isFirstPixel = true
						dimensionXLow = indexOperate
					}
					if isEndGreyFoundForYDelim == false && isPreviousPixelColored && isPixelContainGrey1 {
						dimensionYHigh = interval2 - 1
						isEndGreyFoundForYDelim = true
					}
					if isPixelContainGrey1 {
						isPreviousPixelColored = false
						isPreviousPixelGrey = true
					}
					if isPixelContainColor1 {
						if isPreviousPixelGrey {
							dimensionYLow = interval2
						}
						isPreviousPixelGrey = false
						isPreviousPixelColored = true
					}
				}
				if interval2 == 887 {
					break
				}
				interval2++
			}
			//if whitespace encountered and no color indexGrey
			if isCandleContainGrey && isColorFoundCandle1 == false && isColumnColored == false && isColumnGrey == false {
				listVRCandleObject = append(listVRCandleObject, VRCandleObject{Coloration: "grey", DimensionXHigh: dimensionXHigh, DimensionXLow: dimensionXLow, DimensionYHigh: dimensionYHigh, DimensionYLow: dimensionYLow})
				isCandleContainGrey = false
			}
			if isColumnColored {
				//where end of column calculation,
				//where colored, documented, add the dimension y value, with lim, can deduce x value.
				dimensionXHigh = dimensionXLow + dimensionColor - 1
				listVRCandleObject = append(listVRCandleObject, VRCandleObject{Coloration: coloration, DimensionXHigh: dimensionXHigh, DimensionXLow: dimensionXLow, DimensionYHigh: dimensionYHigh, DimensionYLow: dimensionYLow})
				indexAmountToAdd := (dimensionColor - 1) + (dimensionGrey / 2)
				indexOperate = indexOperate + indexAmountToAdd
				isCandleContainGrey = false
				isColorFoundCandle1 = false
			}
		}
		indexOperate++
		if indexOperate >= len(listPixelObjectList) {
			break
		}
	}
}

var dimensionGrey = 0
var dimensionColor = 0

func doCalculationCandleWidth() {
	dimensionGrey = indexGreyCounter
	if isWhiteSpaceEncounteredBeforeOtherCandle {
		dimensionGrey = indexGreyCounter * 2
	}
	dimensionColor = indexColorCounter
	strCandleDelimGrey := strconv.Itoa(dimensionGrey)
	strCandleDelimColor := strconv.Itoa(dimensionColor)
	fmt.Println("strCandleDelimGrey")
	fmt.Println(strCandleDelimGrey)
	fmt.Println("strCandleDelimColor")
	fmt.Println(strCandleDelimColor)
	candleDelimXDimension = dimensionGrey + dimensionColor
	strCandleDelimXDimension := strconv.Itoa(candleDelimXDimension)
	fmt.Println("strCandleDelimXDimension")
	fmt.Println(strCandleDelimXDimension)
}

func doCalculateIsGoodSwingTrade() {
}

type PixelColumnReviewObject struct {
	IsContainGrey  bool
	IsContainRed   bool
	IsContainGreen bool
	// PixelX         string
	// PixelY         string
	PixelYHigh PixelObject
	PixelYLow  PixelObject
}

type VRCandleObject struct {
	DimensionXHigh int
	DimensionXLow  int
	DimensionYHigh int
	DimensionYLow  int
	Coloration     string
}

var isGlobalDayChange = false

//hook in phase 2
var indexCheckIfDayChange = 0
var dayLast = ""

func checkIfDayChange() {
	day := getDay()
	stringDay := strconv.Itoa(day)
	if indexCheckIfDayChange != 0 {
		if dayLast != stringDay {
			isGlobalDayChange = true
			fmt.Println("day change detected")
			dayLast = stringDay
			//upon create new folder for day, set back to false isGlobalDayChange
		}
	}
	if indexCheckIfDayChange == 0 {
		dayLast = stringDay
		indexCheckIfDayChange += 1
	}
}

func buyOperation() {
	// robotgo.MoveMouse(140, 780)
	// robotgo.Click()
}
func sellOperation() {
// 	robotgo.MoveMouse(140, 810)
// 	robotgo.Click()
}
func closeOperation() {
	// robotgo.MoveMouse(290, 750)
	// robotgo.Click()
}
func calculatePixels() [][]PixelObject {
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	file, err := os.Open("./saveCapture1.png")

	if err != nil {
		fmt.Println("Error: File could not be opened")
		os.Exit(1)
	}

	defer file.Close()

	pixels, err := getPixels(file)

	if err != nil {
		fmt.Println("Error: Image could not be decoded")
		os.Exit(1)
	}
	// fmt.Println(pixels)
	return pixels
}

// Get the bi-dimensional pixel array
func getPixels(file io.Reader) ([][]PixelObject, error) {
	img, _, err := image.Decode(file)

	if err != nil {
		return nil, err
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y

	fmt.Println(width)
	fmt.Println(height)
	var pixels [][]PixelObject
	for y := 0; y < height; y++ {
		var row []PixelObject
		for x := 0; x < width; x++ {
			// row = append(row, rgbaToPixel(img.At(x, y).RGBA(), x, y))
			// row = append(row, rgbaToPixel(img, x, y))
			value1, v2, v3, v4 := img.At(x, y).RGBA()
			row = append(row, rgbaToPixel(value1, v2, v3, v4, x, y))
			// break
		}
		pixels = append(pixels, row)
		// break
	}

	return pixels, nil
}

//bay3
// img.At(x, y).RGBA() returns four uint32 values; we want a Pixel
func rgbaToPixel(v1 uint32, v2 uint32, v3 uint32, v4 uint32, x int, y int) PixelObject {
	pixel := PixelObject{}

	pixel.XValue = strconv.Itoa(x)
	pixel.YValue = strconv.Itoa(y)
	pixel.RedValue = strconv.Itoa(int(v1 / 257))
	pixel.BlueValue = strconv.Itoa(int(v2 / 257))
	pixel.GreenValue = strconv.Itoa(int(v3 / 257))
	return pixel
}

// Pixel struct example
type Pixel struct {
	xValue int
	yValue int
	R      int
	G      int
	B      int
	A      int
}

func createFolder(folderName string) {
	out, err := exec.Command("cmd", "/c", "cd /d Z:\\sharedFolder\\dayCandleStore\\ && mkdir "+folderName).Output()
	if err != nil {
		log.Fatal(err)
	}
	s := string(out)
	fmt.Println(s)
}
func createDayFilesInFolder(folderName string, textValue string, fileName string) { //"+folderName+"
	out, err := exec.Command("cmd", "/c", "cd /d Z:\\sharedFolder\\dayCandleStore\\"+folderName+" && echo "+textValue+" > "+fileName+".txt").Output()
	if err != nil {
		log.Fatal(err)
	}
	s := string(out)
	fmt.Println(s)
}
func renameFile(fileName string) string {
	out, err := exec.Command("cmd", "/c", "cd C:\\Users\\Bayliss Carr\\Pictures\\Screenshots && dir").Output()
	if err != nil {
		log.Fatal(err)
	}
	s := string(out)
	fmt.Println(s)
	listValues := strings.Split(s, ").png")
	listValues1 := strings.Split(listValues[0], "(")
	fmt.Println(listValues1[1])

	src := "C:\\Users\\Bayliss Carr\\Pictures\\Screenshots\\Screenshot (" + listValues1[1] + ").png"
	dst := "C:\\Users\\Bayliss Carr\\Pictures\\Screenshots\\" + fileName + ".png"
	os.Rename(src, dst)
	return fileName
}
func deleteTranslatedFiles() {
	os.RemoveAll("C:\\Users\\Bayliss Carr\\Coding\\fuvr_py\\translatedPictures3\\")
	os.MkdirAll("C:\\Users\\Bayliss Carr\\Coding\\fuvr_py\\translatedPictures3\\", os.FileMode(0522))
}
func deleteScreenshots() {
	os.RemoveAll("C:\\Users\\Bayliss Carr\\Pictures\\Screenshots\\")
	os.MkdirAll("C:\\Users\\Bayliss Carr\\Pictures\\Screenshots\\", os.FileMode(0522))
}

func startProcessReadCommandText() {
	sum := 0
	fmt.Println("start")
	for sum != -1 {
		time.Sleep(time.Duration(1) * time.Second)
		doReadCommandTextOperation()
	}
}

//var isPhase1 = true
var isBought = false
var isClosed = false
var isOperationCheckPoint = false

func doReadCommandTextOperation() {
	fmt.Println("hi")
	fileString := readFile(`Z:\sharedFolder\commandText.txt`)
	if fileString == "buy" {
		if isBought == false {
			//..isPhase1 = false
			// if isOperationCheckPoint {
			time.Sleep(time.Duration(5) * time.Second)
			fmt.Println("BUYYYYY command read")
			// postBuyOperation()
			buyOperation()
			writeStatusText("isPurchase")
			isBought = true
			isClosed = false
			// }
		}
	}
	if fileString == "sell" {
		if isBought == false {
			//isPhase1 = false
			// if isOperationCheckPoint {
			time.Sleep(time.Duration(5) * time.Second)
			fmt.Println("sell command read")
			sellOperation()
			writeStatusText("isPurchase")
			isBought = true
			isClosed = false
		}
	}

	if fileString == "close" {
		if isClosed == false {
			fmt.Println("close command read")
			//isPhase1 = false
			// if isOperationCheckPoint {
			closeOperation()
			isBought = false
			isClosed = true
			time.Sleep(time.Duration(10) * time.Second)
		}

	}
	time.Sleep(time.Duration(5) * time.Second)
	fmt.Println("reading on")
	doReadCommandTextOperation()
}

func writeStatusText(statusString string) {
	writeToFile(`Z:\sharedFolder\`, "statusText.txt", statusString)
}

func clearPriceText() {
	fileString := readFile(`Z:\sharedFolder\cache\priceFluxCache.txt`)
	writeToFile(`Z:\sharedFolder\cache`, "priceFluxCache1.txt", fileString)
	writeToFile(`Z:\sharedFolder\cache`, "priceFluxCache.txt", "")
}

func postDeleteTranslatedFiles() {
	json := "{ \"requestType\": \"deleteTranslatedFiles\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	fmt.Println(response)
	//return response
}

func postBuyOperation() {
	json := "{ \"requestType\": \"buyOperation\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	fmt.Println(response)
	//return response
}
func postSellOperation() {
	json := "{ \"requestType\": \"sellOperation\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	fmt.Println(response)
	//return response
}
func postCloseOperation() {
	json := "{ \"requestType\": \"closeOperation\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	fmt.Println(response)
	//return response
}

func setAllIdentiferGroups() {
	setIdentifierGroupsBuyPrice()
	setIdentifierGroupsBuyPriceRed()
	setIdentifierGroups()
	setIdentifierGroups1Account()
}

func accountIdentifyProcedure() string {
	listPixelListY := calculateDimensionsPixelObjectDigits3()
	listSeperateAccountCalculit0 := [][][]PixelObject{}
	listSeperateAccountCalculit0 = seperateAccountCalculit0Updated(listPixelListY)

	digitContainer := [][][]int{}
	for indexDigit, digitInQuestion := range listSeperateAccountCalculit0 {
		digitContainer = append(digitContainer, [][]int{})
		for indexPixel, pixelList := range digitInQuestion {
			if indexPixel == 7 {
				break
			}
			digitContainer[len(digitContainer)-1] = append(digitContainer[len(digitContainer)-1], []int{})
			for indexPixel1, pixel := range pixelList {
				intColor := 0
				if s, err := strconv.Atoi(pixel.RedValue); err == nil {
					intColor = s
				}
				if intColor > 150 {
					digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1] = append(digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1], indexPixel1)
				}
			}
			indexPixel++
		}
		indexDigit += 1
	}
	for i, v := range digitContainer {
		fmt.Println(v)
		i += 1
	}
	listResultsDigits := []int{}
	for i, v := range digitContainer {
		indexMatching := -1
		for indexListPixelCharacter, listPixelCharacter := range identifierGroupAccount {
			isMatchPixel := true
			for index1, listValues := range listPixelCharacter {
				if len(listValues) != len(v[index1]) {
					isMatchPixel = false
				}
			}
			if isMatchPixel {
				indexMatching = indexListPixelCharacter
				break
			}
		}
		listResultsDigits = append(listResultsDigits, indexMatching)
		i += 1
	}
	fmt.Println("listResultsDigits")
	fmt.Println(listResultsDigits)
	stringToWrite := ""
	stringToWrite = strconv.Itoa(listResultsDigits[0]) + strconv.Itoa(listResultsDigits[1]) + strconv.Itoa(listResultsDigits[2]) + "." + strconv.Itoa(listResultsDigits[3]) + strconv.Itoa(listResultsDigits[4])

	return stringToWrite
}

func accountIdentifyProcedureBuyProcess() string {
	listPixelListY := calculateDimensionsPixelObjectDigits3BuyProcedureForAccountIdentify()
	listSeperateAccountCalculit0 := [][][]PixelObject{}
	listSeperateAccountCalculit0 = seperateAccountCalculit0Updated(listPixelListY)

	digitContainer := [][][]int{}
	for indexDigit, digitInQuestion := range listSeperateAccountCalculit0 {
		//this just adds the container for each container of values
		digitContainer = append(digitContainer, [][]int{})
		for indexPixel, pixelList := range digitInQuestion {
			if indexPixel == 5 {
				break
			}
			digitContainer[len(digitContainer)-1] = append(digitContainer[len(digitContainer)-1], []int{})
			for indexPixel1, pixel := range pixelList {
				intColor := 0
				if s, err := strconv.Atoi(pixel.RedValue); err == nil {
					intColor = s
				}
				if intColor > 140 {
					digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1] = append(digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1], indexPixel1)
				}
			}
		}
		indexDigit += 1
	}

	fmt.Println("account")
	for i, v := range identifierGroupAccount {
		fmt.Println(v)
		fmt.Println(i)
		i += 1
	}
	for i, v := range digitContainer {
		fmt.Println(v)
		fmt.Println(i)
		i += 1
	}
	listResultsDigits := []int{}
	for i, v := range digitContainer {
		indexMatching := -1
		for indexListPixelCharacter, listPixelCharacter := range identifierGroupAccount {
			isMatchPixel := true
			for index1, listValues := range listPixelCharacter {
				if len(listValues) != len(v[index1]) {
					isMatchPixel = false
				}
			}
			if isMatchPixel {
				indexMatching = indexListPixelCharacter
				break
			}
		}
		listResultsDigits = append(listResultsDigits, indexMatching)
		i += 1
	}
	fmt.Println(listResultsDigits)
	stringToWrite := ""

	stringToWrite = strconv.Itoa(listResultsDigits[0]) + strconv.Itoa(listResultsDigits[1]) + strconv.Itoa(listResultsDigits[2]) + "." + strconv.Itoa(listResultsDigits[3]) + strconv.Itoa(listResultsDigits[4])

	fmt.Println("stringToWrite")
	fmt.Println(stringToWrite)
	return stringToWrite
}

func accountIdentifyProcedureBuyProcess1() string {
	listPixelListY := calculateDimensionsPixelObjectDigits3BuyProcedureForAccountIdentify1()
	listSeperateAccountCalculit0 := [][][]PixelObject{}
	listSeperateAccountCalculit0 = seperateAccountCalculit0Updated(listPixelListY)

	digitContainer := [][][]int{}
	for indexDigit, digitInQuestion := range listSeperateAccountCalculit0 {
		//this just adds the container for each container of values
		digitContainer = append(digitContainer, [][]int{})
		for indexPixel, pixelList := range digitInQuestion {
			if indexPixel == 5 {
				break
			}
			digitContainer[len(digitContainer)-1] = append(digitContainer[len(digitContainer)-1], []int{})
			for indexPixel1, pixel := range pixelList {
				intColor := 0
				if s, err := strconv.Atoi(pixel.RedValue); err == nil {
					intColor = s
				}
				if intColor > 140 {
					digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1] = append(digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1], indexPixel1)
				}
			}
		}
		indexDigit += 1
	}

	fmt.Println("account")
	for i, v := range identifierGroupAccount {
		fmt.Println(v)
		fmt.Println(i)
		i += 1
	}
	for i, v := range digitContainer {
		fmt.Println(v)
		fmt.Println(i)
		i += 1
	}

	listResultsDigits := []int{}
	for i, v := range digitContainer {
		indexMatching := -1
		for indexListPixelCharacter, listPixelCharacter := range identifierGroupAccount {
			isMatchPixel := true
			for index1, listValues := range listPixelCharacter {
				if len(listValues) != len(v[index1]) {
					isMatchPixel = false
				}
			}
			if isMatchPixel {
				indexMatching = indexListPixelCharacter
				break
			}
		}
		listResultsDigits = append(listResultsDigits, indexMatching)
		i += 1
	}
	fmt.Println(listResultsDigits)
	stringToWrite := ""
	stringToWrite = strconv.Itoa(listResultsDigits[0]) + strconv.Itoa(listResultsDigits[1]) + strconv.Itoa(listResultsDigits[2]) + "." + strconv.Itoa(listResultsDigits[3]) + strconv.Itoa(listResultsDigits[4])

	fmt.Println("stringToWrite")
	fmt.Println(stringToWrite)
	return stringToWrite
}

func writeToAccountPriceFile(statusString string) {
	writeToFile(`Z:\sharedFolder\`, "accountPrice.txt", statusString)
}
func calculateCommaAccountValue(listPixelListY [][]PixelObject) int {
	isComma1 := false
	//this will be in question identification later for comma seperation, in seperateCharacter segment
	listCharacterPixelLists := [][][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 825 {
				break
			}
			if YValue > 823 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}
	listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
	for index, listPixelObjects := range listCharacterPixelLists0 {
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 785 {
			if XValue == 788 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	intColor := 0
	if s, err := strconv.Atoi(listCharacterPixelLists[0][1][0].RedValue); err == nil {
		intColor = s
	}
	if intColor > 140 {
		isComma1 = true
	}

	isComma2 := false
	if isComma1 == false {
		listCharacterPixelLists = [][][]PixelObject{}
		listCharacterPixelLists0 = [][]PixelObject{}
		for index, listPixelObjects := range listPixelListY {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 825 {
					break
				}
				if YValue > 823 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
			index += 1
		}
		listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
		for index, listPixelObjects := range listCharacterPixelLists0 {
			XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
			if err != nil {
			}
			if XValue > 779 {
				if XValue == 781 {
					break
				}
				listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
			}
			index += 1
		}
		intColor = 0
		if s, err := strconv.Atoi(listCharacterPixelLists[0][0][0].RedValue); err == nil {
			intColor = s
		}
		if intColor > 140 {
			isComma2 = true
		}
	}
	returningValue := 0
	if isComma1 {
		fmt.Println("is 10000")
		returningValue = 2
	}
	if isComma2 {
		fmt.Println("is 1000")
		returningValue = 1
	}
	return returningValue
}

func phase1PriceSearchReset() {
	postDeleteScreenshots()
	postDoRecycle()
}

var indexPostMarketPriceReadByFileName3 = 0
var priceFileName = ""
var nextMinuteToStore = 0
var isNoHandleMinuteProcedure = false

var nextMinuteToStorePicture = 0
var isNoHandleMinuteProcedurePicture = false
var indexPictureIncrement = 0

func phase1PriceSearch() {
	// isOperationCheckPoint = false
	// year, month, day := getDate()
	// stringDay := strconv.Itoa(day)
	// stringMonth := strconv.Itoa(month)
	// stringYear := strconv.Itoa(year)

	// hour := getCurrentHour()
	// minute := getCurrentMinute()
	// second := getCurrentSecond()
	// stringHour := strconv.Itoa(hour)
	// stringMinute := strconv.Itoa(minute)
	// stringSecond := strconv.Itoa(second)

	// if indexPostMarketPriceReadByFileName3 == 0 {
	// 	nextMinuteToStore = minute + 20
	// 	if nextMinuteToStore > 60 {
	// 		nextMinuteToStore = nextMinuteToStore - 60
	// 	}
	// 	isNoHandleMinuteProcedure = true
	// 	nextMinuteToStorePicture = minute + 10
	// 	if nextMinuteToStorePicture > 60 {
	// 		nextMinuteToStorePicture = nextMinuteToStorePicture - 60
	// 	}
	// 	isNoHandleMinuteProcedurePicture = true
	// 	//
	// 	indexPostMarketPriceReadByFileName3 = 1
	// }

	// fmt.Println(nextMinuteToStore)
	// if minute == nextMinuteToStore && isNoHandleMinuteProcedure == false {
	// 	isNoHandleMinuteProcedure = true
	// 	nextMinuteToStore = minute + 20
	// 	if nextMinuteToStore > 60 {
	// 		nextMinuteToStore = nextMinuteToStore - 60
	// 	}
	// 	fatherFileString := readFile(`Z:\sharedFolder\cache\fatherPriceFile.txt`)
	// 	fileString := readFile(`Z:\sharedFolder\cache\priceFluxCache.txt`)
	// 	fatherFileString = fatherFileString + fileString
	// 	writeToFile(`Z:\sharedFolder\cache`, "fatherPriceFile.txt", fatherFileString)
	// 	writeToFile(`Z:\sharedFolder\cache`, "priceFluxCache.txt", "")
	// }
	// if minute != nextMinuteToStore && isNoHandleMinuteProcedure {
	// 	isNoHandleMinuteProcedure = false
	// }

	// priceFileName = "date~" + stringYear + "~" + stringMonth + "~" + stringDay + "~"
	// priceFileName += stringHour + "~" + stringMinute + "~" + stringSecond + "~"

	// // robotgo.SaveCapture("saveCapture1.png")
	// if minute == nextMinuteToStorePicture && isNoHandleMinuteProcedurePicture == false {
	// 	isNoHandleMinuteProcedurePicture = true
	// 	nextMinuteToStorePicture = minute + 10
	// 	if nextMinuteToStorePicture > 60 {
	// 		nextMinuteToStorePicture = nextMinuteToStorePicture - 60
	// 	}
	// 	stringIncrement := strconv.Itoa(indexPictureIncrement)
	// 	stringName := "./pictureHold/saveCapture" + stringIncrement + ".png"
	// 	// robotgo.SaveCapture(stringName)
	// 	indexPictureIncrement++
	// }
	// if minute != nextMinuteToStorePicture && isNoHandleMinuteProcedurePicture {
	// 	isNoHandleMinuteProcedurePicture = false
	// }

	// phase2PriceSearch()
}

var isAccountValueChanged = true

func phase2PriceSearch() {
	isOperationCheckPoint = true

	listPixelListYInQuestion := calculateDimensionsPixelObjectDigits3()

	listCharacterPixelListsInQuestionPrice := seperateCharactersLastPrice(listPixelListYInQuestion)
	listCharacterPixelListsInQuestion := seperateCharactersTicker(listPixelListYInQuestion)
	listCharacterPixelListsBuyQuestion := seperateCharactersBuyPriceUpdated(listPixelListYInQuestion)

	priceNumber := identifyPriceDigits(listCharacterPixelListsInQuestionPrice)
	tickerNumber := identifyTickerDigits(listCharacterPixelListsInQuestion)
	stringPrice := identifyBuyPriceDigits(listCharacterPixelListsBuyQuestion)

	stringToWrite := priceFileName + "~" + priceNumber + "~" + tickerNumber
	if stringPrice == "--5000" {
		stringToWrite += "~!"
	}
	if stringPrice != "--5000" {
		stringToWrite += "~" + stringPrice
	}
	writePriceFluxToFile(stringToWrite)

	if isAccountValueChanged {
		accountValueBefore = accountIdentifyProcedure()
		isAccountValueChanged = false
	}

	time.Sleep(time.Duration(2) * time.Second)
	phase1PriceSearch()
}

func postResultant(resultant string) string {
	json := `{
		"request_type": "resultant",
		"data": {
		`
	json = json + "\"resultant\":" + "\"" + resultant + "\""
	json = json + `}}`

	url := "http://localhost:3000/api/brokerage"
	response := post(url, json)
	return response
}

var boughtPriceGlobal = "0.0"
var currentPriceGlobal = "0.0"
var tickerNumberGlobal = "0"

var accountValueBefore = "0.0"
var accountValueAfter = "0.0"
var priceBought = "0.0"
var calculatedPriceSold = "0.0"

//
var indexOperationMonitorHolding = 0
var highDelimHolding = 0.0
var lowDelimHolding = 0.0

var minuteBought = 0

var isSellBoughtPriceFail = false

var globalBoughtTick = 0
var calculatedFloatBoughtPrice = 0.0

func doDigitReadForSellProcedureSecondChain() {
	// robotgo.SaveCapture("saveCapture1.png")
	time.Sleep(time.Duration(2) * time.Second)
	listPixelListYInQuestion := calculateDimensionsPixelObjectDigits3()
	listCharacterPixelListsInQuestionPrice := seperateCharactersLastPrice(listPixelListYInQuestion)
	currentPriceGlobal = identifyPriceDigits(listCharacterPixelListsInQuestionPrice)
	floatCurrentPrice := 0.0
	if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
		floatCurrentPrice = s
	}
	if indexOperationMonitorHolding == 0 {
		highDelimDifference := 3.0
		lowDelimDifference := -10.0

		// //rock
		calculatedFloatBoughtPrice = boughtPriceSecondChain

		if directionTradeSecondChain == "negative" {
			highDelimHolding = calculatedFloatBoughtPrice - highDelimDifference
			lowDelimHolding = calculatedFloatBoughtPrice - lowDelimDifference
		}
		if directionTradeSecondChain == "positive" {
			highDelimHolding = calculatedFloatBoughtPrice + highDelimDifference
			lowDelimHolding = calculatedFloatBoughtPrice + lowDelimDifference
		}
		indexOperationMonitorHolding++
	}

	//
	isCurrent0 := false
	if floatCurrentPrice == 0 || floatCurrentPrice == 0.0 {
		storePictureCurrentPriceAnamolly()
		isCurrent0 = true
		fmt.Println("in anamolly current")
	}

	// if isSellBoughtPriceFail == false {
	//check level conditions
	// floatCurrentPrice := 0.0
	// if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
	// 	floatCurrentPrice = s
	// }
	// checkLevelConditions(floatCurrentPrice)
	fmt.Println("isSellBoughtPriceFail == false ")
	//compare if current float is in range.
	fmt.Println("floatCurrentPrice")
	fmt.Println(floatCurrentPrice)
	fmt.Println("highDelimHolding")
	fmt.Println(highDelimHolding)
	fmt.Println("lowDelimHolding")
	fmt.Println(lowDelimHolding)

	// if isCurrent0 == false {
	// 	if directionTradeSecondChain == "positive" {
	// 		if floatCurrentPrice >= highDelimHolding || floatCurrentPrice <= lowDelimHolding {
	// 			fmt.Println("wrapping0 posi")
	// 			doSellWrapUpProcedureSecondChain()
	// 		}
	// 	}
	// 	if directionTradeSecondChain == "negative" {
	// 		if floatCurrentPrice <= highDelimHolding || floatCurrentPrice >= lowDelimHolding {
	// 			//do sell procedure and wrap up account read at sell procedure
	// 			fmt.Println("wrapping0 negi")
	// 			doSellWrapUpProcedureSecondChain()
	// 		}
	// 	}
	// }
	if isCurrent0 == false {
		if buyRecomendation == "positive" {
			if floatCurrentPrice >= highDelimHolding {
				fmt.Println("wrapping0 posi")
				doSellWrapUpProcedureSecondChain()
			}

			if floatCurrentPrice <= lowDelimHolding {
				fmt.Println("wrapping0 posi fail")
				doSellWrapUpProcedureSecondChainFail()
			}
		}

		if buyRecomendation == "negative" {
			if floatCurrentPrice <= highDelimHolding {
				//do sell procedure and wrap up account read at sell procedure
				fmt.Println("wrapping0 negi")
				doSellWrapUpProcedureSecondChain()
			}

			if floatCurrentPrice >= lowDelimHolding {
				//do sell procedure and wrap up account read at sell procedure
				fmt.Println("wrapping0 negi fail")
				doSellWrapUpProcedureSecondChainFail()
			}
		}
	}
	// }
	// if isSellBoughtPriceFail {
	// 	fmt.Println("wrapping")
	// 	doSellWrapUpProcedureSecondChainFail()
	// }
}
func doDigitReadForSellProcedureThirdChain() {
	// robotgo.SaveCapture("saveCapture1.png")
	time.Sleep(time.Duration(2) * time.Second)
	listPixelListYInQuestion := calculateDimensionsPixelObjectDigits3()
	listCharacterPixelListsInQuestionPrice := seperateCharactersLastPrice(listPixelListYInQuestion)
	currentPriceGlobal = identifyPriceDigits(listCharacterPixelListsInQuestionPrice)
	floatCurrentPrice := 0.0
	if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
		floatCurrentPrice = s
	}
	if indexOperationMonitorHolding == 0 {
		highDelimDifference := 3.0
		lowDelimDifference := -10.0
		calculatedFloatBoughtPrice = boughtPriceSecondChain

		if directionTradeSecondChain == "negative" {
			highDelimHolding = calculatedFloatBoughtPrice - highDelimDifference
			lowDelimHolding = calculatedFloatBoughtPrice - lowDelimDifference
		}
		if directionTradeSecondChain == "positive" {
			highDelimHolding = calculatedFloatBoughtPrice + highDelimDifference
			lowDelimHolding = calculatedFloatBoughtPrice + lowDelimDifference
		}
		indexOperationMonitorHolding++
	}

	//
	isCurrent0 := false
	if floatCurrentPrice == 0 || floatCurrentPrice == 0.0 {
		storePictureCurrentPriceAnamolly()
		isCurrent0 = true
		fmt.Println("in anamolly current")
	}

	fmt.Println("isSellBoughtPriceFail == false ")
	//compare if current float is in range.
	fmt.Println("floatCurrentPrice")
	fmt.Println(floatCurrentPrice)
	fmt.Println("highDelimHolding")
	fmt.Println(highDelimHolding)
	fmt.Println("lowDelimHolding")
	fmt.Println(lowDelimHolding)

	// if isCurrent0 == false {
	// 	if directionTradeSecondChain == "positive" {
	// 		if floatCurrentPrice >= highDelimHolding || floatCurrentPrice <= lowDelimHolding {
	// 			fmt.Println("wrapping0 posi")
	// 			doSellWrapUpProcedureThirdChain()
	// 		}
	// 	}
	// 	if directionTradeSecondChain == "negative" {
	// 		if floatCurrentPrice <= highDelimHolding || floatCurrentPrice >= lowDelimHolding {
	// 			//do sell procedure and wrap up account read at sell procedure
	// 			fmt.Println("wrapping0 negi")
	// 			doSellWrapUpProcedureThirdChain()
	// 		}
	// 	}
	// }

	if isCurrent0 == false {
		if buyRecomendation == "positive" {
			if floatCurrentPrice >= highDelimHolding {
				fmt.Println("wrapping0 posi")
				doSellWrapUpProcedureThirdChain()
			}

			if floatCurrentPrice <= lowDelimHolding {
				fmt.Println("wrapping0 posi fail")
				doSellWrapUpProcedureThirdChain()
			}
		}

		if buyRecomendation == "negative" {
			if floatCurrentPrice <= highDelimHolding {
				//do sell procedure and wrap up account read at sell procedure
				fmt.Println("wrapping0 negi")
				doSellWrapUpProcedureThirdChain()
			}

			if floatCurrentPrice >= lowDelimHolding {
				//do sell procedure and wrap up account read at sell procedure
				fmt.Println("wrapping0 negi fail")
				doSellWrapUpProcedureThirdChain()
			}
		}
	}

	// if isSellBoughtPriceFail {
	// 	fmt.Println("wrapping")
	// 	doSellWrapUpProcedureSecondChainFail()
	// }
}

func doDigitReadForSellProcedure() {
	// robotgo.SaveCapture("saveCapture1.png")
	time.Sleep(time.Duration(2) * time.Second)
	listPixelListYInQuestion := calculateDimensionsPixelObjectDigits3()
	listCharacterPixelListsInQuestionPrice := seperateCharactersLastPrice(listPixelListYInQuestion)
	listCharacterPixelListsBuyQuestion := seperateCharactersBuyPriceUpdated(listPixelListYInQuestion)
	currentPriceGlobal = identifyPriceDigits(listCharacterPixelListsInQuestionPrice)
	if indexOperationMonitorHolding == 0 {
		tickBoughtPrice := identifyBuyPriceDigits(listCharacterPixelListsBuyQuestion)
		fmt.Println("tickBoughtPrice")
		fmt.Println(tickBoughtPrice)
		if tickBoughtPrice == "--5000" {
			isSellBoughtPriceFail = true
		}
		if tickBoughtPrice != "--5000" {
			floatCurrentPrice := 0.0
			if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
				floatCurrentPrice = s
			}
			intFloatBoughtPrice := 0
			if s, err := strconv.Atoi(tickBoughtPrice); err == nil {
				intFloatBoughtPrice = s
			}
			numberOfTicks := intFloatBoughtPrice
			calculatedFloatBoughtPrice = floatCurrentPrice
			indexCalcTicks := 0
			// isCalcTicks := true
			fmt.Println("numberOfTicks")
			fmt.Println(numberOfTicks)
			fmt.Println(indexCalcTicks)
			fmt.Println("isBoughtPriceTicksRed")
			fmt.Println(isBoughtPriceTicksRed)
			globalBoughtTick = numberOfTicks
			for numberOfTicks != 0 {
				if numberOfTicks < 0 {
					calculatedFloatBoughtPrice += .25
					numberOfTicks++
					continue
				}
				calculatedFloatBoughtPrice -= .25
				numberOfTicks--
			}
			// for isCalcTicks {
			// 	if indexCalcTicks == numberOfTicks {
			// 		break
			// 	}
			// 	//red
			// 	if isBoughtPriceTicksRed {
			// 		calculatedFloatBoughtPrice += .25
			// 		indexCalcTicks++
			// 	}
			// 	//green
			// 	if isBoughtPriceTicksRed == false {
			// 		calculatedFloatBoughtPrice -= .25
			// 		indexCalcTicks--
			// 	}
			// 	// if numberOfTicks > 0 {
			// 	// 	indexCalcTicks++
			// 	// }
			// 	// if numberOfTicks < 0 {
			// 	// 	indexCalcTicks--
			// 	// }
			// }
			//levels set
			// setThresholdForBoughtPrice(calculatedFloatBoughtPrice)
			// calculatedFloatBoughtPrice
			// calculatedFloatBoughtPrice
			// minuteBought = getCurrentMinute()
			// highDelimDifference := 5.0
			// lowDelimDifference := -30.0
			// highDelimDifference := 1.0
			// lowDelimDifference := -2.0

			// highDelimDifference := 6.0
			// lowDelimDifference := -5.0

			highDelimDifference := 1.0
			lowDelimDifference := -10.0

			// highDelimDifference := 5.0
			// lowDelimDifference := -12.0
			//
			// lowDelimDifference := -15.0
			// if isDoLesserTrade {
			// 	highDelimDifference = 7.0
			// 	lowDelimDifference = -7.0
			// }
			// if isDoLesserTrade == false {
			// 	highDelimDifference = 15.0
			// 	lowDelimDifference = -10.0
			// }
			// //
			fmt.Println("calculatedFloatBoughtPrice check")
			fmt.Println(calculatedFloatBoughtPrice)
			// if isDoLesserTrade {
			// 	fmt.Println("is lesser trade")
			// }
			if buyRecomendation == "negative" {
				highDelimHolding = calculatedFloatBoughtPrice - highDelimDifference
				lowDelimHolding = calculatedFloatBoughtPrice - lowDelimDifference
			}
			if buyRecomendation == "positive" {
				highDelimHolding = calculatedFloatBoughtPrice + highDelimDifference
				lowDelimHolding = calculatedFloatBoughtPrice + lowDelimDifference
			}
			indexOperationMonitorHolding++
		}
	}

	//then reoccuring check if level conditions met.

	fmt.Println("calculatedFloatBoughtPrice check")
	fmt.Println(calculatedFloatBoughtPrice)
	fmt.Println("globalBoughtTick")
	fmt.Println(globalBoughtTick)

	if isSellBoughtPriceFail == false {
		//check level conditions
		floatCurrentPrice := 0.0
		if s, err := strconv.ParseFloat(currentPriceGlobal, 64); err == nil {
			floatCurrentPrice = s
		}

		isCurrent0 := false
		if floatCurrentPrice == 0 || floatCurrentPrice == 0.0 {
			storePictureCurrentPriceAnamolly()
			isCurrent0 = true
			fmt.Println("in anamolly current")
			fmt.Println("isCurrent0 == true")
		}
		// checkLevelConditions(floatCurrentPrice)
		fmt.Println("isSellBoughtPriceFail == false ")
		//compare if current float is in range.
		fmt.Println("floatCurrentPrice")
		fmt.Println(floatCurrentPrice)
		fmt.Println("highDelimHolding")
		fmt.Println(highDelimHolding)
		fmt.Println("lowDelimHolding")
		fmt.Println(lowDelimHolding)
		if isCurrent0 == false {
			fmt.Println("isCurrent0 == false")
			if buyRecomendation == "positive" {
				if floatCurrentPrice >= highDelimHolding {
					fmt.Println("wrapping0 posi")
					doSellWrapUpProcedure()
				}

				if floatCurrentPrice <= lowDelimHolding {
					fmt.Println("wrapping0 posi fail")
					doSellWrapUpProcedureFail()
				}
			}

			if buyRecomendation == "negative" {
				if floatCurrentPrice <= highDelimHolding {
					//do sell procedure and wrap up account read at sell procedure
					fmt.Println("wrapping0 negi")
					doSellWrapUpProcedure()
				}

				if floatCurrentPrice >= lowDelimHolding {
					//do sell procedure and wrap up account read at sell procedure
					fmt.Println("wrapping0 negi fail")
					doSellWrapUpProcedureFail()
				}
			}
		}
	}
	if isSellBoughtPriceFail {
		fmt.Println("wrapping")
		doSellWrapUpProcedureFail()
	}
}

func storePictureCurrentPriceAnamolly() {
	//store picture
	// out, err := exec.Command("cmd", "/c", "cd /d Z:\\sharedFolder\\dayCandleStore\\ && mkdir "+folderName).Output()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// s := string(out)
	// fmt.Println(s)

	//move "%USERPROFILE%\Downloads\Test.exe" "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"
}

func checkLevelConditions(floatCurrentPrice float64) {

	//check and update positive conditions,
	for i, v := range boughtPriceLevelObject.ListLevelPositive {
		if v.IsReachedPositiveDirection == false {
			if floatCurrentPrice >= v.Price {
				boughtPriceLevelObject.ListLevelPositive[i].IsReachedPositiveDirection = true
				//increment value of buy measurement
			}
		}
		if v.IsReachedPositiveDirection {
			// if v.Price {
			// }
			i++
			continue
		}
		i++
		break
	}

	for i, v := range boughtPriceLevelObject.ListLevelPositive {
		if v.IsReachedNegativeDirection == false {
			if floatCurrentPrice <= v.Price {
				boughtPriceLevelObject.ListLevelNegative[i].IsReachedNegativeDirection = true
				//increment value of buy measurement
			}
		}
		if v.IsReachedNegativeDirection {
			// if v.Price {
			// }
			i++
			continue
		}
		i++
		break
	}

	//check and update negative conditions

}

func doOperationFUVRHead() {
	setAllIdentiferGroups()
	go doCycleStorePictureAtInterval()
	go doOperateItterateCalculateBuy()
}

func doSellWrapUpProcedure() {
	fmt.Println("closing")
	closeOperation()
	time.Sleep(time.Duration(10) * time.Second)
	itterateItterateCacheValue()
	// robotgo.SaveCapture("./saveCapture11.png")
	accountValueAfter = accountIdentifyProcedureBuyProcess1()
	//handle on bought difference
	floatAccountBefore := 0.0
	if s, err := strconv.ParseFloat(accountValueBefore, 64); err == nil {
		floatAccountBefore = s
	}
	floatAccountAfter := 0.0
	if s, err := strconv.ParseFloat(accountValueAfter, 64); err == nil {
		floatAccountAfter = s
	}
	accountDifference := floatAccountBefore - floatAccountAfter
	resultOfTest := "null"
	if accountDifference > 0 {
		resultOfTest = "positive"
		// postResultant("1")
	}
	if accountDifference < 0 {
		resultOfTest = "negative"
		// postResultant("0")
	}

	//
	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	nameOfTest := ",itterateCacheValueIndexEnd:" + globalBuyProcessVRItterate + "~" + "dateAndTime:" + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringMinute + "~" + stringHour + "EndDateAndTime"

	stringMinuteBought := strconv.Itoa(minuteBought)
	//gotta read file first.
	informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	isAccountValueChanged = true
	isOperateMonitorHolding = false

	// informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)
	directionTradeSecondChain := "0"
	if buyRecomendation == "negative" {
		directionTradeSecondChain = "positive"
	}
	if buyRecomendation == "positive" {
		directionTradeSecondChain = "negative"
	}

	boughtPriceSecondChain := fmt.Sprintf("%.2f", calculatedFloatBoughtPrice)
	dayOfWeek := getDay()
	stringDay1 := strconv.Itoa(dayOfWeek)
	informationString1 := "," + "1" + "~" + directionTradeSecondChain + "~" + boughtPriceSecondChain + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr1`, "secondChainFile.txt", informationString1)
	doFuvrRestart()
	os.Exit(1)
}

func doSellWrapUpProcedureFail() {
	fmt.Println("closing")
	closeOperation()
	time.Sleep(time.Duration(10) * time.Second)
	itterateItterateCacheValue()
	// robotgo.SaveCapture("./saveCapture11.png")
	accountValueAfter = accountIdentifyProcedureBuyProcess1()
	//handle on bought difference
	floatAccountBefore := 0.0
	if s, err := strconv.ParseFloat(accountValueBefore, 64); err == nil {
		floatAccountBefore = s
	}
	floatAccountAfter := 0.0
	if s, err := strconv.ParseFloat(accountValueAfter, 64); err == nil {
		floatAccountAfter = s
	}
	accountDifference := floatAccountBefore - floatAccountAfter
	resultOfTest := "null"
	if accountDifference > 0 {
		resultOfTest = "positive"
		// postResultant("1")
	}
	if accountDifference < 0 {
		resultOfTest = "negative"
		// postResultant("0")
	}

	//
	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	nameOfTest := ",itterateCacheValueIndexEnd:" + globalBuyProcessVRItterate + "~" + "dateAndTime:" + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringMinute + "~" + stringHour + "EndDateAndTime"

	stringMinuteBought := strconv.Itoa(minuteBought)
	//gotta read file first.
	informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr1`, "testRecordFile.txt", informationString)

	isAccountValueChanged = true
	isOperateMonitorHolding = false

	// informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)
	directionTradeSecondChain := "0"
	if buyRecomendation == "negative" {
		directionTradeSecondChain = "positive"
	}
	if buyRecomendation == "positive" {
		directionTradeSecondChain = "negative"
	}

	boughtPriceSecondChain := fmt.Sprintf("%.2f", calculatedFloatBoughtPrice)
	dayOfWeek := getDay()
	stringDay1 := strconv.Itoa(dayOfWeek)
	informationString1 := "," + "0" + "~" + directionTradeSecondChain + "~" + boughtPriceSecondChain + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "secondChainFile.txt", informationString1)
	doFuvrRestart()
	os.Exit(1)
}

func doSellWrapUpProcedureSecondChain() {
	fmt.Println("closing")
	closeOperation()
	time.Sleep(time.Duration(10) * time.Second)
	itterateItterateCacheValue()
	// robotgo.SaveCapture("./saveCapture11.png")
	accountValueAfter = accountIdentifyProcedureBuyProcess1()
	//handle on bought difference
	floatAccountBefore := 0.0
	if s, err := strconv.ParseFloat(accountValueBefore, 64); err == nil {
		floatAccountBefore = s
	}
	floatAccountAfter := 0.0
	if s, err := strconv.ParseFloat(accountValueAfter, 64); err == nil {
		floatAccountAfter = s
	}
	accountDifference := floatAccountBefore - floatAccountAfter
	resultOfTest := "null"
	if accountDifference > 0 {
		resultOfTest = "positive"
		// postResultant("1")
	}
	if accountDifference < 0 {
		resultOfTest = "negative"
		// postResultant("0")
	}

	//
	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	nameOfTest := ",itterateCacheValueIndexEnd:" + globalBuyProcessVRItterate + "~" + "dateAndTime:" + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringMinute + "~" + stringHour + "EndDateAndTime"

	stringMinuteBought := strconv.Itoa(minuteBought)
	//gotta read file first.
	informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	isAccountValueChanged = true
	isOperateMonitorHolding = false

	// informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	directionTradeSecondChain := "0"
	// if buyRecomendation == "negative" {
	// 	directionTradeSecondChain = "negative"
	// }
	// if buyRecomendation == "positive" {
	// 	directionTradeSecondChain = "positive"
	// }

	// directionTradeSecondChain := "0"
	if buyRecomendation == "negative" {
		directionTradeSecondChain = "positive"
	}
	if buyRecomendation == "positive" {
		directionTradeSecondChain = "negative"
	}

	dayOfWeek := getDay()

	stringDay1 := strconv.Itoa(dayOfWeek)

	informationString1 := "," + "1" + "~" + directionTradeSecondChain + "~" + "0.0" + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "thirdChainFile.txt", informationString1)

	informationString2 := "," + "0" + "~" + directionTradeSecondChain + "~" + "0.0" + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "secondChainFile.txt", informationString2)

	doFuvrRestart()
	os.Exit(1)
}
func doSellWrapUpProcedureSecondChainFail() {
	fmt.Println("closing")
	closeOperation()
	time.Sleep(time.Duration(10) * time.Second)
	itterateItterateCacheValue()
	// robotgo.SaveCapture("./saveCapture11.png")
	accountValueAfter = accountIdentifyProcedureBuyProcess1()
	//handle on bought difference
	floatAccountBefore := 0.0
	if s, err := strconv.ParseFloat(accountValueBefore, 64); err == nil {
		floatAccountBefore = s
	}
	floatAccountAfter := 0.0
	if s, err := strconv.ParseFloat(accountValueAfter, 64); err == nil {
		floatAccountAfter = s
	}
	accountDifference := floatAccountBefore - floatAccountAfter
	resultOfTest := "null"
	if accountDifference > 0 {
		resultOfTest = "positive"
		postResultant("1")
	}
	if accountDifference < 0 {
		resultOfTest = "negative"
		postResultant("0")
	}

	//
	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	nameOfTest := ",itterateCacheValueIndexEnd:" + globalBuyProcessVRItterate + "~" + "dateAndTime:" + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringMinute + "~" + stringHour + "EndDateAndTime"

	stringMinuteBought := strconv.Itoa(minuteBought)
	//gotta read file first.
	informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	isAccountValueChanged = true
	isOperateMonitorHolding = false

	// informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	directionTradeSecondChain := "0"
	// if buyRecomendation == "negative" {
	// 	directionTradeSecondChain = "negative"
	// }
	// if buyRecomendation == "positive" {
	// 	directionTradeSecondChain = "positive"
	// }

	// directionTradeSecondChain := "0"
	if buyRecomendation == "negative" {
		directionTradeSecondChain = "positive"
	}
	if buyRecomendation == "positive" {
		directionTradeSecondChain = "negative"
	}

	dayOfWeek := getDay()

	stringDay1 := strconv.Itoa(dayOfWeek)

	informationString1 := "," + "0" + "~" + directionTradeSecondChain + "~" + "0.0" + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "thirdChainFile.txt", informationString1)

	informationString2 := "," + "0" + "~" + directionTradeSecondChain + "~" + "0.0" + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "secondChainFile.txt", informationString2)

	doFuvrRestart()
	os.Exit(1)
}

func doSellWrapUpProcedureThirdChain() {
	fmt.Println("closing")
	closeOperation()
	time.Sleep(time.Duration(10) * time.Second)
	itterateItterateCacheValue()
	// robotgo.SaveCapture("./saveCapture11.png")
	accountValueAfter = accountIdentifyProcedureBuyProcess1()
	//handle on bought difference
	floatAccountBefore := 0.0
	if s, err := strconv.ParseFloat(accountValueBefore, 64); err == nil {
		floatAccountBefore = s
	}
	floatAccountAfter := 0.0
	if s, err := strconv.ParseFloat(accountValueAfter, 64); err == nil {
		floatAccountAfter = s
	}
	accountDifference := floatAccountBefore - floatAccountAfter
	resultOfTest := "null"
	if accountDifference > 0 {
		resultOfTest = "positive"
		postResultant("1")
	}
	if accountDifference < 0 {
		resultOfTest = "negative"
		postResultant("0")
	}

	//
	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	nameOfTest := ",itterateCacheValueIndexEnd:" + globalBuyProcessVRItterate + "~" + "dateAndTime:" + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringMinute + "~" + stringHour + "EndDateAndTime"

	stringMinuteBought := strconv.Itoa(minuteBought)
	//gotta read file first.
	informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	isAccountValueChanged = true
	isOperateMonitorHolding = false

	// informationString := nameOfTest + "~" + "minuteBought:" + stringMinuteBought + "~" + resultOfTest + "~" + boughtPriceGlobal + "~" + "accountValueBefore:" + accountValueBefore + "~" + "accountValueAfter:" + accountValueAfter + "~"
	// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "testRecordFile.txt", informationString)

	// directionTradeThirdChain := "0"
	// if buyRecomendation == "negative" {
	// 	directionTradeSecondChain = "negative"
	// }
	// if buyRecomendation == "positive" {
	// 	directionTradeSecondChain = "positive"
	// }

	directionTradeThirdChain := "0"
	// if buyRecomendation == "negative" {
	// 	directionTradeSecondChain = "negative"
	// }
	// if buyRecomendation == "positive" {
	// 	directionTradeSecondChain = "positive"
	// }

	// directionTradeSecondChain := "0"
	if buyRecomendation == "negative" {
		directionTradeThirdChain = "positive"
	}
	if buyRecomendation == "positive" {
		directionTradeThirdChain = "negative"
	}

	dayOfWeek := getDay()

	stringDay1 := strconv.Itoa(dayOfWeek)

	informationString1 := "," + "0" + "~" + directionTradeThirdChain + "~" + "0.0" + "~" + stringDay1
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "thirdChainFile.txt", informationString1)

	doFuvrRestart()
	os.Exit(1)
}

// var isDoSecondChainCalculated := "0"

func readCache() string {
	fileString := readFile(`Z:\sharedFolder\cache\priceFluxCache.txt`)
	return fileString
}
func readGetLatestPictureEntry() {
	//
}

func getDate() (int, int, int) {
	currentTime := time.Now()
	yr, mt, day := currentTime.Date()
	intMonth := int(mt)
	fmt.Println(yr, mt, day)
	return yr, intMonth, day
}
func getDay() int {
	currentTime := time.Now()
	yr, mt, day := currentTime.Date()
	//intMonth := int(mt)
	fmt.Println(yr, mt, day)
	return day
}

func getDayOfWeek() string {
	currentTime := time.Now()
	day := currentTime.Weekday()

	stringDay := day.String()

	return stringDay
}

var operationIndexWritePriceFluxToFile = 0

func writePriceFluxToFile(priceFlux string) {
	fileString := readFile(`Z:\sharedFolder\cache\priceFluxCache.txt`)
	modifiedFileString := ""
	if len(fileString) > 3 {
		modifiedFileString += fileString + ","
	}
	modifiedFileString += priceFlux

	writeToFile(`Z:\sharedFolder\cache`, "priceFluxCache.txt", modifiedFileString)
	operationIndexWritePriceFluxToFile += 1
}

func getCurrentHour() int {
	hour := time.Now().Hour()
	return hour
}
func getCurrentMinute() int {
	minute := time.Now().Minute()
	return minute
}
func getCurrentSecond() int {
	second := time.Now().Second()
	return second
}

func writeToFile(path string, fileName string, data string) {
	f, err := os.Create(path + "/" + fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
	l, err := f.WriteString(data)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	fmt.Println(l, "bytes written successfully")
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

var listCharacterPixelListsBuy0 = [][]PixelObject{}
var listCharacterPixelListsBuy1 = [][]PixelObject{}
var listCharacterPixelListsBuy2 = [][]PixelObject{}
var listCharacterPixelListsBuy3 = [][]PixelObject{}
var listCharacterPixelListsBuy4 = [][]PixelObject{}
var listCharacterPixelListsBuy5 = [][]PixelObject{}
var listCharacterPixelListsBuy6 = [][]PixelObject{}
var listCharacterPixelListsBuy7 = [][]PixelObject{}
var listCharacterPixelListsBuy8 = [][]PixelObject{}
var listCharacterPixelListsBuy9 = [][]PixelObject{}
var listCharacterPixelListsQuestion = [][]PixelObject{}

var coreDigitPixelList0 = [][]int{}
var coreDigitPixelList1 = [][]int{}
var coreDigitPixelList2 = [][]int{}
var coreDigitPixelList3 = [][]int{}
var coreDigitPixelList4 = [][]int{}
var coreDigitPixelList5 = [][]int{}
var coreDigitPixelList6 = [][]int{}
var coreDigitPixelList7 = [][]int{}
var coreDigitPixelList8 = [][]int{}
var coreDigitPixelList9 = [][]int{}

//var coreDigitPixelListBuyPrice0 = [][]int{}
var coreDigitPixelListBuyPrice1 = [][]int{}
var coreDigitPixelListBuyPrice2 = [][]int{}
var coreDigitPixelListBuyPrice3 = [][]int{}
var coreDigitPixelListBuyPrice4 = [][]int{}
var coreDigitPixelListBuyPrice5 = [][]int{}
var coreDigitPixelListBuyPrice6 = [][]int{}
var coreDigitPixelListBuyPrice7 = [][]int{}
var coreDigitPixelListBuyPrice8 = [][]int{}
var coreDigitPixelListBuyPrice9 = [][]int{}

//var coreDigitPixelList0Red = [][]int{}
var coreDigitPixelListBuyPriceRed0 = [][]int{}
var coreDigitPixelListBuyPriceRed1 = [][]int{}
var coreDigitPixelListBuyPriceRed2 = [][]int{}
var coreDigitPixelListBuyPriceRed3 = [][]int{}
var coreDigitPixelListBuyPriceRed4 = [][]int{}
var coreDigitPixelListBuyPriceRed5 = [][]int{}
var coreDigitPixelListBuyPriceRed6 = [][]int{}
var coreDigitPixelListBuyPriceRed7 = [][]int{}
var coreDigitPixelListBuyPriceRed8 = [][]int{}
var coreDigitPixelListBuyPriceRed9 = [][]int{}

var coreDigitPixelListQuestion = [][]int{}

var coreDigitPixelList0Account = [][]int{}
var coreDigitPixelList1Account = [][]int{}
var coreDigitPixelList2Account = [][]int{}
var coreDigitPixelList3Account = [][]int{}
var coreDigitPixelList4Account = [][]int{}
var coreDigitPixelList5Account = [][]int{}
var coreDigitPixelList6Account = [][]int{}
var coreDigitPixelList7Account = [][]int{}
var coreDigitPixelList8Account = [][]int{}
var coreDigitPixelList9Account = [][]int{}

func setIdentifierGroups1Account() {
	//price
	// redDelmiter0 := "0.png"
	// redDelmiter1 := "1.png"
	// redDelmiter2 := "2.png"
	// redDelmiter3 := "3.png"
	// redDelmiter4 := "4.png"
	// redDelmiter5 := "5.png"
	// redDelmiter6 := "6.png"
	// redDelmiter7 := "7.png"
	// redDelmiter8 := "8.png"
	// redDelmiter9 := "9.png"

	// redDelmiter0 := "0C.png"
	// redDelmiter1 := "1C.png"
	// redDelmiter2 := "2C.png"
	// redDelmiter3 := "3C.png"
	// redDelmiter4 := "4C.png"
	// redDelmiter5 := "5C.png"
	// redDelmiter6 := "6C.png"
	// redDelmiter7 := "7C.png"
	// redDelmiter8 := "8C.png"
	// redDelmiter9 := "9C.png"

	//bay1
	//postMarketPriceReadByFileName2(redDelmiter0)
	//postMarketPriceReadByFileName2(redDelmiter1)
	//postMarketPriceReadByFileName2(redDelmiter2)
	//postMarketPriceReadByFileName2(redDelmiter3)
	//postMarketPriceReadByFileName2(redDelmiter4)
	//postMarketPriceReadByFileName2(redDelmiter5)
	//postMarketPriceReadByFileName2(redDelmiter6)
	//postMarketPriceReadByFileName2(redDelmiter7)
	//postMarketPriceReadByFileName2(redDelmiter8)
	//postMarketPriceReadByFileName2(redDelmiter9)

	// listPixelListY0 := calculateDimensionsPixelObjectDigits2(redDelmiter0)
	// listPixelListY1 := calculateDimensionsPixelObjectDigits2(redDelmiter1)
	// listPixelListY2 := calculateDimensionsPixelObjectDigits2(redDelmiter2)
	// listPixelListY3 := calculateDimensionsPixelObjectDigits2(redDelmiter3)
	// listPixelListY4 := calculateDimensionsPixelObjectDigits2(redDelmiter4)
	// listPixelListY5 := calculateDimensionsPixelObjectDigits2(redDelmiter5)
	// listPixelListY6 := calculateDimensionsPixelObjectDigits2(redDelmiter6)
	// listPixelListY7 := calculateDimensionsPixelObjectDigits2(redDelmiter7)
	// listPixelListY8 := calculateDimensionsPixelObjectDigits2(redDelmiter8)
	// listPixelListY9 := calculateDimensionsPixelObjectDigits2(redDelmiter9)

	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter0, "0AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter1, "1AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter2, "2AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter3, "3AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter4, "4AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter5, "5AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter6, "6AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter7, "7AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter8, "8AccountPrice")
	// calculateDimensionsPixelObjectDigitsStore2Account(redDelmiter9, "9AccountPrice")

	listCharacterPixelListsBuy0 = parsePixelObjectListFromStore("0AccountPrice.txt")
	listCharacterPixelListsBuy1 = parsePixelObjectListFromStore("1AccountPrice.txt")
	listCharacterPixelListsBuy2 = parsePixelObjectListFromStore("2AccountPrice.txt")
	listCharacterPixelListsBuy3 = parsePixelObjectListFromStore("3AccountPrice.txt")
	listCharacterPixelListsBuy4 = parsePixelObjectListFromStore("4AccountPrice.txt")
	listCharacterPixelListsBuy5 = parsePixelObjectListFromStore("5AccountPrice.txt")
	listCharacterPixelListsBuy6 = parsePixelObjectListFromStore("6AccountPrice.txt")
	listCharacterPixelListsBuy7 = parsePixelObjectListFromStore("7AccountPrice.txt")
	listCharacterPixelListsBuy8 = parsePixelObjectListFromStore("8AccountPrice.txt")
	listCharacterPixelListsBuy9 = parsePixelObjectListFromStore("9AccountPrice.txt")

	// listPixelListY0 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter0, "0AccountPrice.txt")
	// listPixelListY1 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter1, "1AccountPrice.txt")
	// listPixelListY2 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter2, "2AccountPrice.txt")
	// listPixelListY3 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter3, "3AccountPrice.txt")
	// listPixelListY4 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter4, "4AccountPrice.txt")
	// listPixelListY5 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter5, "5AccountPrice.txt")
	// listPixelListY6 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter6, "6AccountPrice.txt")
	// listPixelListY7 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter7, "7AccountPrice.txt")
	// listPixelListY8 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter8, "8AccountPrice.txt")
	// listPixelListY9 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter9, "9AccountPrice.txt")

	//bays1
	// listCharacterPixelListsBuy0 := seperateCharactersAccountIdentifer(listPixelListY0)
	// listCharacterPixelListsBuy1 := seperateCharactersAccountIdentifer(listPixelListY1)
	// listCharacterPixelListsBuy2 := seperateCharactersAccountIdentifer(listPixelListY2)
	// listCharacterPixelListsBuy3 := seperateCharactersAccountIdentifer(listPixelListY3)
	// listCharacterPixelListsBuy4 := seperateCharactersAccountIdentifer(listPixelListY4)
	// listCharacterPixelListsBuy5 := seperateCharactersAccountIdentifer(listPixelListY5)
	// listCharacterPixelListsBuy6 := seperateCharactersAccountIdentifer(listPixelListY6)
	// listCharacterPixelListsBuy7 := seperateCharactersAccountIdentifer(listPixelListY7)
	// listCharacterPixelListsBuy8 := seperateCharactersAccountIdentifer(listPixelListY8)
	// listCharacterPixelListsBuy9 := seperateCharactersAccountIdentifer(listPixelListY9)

	// listCharacterPixelListsBuy0 := seperateCharactersAccountIdentiferUpdated(listPixelListY0)
	// listCharacterPixelListsBuy1 := seperateCharactersAccountIdentiferUpdated(listPixelListY1)
	// listCharacterPixelListsBuy2 := seperateCharactersAccountIdentiferUpdated(listPixelListY2)
	// listCharacterPixelListsBuy3 := seperateCharactersAccountIdentiferUpdated(listPixelListY3)
	// listCharacterPixelListsBuy4 := seperateCharactersAccountIdentiferUpdated(listPixelListY4)
	// listCharacterPixelListsBuy5 := seperateCharactersAccountIdentiferUpdated(listPixelListY5)
	// listCharacterPixelListsBuy6 := seperateCharactersAccountIdentiferUpdated(listPixelListY6)
	// listCharacterPixelListsBuy7 := seperateCharactersAccountIdentiferUpdated(listPixelListY7)
	// listCharacterPixelListsBuy8 := seperateCharactersAccountIdentiferUpdated(listPixelListY8)
	// listCharacterPixelListsBuy9 := seperateCharactersAccountIdentiferUpdated(listPixelListY9)

	pixelListContainer0 := [][]int{}
	//red
	for indexPixel, pixelList := range listCharacterPixelListsBuy0 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer0 = append(pixelListContainer0, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer0[len(pixelListContainer0)-1] = append(pixelListContainer0[len(pixelListContainer0)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer1 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy1 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer1 = append(pixelListContainer1, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer1[len(pixelListContainer1)-1] = append(pixelListContainer1[len(pixelListContainer1)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer2 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy2 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer2 = append(pixelListContainer2, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer2[len(pixelListContainer2)-1] = append(pixelListContainer2[len(pixelListContainer2)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer3 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy3 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer3 = append(pixelListContainer3, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer3[len(pixelListContainer3)-1] = append(pixelListContainer3[len(pixelListContainer3)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer4 := [][]int{}
	//red
	for indexPixel, pixelList := range listCharacterPixelListsBuy4 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer4 = append(pixelListContainer4, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer4[len(pixelListContainer4)-1] = append(pixelListContainer4[len(pixelListContainer4)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer5 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy5 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer5 = append(pixelListContainer5, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer5[len(pixelListContainer5)-1] = append(pixelListContainer5[len(pixelListContainer5)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer6 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy6 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer6 = append(pixelListContainer6, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer6[len(pixelListContainer6)-1] = append(pixelListContainer6[len(pixelListContainer6)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer7 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy7 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer7 = append(pixelListContainer7, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer7[len(pixelListContainer7)-1] = append(pixelListContainer7[len(pixelListContainer7)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer8 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy8 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer8 = append(pixelListContainer8, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer8[len(pixelListContainer8)-1] = append(pixelListContainer8[len(pixelListContainer8)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer9 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy9 {
		if indexPixel == 0 {
			continue
		}
		pixelListContainer9 = append(pixelListContainer9, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer9[len(pixelListContainer9)-1] = append(pixelListContainer9[len(pixelListContainer9)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	fmt.Println("pixelListContainer0")
	fmt.Println(pixelListContainer0)
	fmt.Println("pixelListContainer1")
	fmt.Println(pixelListContainer1)
	fmt.Println("pixelListContainer2")
	fmt.Println(pixelListContainer2)
	fmt.Println("pixelListContainer3")
	fmt.Println(pixelListContainer3)
	fmt.Println("pixelListContainer4")
	fmt.Println(pixelListContainer4)
	fmt.Println("pixelListContainer5")
	fmt.Println(pixelListContainer5)
	fmt.Println("pixelListContainer6")
	fmt.Println(pixelListContainer6)
	fmt.Println("pixelListContainer7")
	fmt.Println(pixelListContainer7)
	fmt.Println("pixelListContainer8")
	fmt.Println(pixelListContainer8)
	fmt.Println("pixelListContainer9")
	fmt.Println(pixelListContainer9)

	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer0)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer1)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer2)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer3)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer4)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer5)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer6)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer7)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer8)
	identifierGroupAccount = append(identifierGroupAccount, pixelListContainer9)

	//coreDigitPixelList0Red = calculateSingleCoreDigit(pixelListContainer0)
	//coreDigitPixelList1Red = calculateSingleCoreDigit(pixelListContainer1)
	//coreDigitPixelList2Red = calculateSingleCoreDigit(pixelListContainer2)
	//coreDigitPixelList3Red = calculateSingleCoreDigit(pixelListContainer3)
	//coreDigitPixelList4Red = calculateSingleCoreDigit(pixelListContainer4)
	//coreDigitPixelList5Red = calculateSingleCoreDigit(pixelListContainer5)
	//coreDigitPixelList6Red = calculateSingleCoreDigit(pixelListContainer6)
	//coreDigitPixelList7Red = calculateSingleCoreDigit(pixelListContainer7)
	//coreDigitPixelList8Red = calculateSingleCoreDigit(pixelListContainer8)
	//coreDigitPixelList9Red = calculateSingleCoreDigit(pixelListContainer9)

}

var identifierGroupAccount = [][][]int{}

func setIdentifierGroupsBuyPriceRed() {
	//price
	// redDelmiter0 := "0BuyPrice.png"
	// redDelmiter1 := "-1BuyPrice.png"
	// redDelmiter2 := "-2BuyPrice.png"
	// redDelmiter3 := "-3BuyPrice.png"
	// redDelmiter4 := "-4BuyPrice.png"
	// redDelmiter5 := "-5BuyPrice.png"
	// redDelmiter6 := "-6BuyPrice.png"
	// redDelmiter7 := "-7BuyPrice.png"
	// redDelmiter8 := "-8BuyPrice.png"
	// redDelmiter9 := "-9BuyPrice.png"

	//doubleNeg := "date~2021~1~20~20~8~9~"
	//red
	// postMarketPriceReadByFileName2(redDelmiter0)
	// postMarketPriceReadByFileName2(redDelmiter1)
	// postMarketPriceReadByFileName2(redDelmiter2)
	// postMarketPriceReadByFileName2(redDelmiter3)
	//postMarketPriceReadByFileName2(redDelmiter4)
	// postMarketPriceReadByFileName2(redDelmiter5)
	// postMarketPriceReadByFileName2(redDelmiter6)
	// postMarketPriceReadByFileName2(redDelmiter7)
	// postMarketPriceReadByFileName2(redDelmiter8)
	// postMarketPriceReadByFileName2(redDelmiter9)

	// listPixelListY0 := calculateDimensionsPixelObjectDigits2(redDelmiter0)
	// listPixelListY1 := calculateDimensionsPixelObjectDigits2(redDelmiter1)
	// listPixelListY2 := calculateDimensionsPixelObjectDigits2(redDelmiter2)
	// listPixelListY3 := calculateDimensionsPixelObjectDigits2(redDelmiter3)
	// listPixelListY4 := calculateDimensionsPixelObjectDigits2(redDelmiter4)
	// listPixelListY5 := calculateDimensionsPixelObjectDigits2(redDelmiter5)
	// listPixelListY6 := calculateDimensionsPixelObjectDigits2(redDelmiter6)
	// listPixelListY7 := calculateDimensionsPixelObjectDigits2(redDelmiter7)
	// listPixelListY8 := calculateDimensionsPixelObjectDigits2(redDelmiter8)
	// listPixelListY9 := calculateDimensionsPixelObjectDigits2(redDelmiter9)

	// listPixelListY0 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter0, "0BuyPriceStore")
	// listPixelListY1 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter1, "-1BuyPriceStore")
	// listPixelListY2 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter2, "-2BuyPriceStore")
	// listPixelListY3 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter3, "-3BuyPriceStore")
	// listPixelListY4 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter4, "-4BuyPriceStore")
	// listPixelListY5 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter5, "-5BuyPriceStore")
	// listPixelListY6 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter6, "-6BuyPriceStore")
	// listPixelListY7 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter7, "-7BuyPriceStore")
	// listPixelListY8 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter8, "-8BuyPriceStore")
	// listPixelListY9 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter9, "-9BuyPriceStore")

	// listCharacterPixelListsBuy0 = seperateCharactersBuyPriceUpdated(listPixelListY0)
	// listCharacterPixelListsBuy1 = seperateCharactersBuyPriceUpdated(listPixelListY1)
	// listCharacterPixelListsBuy2 = seperateCharactersBuyPriceUpdated(listPixelListY2)
	// listCharacterPixelListsBuy3 = seperateCharactersBuyPriceUpdated(listPixelListY3)
	// listCharacterPixelListsBuy4 = seperateCharactersBuyPriceUpdated(listPixelListY4)
	// listCharacterPixelListsBuy5 = seperateCharactersBuyPriceUpdated(listPixelListY5)
	// listCharacterPixelListsBuy6 = seperateCharactersBuyPriceUpdated(listPixelListY6)
	// listCharacterPixelListsBuy7 = seperateCharactersBuyPriceUpdated(listPixelListY7)
	// listCharacterPixelListsBuy8 = seperateCharactersBuyPriceUpdated(listPixelListY8)
	// listCharacterPixelListsBuy9 = seperateCharactersBuyPriceUpdated(listPixelListY9)

	listCharacterPixelListsBuy0 = parsePixelObjectListFromStore("0BuyPriceStore.txt")
	listCharacterPixelListsBuy1 = parsePixelObjectListFromStore("-1BuyPriceStore.txt")
	listCharacterPixelListsBuy2 = parsePixelObjectListFromStore("-2BuyPriceStore.txt")
	listCharacterPixelListsBuy3 = parsePixelObjectListFromStore("-3BuyPriceStore.txt")
	listCharacterPixelListsBuy4 = parsePixelObjectListFromStore("-4BuyPriceStore.txt")
	listCharacterPixelListsBuy5 = parsePixelObjectListFromStore("-5BuyPriceStore.txt")
	listCharacterPixelListsBuy6 = parsePixelObjectListFromStore("-6BuyPriceStore.txt")
	listCharacterPixelListsBuy7 = parsePixelObjectListFromStore("-7BuyPriceStore.txt")
	listCharacterPixelListsBuy8 = parsePixelObjectListFromStore("-8BuyPriceStore.txt")
	listCharacterPixelListsBuy9 = parsePixelObjectListFromStore("-9BuyPriceStore.txt")

	//listCharacterPixelListsBuy10 = seperateCharactersBuyPrice(listPixelListY10)

	pixelListContainer0 := [][]int{}
	//red
	for indexPixel, pixelList := range listCharacterPixelListsBuy0 {
		pixelListContainer0 = append(pixelListContainer0, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer0[len(pixelListContainer0)-1] = append(pixelListContainer0[len(pixelListContainer0)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer1 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy1 {
		pixelListContainer1 = append(pixelListContainer1, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer1[len(pixelListContainer1)-1] = append(pixelListContainer1[len(pixelListContainer1)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer2 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy2 {
		pixelListContainer2 = append(pixelListContainer2, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer2[len(pixelListContainer2)-1] = append(pixelListContainer2[len(pixelListContainer2)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer3 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy3 {
		pixelListContainer3 = append(pixelListContainer3, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer3[len(pixelListContainer3)-1] = append(pixelListContainer3[len(pixelListContainer3)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer4 := [][]int{}
	//red
	for indexPixel, pixelList := range listCharacterPixelListsBuy4 {
		pixelListContainer4 = append(pixelListContainer4, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer4[len(pixelListContainer4)-1] = append(pixelListContainer4[len(pixelListContainer4)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer5 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy5 {
		pixelListContainer5 = append(pixelListContainer5, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer5[len(pixelListContainer5)-1] = append(pixelListContainer5[len(pixelListContainer5)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer6 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy6 {
		pixelListContainer6 = append(pixelListContainer6, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer6[len(pixelListContainer6)-1] = append(pixelListContainer6[len(pixelListContainer6)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer7 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy7 {
		pixelListContainer7 = append(pixelListContainer7, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer7[len(pixelListContainer7)-1] = append(pixelListContainer7[len(pixelListContainer7)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer8 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy8 {
		pixelListContainer8 = append(pixelListContainer8, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer8[len(pixelListContainer8)-1] = append(pixelListContainer8[len(pixelListContainer8)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer9 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy9 {
		pixelListContainer9 = append(pixelListContainer9, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				pixelListContainer9[len(pixelListContainer9)-1] = append(pixelListContainer9[len(pixelListContainer9)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	fmt.Println("pixelListContainer0")
	fmt.Println(pixelListContainer0)
	fmt.Println("pixelListContainer1")
	fmt.Println(pixelListContainer1)
	fmt.Println("pixelListContainer2")
	fmt.Println(pixelListContainer2)
	fmt.Println("pixelListContainer3")
	fmt.Println(pixelListContainer3)
	fmt.Println("pixelListContainer4")
	fmt.Println(pixelListContainer4)
	fmt.Println("pixelListContainer5")
	fmt.Println(pixelListContainer5)
	fmt.Println("pixelListContainer6")
	fmt.Println(pixelListContainer6)
	fmt.Println("pixelListContainer7")
	fmt.Println(pixelListContainer7)
	fmt.Println("pixelListContainer8")
	fmt.Println(pixelListContainer8)
	fmt.Println("pixelListContainer9")
	fmt.Println(pixelListContainer9)

	coreDigitPixelListBuyPriceRed0 = pixelListContainer0 //calculateSingleCoreDigit(pixelListContainer0)
	coreDigitPixelListBuyPriceRed1 = pixelListContainer1 //calculateSingleCoreDigit(pixelListContainer1)
	coreDigitPixelListBuyPriceRed2 = pixelListContainer2 //calculateSingleCoreDigit(pixelListContainer2)
	coreDigitPixelListBuyPriceRed3 = pixelListContainer3 //calculateSingleCoreDigit(pixelListContainer3)
	coreDigitPixelListBuyPriceRed4 = pixelListContainer4 //calculateSingleCoreDigit(pixelListContainer4)
	coreDigitPixelListBuyPriceRed5 = pixelListContainer5 //calculateSingleCoreDigit(pixelListContainer5)
	coreDigitPixelListBuyPriceRed6 = pixelListContainer6 //calculateSingleCoreDigit(pixelListContainer6)
	coreDigitPixelListBuyPriceRed7 = pixelListContainer7 //calculateSingleCoreDigit(pixelListContainer7)
	coreDigitPixelListBuyPriceRed8 = pixelListContainer8 //calculateSingleCoreDigit(pixelListContainer8)
	coreDigitPixelListBuyPriceRed9 = pixelListContainer9 //calculateSingleCoreDigit(pixelListContainer9)
}

func setIdentifierGroupsBuyPrice() {
	// redDelmiter1 := "1BuyPrice.png"
	// redDelmiter2 := "2BuyPrice.png"
	// redDelmiter3 := "3BuyPrice.png"
	// redDelmiter4 := "4BuyPrice.png"
	// redDelmiter5 := "5BuyPrice.png"
	// redDelmiter6 := "6BuyPrice.png"
	// redDelmiter7 := "7BuyPrice.png"
	// redDelmiter8 := "8BuyPrice.png"
	// redDelmiter9 := "9BuyPrice.png"

	//doubleNeg := "date~2021~1~20~20~8~9~"
	//red
	///postMarketPriceReadByFileName2(redDelmiter0)
	// postMarketPriceReadByFileName2(redDelmiter1)
	// postMarketPriceReadByFileName2(redDelmiter2)
	// postMarketPriceReadByFileName2(redDelmiter3)
	//postMarketPriceReadByFileName2(redDelmiter4)
	// postMarketPriceReadByFileName2(redDelmiter5)
	// postMarketPriceReadByFileName2(redDelmiter6)
	// postMarketPriceReadByFileName2(redDelmiter7)
	// postMarketPriceReadByFileName2(redDelmiter8)
	// postMarketPriceReadByFileName2(redDelmiter9)

	// listPixelListY0 := calculateDimensionsPixelObjectDigits2(redDelmiter0)
	// listPixelListY1 := calculateDimensionsPixelObjectDigits2(redDelmiter1)
	// listPixelListY2 := calculateDimensionsPixelObjectDigits2(redDelmiter2)
	// listPixelListY3 := calculateDimensionsPixelObjectDigits2(redDelmiter3)
	// listPixelListY4 := calculateDimensionsPixelObjectDigits2(redDelmiter4)
	// listPixelListY5 := calculateDimensionsPixelObjectDigits2(redDelmiter5)
	// listPixelListY6 := calculateDimensionsPixelObjectDigits2(redDelmiter6)
	// listPixelListY7 := calculateDimensionsPixelObjectDigits2(redDelmiter7)
	// listPixelListY8 := calculateDimensionsPixelObjectDigits2(redDelmiter8)
	// listPixelListY9 := calculateDimensionsPixelObjectDigits2(redDelmiter9)

	//
	//

	//

	// listPixelListY1 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter1, "1BuyPriceStore")
	// listPixelListY2 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter2, "2BuyPriceStore")
	// listPixelListY3 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter3, "3BuyPriceStore")
	// listPixelListY4 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter4, "4BuyPriceStore")
	// listPixelListY5 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter5, "5BuyPriceStore")
	// listPixelListY6 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter6, "6BuyPriceStore")
	// listPixelListY7 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter7, "7BuyPriceStore")
	// listPixelListY8 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter8, "8BuyPriceStore")
	// listPixelListY9 := calculateDimensionsPixelObjectDigitsStore2(redDelmiter9, "9BuyPriceStore")

	// // listCharacterPixelListsBuy0 = seperateCharactersBuyPrice(listPixelListY0)
	// listCharacterPixelListsBuy1 = seperateCharactersBuyPriceUpdated(listPixelListY1)
	// listCharacterPixelListsBuy2 = seperateCharactersBuyPriceUpdated(listPixelListY2)
	// listCharacterPixelListsBuy3 = seperateCharactersBuyPriceUpdated(listPixelListY3)
	// listCharacterPixelListsBuy4 = seperateCharactersBuyPriceUpdated(listPixelListY4)
	// listCharacterPixelListsBuy5 = seperateCharactersBuyPriceUpdated(listPixelListY5)
	// listCharacterPixelListsBuy6 = seperateCharactersBuyPriceUpdated(listPixelListY6)
	// listCharacterPixelListsBuy7 = seperateCharactersBuyPriceUpdated(listPixelListY7)
	// listCharacterPixelListsBuy8 = seperateCharactersBuyPriceUpdated(listPixelListY8)
	// listCharacterPixelListsBuy9 = seperateCharactersBuyPriceUpdated(listPixelListY9)

	//

	//

	//this the upon store procedure

	//now can call from file and parse as needed.

	listCharacterPixelListsBuy1 = parsePixelObjectListFromStore("1BuyPriceStore.txt")
	listCharacterPixelListsBuy2 = parsePixelObjectListFromStore("2BuyPriceStore.txt")
	listCharacterPixelListsBuy3 = parsePixelObjectListFromStore("3BuyPriceStore.txt")
	listCharacterPixelListsBuy4 = parsePixelObjectListFromStore("4BuyPriceStore.txt")
	listCharacterPixelListsBuy5 = parsePixelObjectListFromStore("5BuyPriceStore.txt")
	listCharacterPixelListsBuy6 = parsePixelObjectListFromStore("6BuyPriceStore.txt")
	listCharacterPixelListsBuy7 = parsePixelObjectListFromStore("7BuyPriceStore.txt")
	listCharacterPixelListsBuy8 = parsePixelObjectListFromStore("8BuyPriceStore.txt")
	listCharacterPixelListsBuy9 = parsePixelObjectListFromStore("9BuyPriceStore.txt")

	// listCharacterPixelListsBuy1 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter1, "1BuyPriceStore")
	// listCharacterPixelListsBuy2 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter2, "2BuyPriceStore")
	// listCharacterPixelListsBuy3 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter3, "3BuyPriceStore")
	// listCharacterPixelListsBuy4 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter4, "4BuyPriceStore")
	// listCharacterPixelListsBuy5 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter5, "5BuyPriceStore")
	// listCharacterPixelListsBuy6 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter6, "6BuyPriceStore")
	// listCharacterPixelListsBuy7 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter7, "7BuyPriceStore")
	// listCharacterPixelListsBuy8 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter8, "8BuyPriceStore")
	// listCharacterPixelListsBuy9 = calculateDimensionsPixelObjectDigitsStore2(redDelmiter9, "9BuyPriceStore")

	//listCharacterPixelListsBuy10 = seperateCharactersBuyPrice(listPixelListY10)

	// pixelListContainer0 := [][]int{}
	// //red
	// for indexPixel, pixelList := range listCharacterPixelListsBuy0 {
	// 	pixelListContainer0 = append(pixelListContainer0, []int{})
	// 	for indexPixel1, pixel := range pixelList {
	// 		intColor := 0
	// 		if s, err := strconv.Atoi(pixel.RedValue); err == nil {
	// 			intColor = s
	// 		}
	// 		if intColor > 150 {
	// 			pixelListContainer0[len(pixelListContainer0)-1] = append(pixelListContainer0[len(pixelListContainer0)-1], indexPixel1)
	// 		}
	// 	}
	// 	indexPixel += 1
	// }

	fmt.Println("len(listCharacterPixelListsBuy1) - 1")
	fmt.Println(len(listCharacterPixelListsBuy1) - 1)
	pixelListContainer1 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy1 {
		pixelListContainer1 = append(pixelListContainer1, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer1[len(pixelListContainer1)-1] = append(pixelListContainer1[len(pixelListContainer1)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer2 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy2 {
		pixelListContainer2 = append(pixelListContainer2, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer2[len(pixelListContainer2)-1] = append(pixelListContainer2[len(pixelListContainer2)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer3 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy3 {
		pixelListContainer3 = append(pixelListContainer3, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer3[len(pixelListContainer3)-1] = append(pixelListContainer3[len(pixelListContainer3)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer4 := [][]int{}
	//red
	for indexPixel, pixelList := range listCharacterPixelListsBuy4 {
		pixelListContainer4 = append(pixelListContainer4, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer4[len(pixelListContainer4)-1] = append(pixelListContainer4[len(pixelListContainer4)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer5 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy5 {
		pixelListContainer5 = append(pixelListContainer5, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer5[len(pixelListContainer5)-1] = append(pixelListContainer5[len(pixelListContainer5)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer6 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy6 {
		pixelListContainer6 = append(pixelListContainer6, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer6[len(pixelListContainer6)-1] = append(pixelListContainer6[len(pixelListContainer6)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer7 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy7 {
		pixelListContainer7 = append(pixelListContainer7, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer7[len(pixelListContainer7)-1] = append(pixelListContainer7[len(pixelListContainer7)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer8 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy8 {
		pixelListContainer8 = append(pixelListContainer8, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer8[len(pixelListContainer8)-1] = append(pixelListContainer8[len(pixelListContainer8)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	pixelListContainer9 := [][]int{}
	//green
	for indexPixel, pixelList := range listCharacterPixelListsBuy9 {
		pixelListContainer9 = append(pixelListContainer9, []int{})
		for indexPixel1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 40 {
				pixelListContainer9[len(pixelListContainer9)-1] = append(pixelListContainer9[len(pixelListContainer9)-1], indexPixel1)
			}
		}
		indexPixel += 1
	}

	// // fmt.Println("pixelListContainer0")
	// // fmt.Println(pixelListContainer0)
	fmt.Println("pixelListContainer1")
	fmt.Println(pixelListContainer1)
	fmt.Println("pixelListContainer2")
	fmt.Println(pixelListContainer2)
	fmt.Println("pixelListContainer3")
	fmt.Println(pixelListContainer3)
	fmt.Println("pixelListContainer4")
	fmt.Println(pixelListContainer4)
	fmt.Println("pixelListContainer5")
	fmt.Println(pixelListContainer5)
	fmt.Println("pixelListContainer6")
	fmt.Println(pixelListContainer6)
	fmt.Println("pixelListContainer7")
	fmt.Println(pixelListContainer7)
	fmt.Println("pixelListContainer8")
	fmt.Println(pixelListContainer8)
	fmt.Println("pixelListContainer9")
	fmt.Println(pixelListContainer9)

	// //coreDigitPixelListBuyPrice0 = pixelListContainer0//calculateSingleCoreDigit(pixelListContainer0)
	coreDigitPixelListBuyPrice1 = pixelListContainer1 //calculateSingleCoreDigit(pixelListContainer1)
	coreDigitPixelListBuyPrice2 = pixelListContainer2 //calculateSingleCoreDigit(pixelListContainer2)
	coreDigitPixelListBuyPrice3 = pixelListContainer3 //calculateSingleCoreDigit(pixelListContainer3)
	coreDigitPixelListBuyPrice4 = pixelListContainer4 //calculateSingleCoreDigit(pixelListContainer4)
	coreDigitPixelListBuyPrice5 = pixelListContainer5 //calculateSingleCoreDigit(pixelListContainer5)
	coreDigitPixelListBuyPrice6 = pixelListContainer6 //calculateSingleCoreDigit(pixelListContainer6)
	coreDigitPixelListBuyPrice7 = pixelListContainer7 //calculateSingleCoreDigit(pixelListContainer7)
	coreDigitPixelListBuyPrice8 = pixelListContainer8 //calculateSingleCoreDigit(pixelListContainer8)
	coreDigitPixelListBuyPrice9 = pixelListContainer9 //calculateSingleCoreDigit(pixelListContainer9)

	// //for each value then filter to core number purate
	// //get core purate if single digit

	// // fmt.Println("0")
	// // fmt.Println(coreDigitPixelList0)
	// // fmt.Println("1")
	// // fmt.Println(coreDigitPixelList1)
	// // fmt.Println("2")
	// // fmt.Println(coreDigitPixelList2)
	// // fmt.Println("3")
	// // fmt.Println(coreDigitPixelList3)
	// // fmt.Println("4")
	// // fmt.Println(coreDigitPixelList4)
	// // fmt.Println("5")
	// // fmt.Println(coreDigitPixelList5)
	// // fmt.Println("6")
	// // fmt.Println(coreDigitPixelList6)
	// // fmt.Println("7")
	// // fmt.Println(coreDigitPixelList7)
	// // fmt.Println("8")
	// // fmt.Println(coreDigitPixelList8)
	// // fmt.Println("9")
	// // fmt.Println(coreDigitPixelList9)

}

func parsePixelObjectListFromStore(fileName string) [][]PixelObject {
	// fileToReadString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\itterateCache.txt`)
	fileToReadString := `C:\Users\Bayliss Carr\Coding\fuvr1\` + fileName //+ `.txt`
	// fileToReadString := `/Users/CommanderCarr/coding/go/fuvr/` + fileName //+ `.txt`
	fileString := readFile(fileToReadString)
	// fileString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\` + fileToReadString)
	listPixelObjectList := [][]PixelObject{}
	//parse into columns
	listColumnsOfPixels := strings.Split(fileString, "$")
	// fmt.Println("listColumnsOfPixels1")
	// fmt.Println(len(listColumnsOfPixels))
	//parse into pixelobject list, then store into larger object list.
	for i, columnString := range listColumnsOfPixels {
		// fmt.Println(columnString)
		listPixelStringSegment := strings.Split(columnString, ",")
		// //for each column string parse into column string
		listColumnOfPixel := []PixelObject{}
		if i == 0 {
			// fmt.Println("listPixelStringSegment")
			// fmt.Println(listPixelStringSegment)
		}

		for i2, pixelStringSegment := range listPixelStringSegment {
			if i2 == (len(listPixelStringSegment) - 1) {
				break
			}
			//parse into PixelObjects for column
			listPixelValues := strings.Split(pixelStringSegment, "~")
			// //take each values then store into pixelObject
			pixelObject := PixelObject{}
			pixelObject.BlueValue = listPixelValues[0]
			pixelObject.GreenValue = listPixelValues[2]
			pixelObject.RedValue = listPixelValues[3]
			pixelObject.XValue = listPixelValues[4]
			pixelObject.YValue = listPixelValues[5]
			listColumnOfPixel = append(listColumnOfPixel, pixelObject)
			i2++
		}
		if i == len(listColumnsOfPixels)-1 {
			break
		}
		listPixelObjectList = append(listPixelObjectList, listColumnOfPixel)
		i++
	}
	return listPixelObjectList
}
func parsePixelObjectListFromStore1(fileName string) [][][]PixelObject {
	// fileString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\itterateCache.txt`)
	fileToReadString := `C:\Users\Bayliss Carr\Coding\fuvr1\` + fileName //+ `.txt`
	// fileToReadString := `/Users/CommanderCarr/coding/go/fuvr/` + fileName //+ `.txt`
	fileString := readFile(fileToReadString)
	// fileString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\` + fileToReadString)
	largerList := [][][]PixelObject{}

	list3d := strings.Split(fileString, "!")
	for i0, v := range list3d {
		listPixelObjectList := [][]PixelObject{}
		//parse into columns
		listColumnsOfPixels := strings.Split(v, "$")
		// fmt.Println("listColumnsOfPixels1")
		// fmt.Println(len(listColumnsOfPixels))

		//parse into pixelobject list, then store into larger object list.
		for i, columnString := range listColumnsOfPixels {
			// fmt.Println(columnString)
			listPixelStringSegment := strings.Split(columnString, ",")
			// //for each column string parse into column string
			listColumnOfPixel := []PixelObject{}
			if i == 0 {
				// fmt.Println("listPixelStringSegment")
				// fmt.Println(listPixelStringSegment)
			}

			for i2, pixelStringSegment := range listPixelStringSegment {
				if i2 == (len(listPixelStringSegment) - 1) {
					break
				}
				//parse into PixelObjects for column
				listPixelValues := strings.Split(pixelStringSegment, "~")
				// //take each values then store into pixelObject
				pixelObject := PixelObject{}
				pixelObject.BlueValue = listPixelValues[0]
				pixelObject.GreenValue = listPixelValues[2]
				pixelObject.RedValue = listPixelValues[3]
				pixelObject.XValue = listPixelValues[4]
				pixelObject.YValue = listPixelValues[5]
				listColumnOfPixel = append(listColumnOfPixel, pixelObject)
				i2++
			}
			if i == len(listColumnsOfPixels)-1 {
				break
			}
			listPixelObjectList = append(listPixelObjectList, listColumnOfPixel)
			i++
		}
		largerList = append(largerList, listPixelObjectList)
		i0++
	}
	return largerList
}

//

func calculateSingleCoreDigit(listContainer [][]int) [][]int {
	isSingle := false
	if len(listContainer[len(listContainer)-3]) == 0 && len(listContainer[len(listContainer)-2]) == 3 {
		isSingle = true
	}
	fmt.Println(isSingle)

	isNegative := false
	//isCalcContinue := false
	indexContinousCount := 0
	for i, v := range listContainer {
		//calc if 3 consec list is value 1
		if len(v) == 1 {
			indexContinousCount += 1
		}

		if len(v) != 1 {
			indexContinousCount = 0
		}

		if indexContinousCount == 3 {
			isNegative = true
			break
		}
		i += 1
	}

	//fmt.Println("isNegative")
	//fmt.Println(isNegative)

	coreListRev1 := [][]int{}
	isLenTouch := false
	isStore := false
	isCalcInitial := true
	if isSingle == false && isNegative == false {
		for i, v := range listContainer {
			if isCalcInitial {
				if len(v) > 0 {
					isLenTouch = true
				}

				if isLenTouch {
					if len(v) == 0 {
						isCalcInitial = false
						isStore = true
					}
				}
			}

			if isStore {
				if len(v) > 0 {
					coreListRev1 = append(coreListRev1, v)
				}
				if len(v) == 0 && i == len(listContainer)-1 {
					coreListRev1 = append(coreListRev1, v)
				}
			}
		}
	}

	indexContinousCount = 0
	isCalcInitial = false

	isCalcNegative := true
	isWait := false
	isSecondCount := false
	if isSingle == false && isNegative == true {
		for i, v := range listContainer {
			if isCalcNegative {
				if len(v) == 1 {
					indexContinousCount += 1
				}

				if len(v) != 1 {
					indexContinousCount = 0
				}

				if indexContinousCount == 2 {
					isSecondCount = true
					isCalcNegative = false
					continue
				}
			}

			if isSecondCount {
				if len(v) > 0 {
					isWait = true
				}
				if len(v) == 0 && isWait {
					//coreListRev1 = append(coreListRev1,v)
					isStore = true
					isSecondCount = false
					continue
				}
			}
			if isStore {
				if len(v) > 0 {
					coreListRev1 = append(coreListRev1, v)
				}
				if len(v) == 0 && i == len(listContainer)-1 {
					coreListRev1 = append(coreListRev1, v)
				}
			}
		}
	}

	isAfterTouchLen := false
	//if neg remove neg value, clean to core val
	if isSingle == true && isNegative == false {
		for i, v := range listContainer {
			if len(v) > 0 {
				coreListRev1 = append(coreListRev1, v)
				isAfterTouchLen = true
			}
			if len(v) == 0 && isAfterTouchLen {
				coreListRev1 = append(coreListRev1, v)
				break
			}
			i += 1
		}
	}

	if isSingle == true && isNegative == true {
		for i, v := range listContainer {
			if isCalcNegative {
				if len(v) == 1 {
					indexContinousCount += 1
				}

				if len(v) != 1 {
					indexContinousCount = 0
				}

				if indexContinousCount == 3 {
					isStore = true
					isCalcNegative = false
					continue
				}
			}

			if isStore {
				if len(v) > 0 {
					coreListRev1 = append(coreListRev1, v)
				}
				if len(v) == 0 && i == len(listContainer)-1 {
					coreListRev1 = append(coreListRev1, v)
				}
			}
		}
	}

	//fmt.Println("coreListRev1")
	//fmt.Println(coreListRev1)
	return coreListRev1
}
func setIdentifierGroups() {

	//price
	// redDelmiter0 := "date~2021~1~13~12~2~54~.png"
	// redDelmiter1 := "date~2021~1~13~12~2~26~.png"
	// redDelmiter2 := "date~2021~1~13~12~4~41~.png"
	// redDelmiter3 := "date~2021~1~13~12~4~8~.png"
	// redDelmiter4 := "date~2021~1~13~12~36~0~.png"
	// redDelmiter5 := "date~2021~1~13~12~3~53~.png"
	// redDelmiter6 := "date~2021~1~13~12~3~37~.png"
	// redDelmiter7 := "date~2021~1~13~12~3~22~.png"
	// redDelmiter8 := "date~2021~1~13~12~3~8~.png"
	// redDelmiter9 := "date~2021~1~13~11~59~23~.png"

	//red
	//postMarketPriceReadByFileName2(redDelmiter0)
	//postMarketPriceReadByFileName2(redDelmiter1)
	//postMarketPriceReadByFileName2(redDelmiter2)
	//postMarketPriceReadByFileName2(redDelmiter3)
	//postMarketPriceReadByFileName2(redDelmiter4)
	//postMarketPriceReadByFileName2(redDelmiter5)
	//postMarketPriceReadByFileName2(redDelmiter6)
	//postMarketPriceReadByFileName2(redDelmiter7)
	//postMarketPriceReadByFileName2(redDelmiter8)
	//postMarketPriceReadByFileName2(redDelmiter9)

	//bay100
	// listPixelListY0 := calculateDimensionsPixelObjectDigits2(redDelmiter0)
	// listPixelListY1 := calculateDimensionsPixelObjectDigits2(redDelmiter1)
	// listPixelListY2 := calculateDimensionsPixelObjectDigits2(redDelmiter2)
	// listPixelListY3 := calculateDimensionsPixelObjectDigits2(redDelmiter3)
	// listPixelListY4 := calculateDimensionsPixelObjectDigits2(redDelmiter4)
	// listPixelListY5 := calculateDimensionsPixelObjectDigits2(redDelmiter5)
	// listPixelListY6 := calculateDimensionsPixelObjectDigits2(redDelmiter6)
	// listPixelListY7 := calculateDimensionsPixelObjectDigits2(redDelmiter7)
	// listPixelListY8 := calculateDimensionsPixelObjectDigits2(redDelmiter8)
	// listPixelListY9 := calculateDimensionsPixelObjectDigits2(redDelmiter9)
	//
	// listPixelListY0 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter0, "0BuyPriceStorePriceRed.txt")
	// listPixelListY1 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter1, "1BuyPriceStorePriceRed.txt")
	// listPixelListY2 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter2, "2BuyPriceStorePriceRed.txt")
	// listPixelListY3 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter3, "3BuyPriceStorePriceRed.txt")
	// listPixelListY4 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter4, "4BuyPriceStorePriceRed.txt")
	// listPixelListY5 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter5, "5BuyPriceStorePriceRed.txt")
	// listPixelListY6 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter6, "6BuyPriceStorePriceRed.txt")
	// listPixelListY7 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter7, "7BuyPriceStorePriceRed.txt")
	// listPixelListY8 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter8, "8BuyPriceStorePriceRed.txt")
	// listPixelListY9 := calculateDimensionsPixelObjectDigitsStore23Dimension(redDelmiter9, "9BuyPriceStorePriceRed.txt")

	// listCharacterPixelListsBuy1 = parsePixelObjectListFromStore1("0BuyPriceStorePriceRed.txt")
	// fmt.Println(listPixelListY0)
	// fmt.Println(listPixelListY1)
	// fmt.Println(listPixelListY2)
	// fmt.Println(listPixelListY3)
	// fmt.Println(listPixelListY4)
	// fmt.Println(listPixelListY5)
	// fmt.Println(listPixelListY6)
	// fmt.Println(listPixelListY7)
	// fmt.Println(listPixelListY8)
	// fmt.Println(listPixelListY9)

	// listCharacterPixelLists0 = seperateCharactersLastPrice(listPixelListY0)
	// listCharacterPixelLists1 = seperateCharactersLastPrice(listPixelListY1)
	// listCharacterPixelLists2 = seperateCharactersLastPrice(listPixelListY2)
	// listCharacterPixelLists3 = seperateCharactersLastPrice(listPixelListY3)
	// listCharacterPixelLists4 = seperateCharactersLastPrice(listPixelListY4)
	// listCharacterPixelLists5 = seperateCharactersLastPrice(listPixelListY5)
	// listCharacterPixelLists6 = seperateCharactersLastPrice(listPixelListY6)
	// listCharacterPixelLists7 = seperateCharactersLastPrice(listPixelListY7)
	// listCharacterPixelLists8 = seperateCharactersLastPrice(listPixelListY8)
	// listCharacterPixelLists9 = seperateCharactersLastPrice(listPixelListY9)

	listCharacterPixelLists0 = parsePixelObjectListFromStore1("0BuyPriceStorePriceRed.txt")
	listCharacterPixelLists1 = parsePixelObjectListFromStore1("1BuyPriceStorePriceRed.txt")
	listCharacterPixelLists2 = parsePixelObjectListFromStore1("2BuyPriceStorePriceRed.txt")
	listCharacterPixelLists3 = parsePixelObjectListFromStore1("3BuyPriceStorePriceRed.txt")
	listCharacterPixelLists4 = parsePixelObjectListFromStore1("4BuyPriceStorePriceRed.txt")
	listCharacterPixelLists5 = parsePixelObjectListFromStore1("5BuyPriceStorePriceRed.txt")
	listCharacterPixelLists6 = parsePixelObjectListFromStore1("6BuyPriceStorePriceRed.txt")
	listCharacterPixelLists7 = parsePixelObjectListFromStore1("7BuyPriceStorePriceRed.txt")
	listCharacterPixelLists8 = parsePixelObjectListFromStore1("8BuyPriceStorePriceRed.txt")
	listCharacterPixelLists9 = parsePixelObjectListFromStore1("9BuyPriceStorePriceRed.txt")

	// fmt.Println("listCharacterPixelLists0 aaa")
	// fmt.Println(listCharacterPixelLists0)
	//green
	// greenDelmiter0 := "date~2021~1~13~11~20~37~.png"
	// greenDelmiter1 := "date~2021~1~13~11~35~25~.png"
	// greenDelmiter2 := "date~2021~1~13~11~35~39~.png"
	// greenDelmiter3 := "date~2021~1~13~11~38~0~.png"
	// greenDelmiter4 := "date~2021~1~13~11~38~14~.png"
	// greenDelmiter5 := "date~2021~1~13~11~22~16~.png"
	// greenDelmiter6 := "date~2021~1~13~11~38~42~.png"
	// greenDelmiter7 := "date~2021~1~13~11~20~9~.png"
	// greenDelmiter8 := "date~2021~1~13~11~35~11~.png"
	// greenDelmiter9 := "date~2021~1~13~11~22~58~.png"

	// //postMarketPriceReadByFileName2(greenDelmiter0)
	// //postMarketPriceReadByFileName2(greenDelmiter1)
	// //postMarketPriceReadByFileName2(greenDelmiter2)
	// //postMarketPriceReadByFileName2(greenDelmiter3)
	// //postMarketPriceReadByFileName2(greenDelmiter4)
	// //postMarketPriceReadByFileName2(greenDelmiter5)
	// //postMarketPriceReadByFileName2(greenDelmiter6)
	// //postMarketPriceReadByFileName2(greenDelmiter7)
	// //postMarketPriceReadByFileName2(greenDelmiter8)
	// //postMarketPriceReadByFileName2(greenDelmiter9)

	// listPixelListY0 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter0, "0BuyPriceStorePriceGreen.txt")
	// listPixelListY1 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter1, "1BuyPriceStorePriceGreen.txt")
	// listPixelListY2 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter2, "2BuyPriceStorePriceGreen.txt")
	// listPixelListY3 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter3, "3BuyPriceStorePriceGreen.txt")
	// listPixelListY4 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter4, "4BuyPriceStorePriceGreen.txt")
	// listPixelListY5 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter5, "5BuyPriceStorePriceGreen.txt")
	// listPixelListY6 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter6, "6BuyPriceStorePriceGreen.txt")
	// listPixelListY7 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter7, "7BuyPriceStorePriceGreen.txt")
	// listPixelListY8 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter8, "8BuyPriceStorePriceGreen.txt")
	// listPixelListY9 := calculateDimensionsPixelObjectDigitsStore23Dimension(greenDelmiter9, "9BuyPriceStorePriceGreen.txt")

	// fmt.Println(listPixelListY0)
	// fmt.Println(listPixelListY1)
	// fmt.Println(listPixelListY2)
	// fmt.Println(listPixelListY3)
	// fmt.Println(listPixelListY4)
	// fmt.Println(listPixelListY5)
	// fmt.Println(listPixelListY6)
	// fmt.Println(listPixelListY7)
	// fmt.Println(listPixelListY8)
	// fmt.Println(listPixelListY9)
	listCharacterPixelLists0Green = parsePixelObjectListFromStore1("0BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists1Green = parsePixelObjectListFromStore1("1BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists2Green = parsePixelObjectListFromStore1("2BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists3Green = parsePixelObjectListFromStore1("3BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists4Green = parsePixelObjectListFromStore1("4BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists5Green = parsePixelObjectListFromStore1("5BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists6Green = parsePixelObjectListFromStore1("6BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists7Green = parsePixelObjectListFromStore1("7BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists8Green = parsePixelObjectListFromStore1("8BuyPriceStorePriceGreen.txt")
	listCharacterPixelLists9Green = parsePixelObjectListFromStore1("9BuyPriceStorePriceGreen.txt")

	// // listPixelListY0Green := calculateDimensionsPixelObjectDigits2(greenDelmiter0)
	// // listPixelListY1Green := calculateDimensionsPixelObjectDigits2(greenDelmiter1)
	// // listPixelListY2Green := calculateDimensionsPixelObjectDigits2(greenDelmiter2)
	// // listPixelListY3Green := calculateDimensionsPixelObjectDigits2(greenDelmiter3)
	// // listPixelListY4Green := calculateDimensionsPixelObjectDigits2(greenDelmiter4)
	// // listPixelListY5Green := calculateDimensionsPixelObjectDigits2(greenDelmiter5)
	// // listPixelListY6Green := calculateDimensionsPixelObjectDigits2(greenDelmiter6)
	// // listPixelListY7Green := calculateDimensionsPixelObjectDigits2(greenDelmiter7)
	// // listPixelListY8Green := calculateDimensionsPixelObjectDigits2(greenDelmiter8)
	// // listPixelListY9Green := calculateDimensionsPixelObjectDigits2(greenDelmiter9)

	// listPixelListY0Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter0, "0BuyPriceStorePriceGreen")
	// listPixelListY1Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter1, "1BuyPriceStorePriceGreen")
	// listPixelListY2Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter2, "2BuyPriceStorePriceGreen")
	// listPixelListY3Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter3, "3BuyPriceStorePriceGreen")
	// listPixelListY4Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter4, "4BuyPriceStorePriceGreen")
	// listPixelListY5Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter5, "5BuyPriceStorePriceGreen")
	// listPixelListY6Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter6, "6BuyPriceStorePriceGreen")
	// listPixelListY7Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter7, "7BuyPriceStorePriceGreen")
	// listPixelListY8Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter8, "8BuyPriceStorePriceGreen")
	// listPixelListY9Green := calculateDimensionsPixelObjectDigitsStore2(greenDelmiter9, "9BuyPriceStorePriceGreen")

	// listCharacterPixelLists0Green = seperateCharactersLastPrice(listPixelListY0Green)
	// listCharacterPixelLists1Green = seperateCharactersLastPrice(listPixelListY1Green)
	// listCharacterPixelLists2Green = seperateCharactersLastPrice(listPixelListY2Green)
	// listCharacterPixelLists3Green = seperateCharactersLastPrice(listPixelListY3Green)
	// listCharacterPixelLists4Green = seperateCharactersLastPrice(listPixelListY4Green)
	// listCharacterPixelLists5Green = seperateCharactersLastPrice(listPixelListY5Green)
	// listCharacterPixelLists6Green = seperateCharactersLastPrice(listPixelListY6Green)
	// listCharacterPixelLists7Green = seperateCharactersLastPrice(listPixelListY7Green)
	// listCharacterPixelLists8Green = seperateCharactersLastPrice(listPixelListY8Green)
	// listCharacterPixelLists9Green = seperateCharactersLastPrice(listPixelListY9Green)

	// // fmt.Println("listCharacterPixelLists9Green")
	// // fmt.Println(listCharacterPixelLists9Green)

	// // //ticker

	// digitDelimiter1 := "date~2021~1~13~12~2~54~.png"
	// digitDelimiter2 := "date~2021~1~14~14~48~20~.png"
	// digitDelimiter3 := "date~2021~1~14~14~48~8~.png"
	// digitDelimiter4 := "date~2021~1~14~14~47~20~.png"
	// digitDelimiter5 := "date~2021~1~14~14~54~7~.png"
	// digitDelimiter6 := "date~2021~1~14~14~54~1~.png"
	// digitDelimiter7 := "date~2021~1~14~14~52~44~.png"
	// digitDelimiter8 := "date~2021~1~14~14~51~50~.png"
	// digitDelimiter9 := "date~2021~1~14~14~59~30~.png"

	// // listPixelListY0 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter1, "1BuyPriceStoreTicker1.txt")
	// listPixelListY1 := calculateDimensionsPixelObjectDigitsStore23DimensionTick(digitDelimiter1, "1BuyPriceStoreTicker1.txt")
	// listPixelListY2 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter2, "2BuyPriceStoreTicker1.txt")
	// listPixelListY3 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter3, "3BuyPriceStoreTicker1.txt")
	// listPixelListY4 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter4, "4BuyPriceStoreTicker1.txt")
	// listPixelListY5 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter5, "5BuyPriceStoreTicker1.txt")
	// listPixelListY6 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter6, "6BuyPriceStoreTicker1.txt")
	// listPixelListY7 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter7, "7BuyPriceStoreTicker1.txt")
	// listPixelListY8 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter8, "8BuyPriceStoreTicker1.txt")
	// listPixelListY9 := calculateDimensionsPixelObjectDigitsStore23Dimension(digitDelimiter9, "9BuyPriceStoreTicker1.txt")

	// //digit 1
	// //postMarketPriceReadByFileName2(digitDelimiter1)
	// //postMarketPriceReadByFileName2(digitDelimiter2)
	// //postMarketPriceReadByFileName2(digitDelimiter3)
	// //postMarketPriceReadByFileName2(digitDelimiter4)
	// //postMarketPriceReadByFileName2(digitDelimiter5)
	// //postMarketPriceReadByFileName2(digitDelimiter6)
	// //postMarketPriceReadByFileName2(digitDelimiter7)
	// //postMarketPriceReadByFileName2(digitDelimiter8)
	// //postMarketPriceReadByFileName2(digitDelimiter9)

	// // listPixelListY1Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter1)
	// // listPixelListY2Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter2)
	// // listPixelListY3Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter3)
	// // listPixelListY4Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter4)
	// // listPixelListY5Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter5)
	// // listPixelListY6Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter6)
	// // listPixelListY7Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter7)
	// // listPixelListY8Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter8)
	// // listPixelListY9Digit1 := calculateDimensionsPixelObjectDigits2(digitDelimiter9)

	// listPixelListY1Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter1, "1BuyPriceStoreTicker1")
	// listPixelListY2Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter2, "2BuyPriceStoreTicker1")
	// listPixelListY3Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter3, "3BuyPriceStoreTicker1")
	// listPixelListY4Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter4, "4BuyPriceStoreTicker1")
	// listPixelListY5Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter5, "5BuyPriceStoreTicker1")
	// listPixelListY6Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter6, "6BuyPriceStoreTicker1")
	// listPixelListY7Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter7, "7BuyPriceStoreTicker1")
	// listPixelListY8Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter8, "8BuyPriceStoreTicker1")
	// listPixelListY9Digit1 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter9, "9BuyPriceStoreTicker1")

	// listCharacterPixelLists1Digit1 = seperateCharactersTicker(listPixelListY1Digit1)
	// listCharacterPixelLists2Digit1 = seperateCharactersTicker(listPixelListY2Digit1)
	// listCharacterPixelLists3Digit1 = seperateCharactersTicker(listPixelListY3Digit1)
	// listCharacterPixelLists4Digit1 = seperateCharactersTicker(listPixelListY4Digit1)
	// listCharacterPixelLists5Digit1 = seperateCharactersTicker(listPixelListY5Digit1)
	// listCharacterPixelLists6Digit1 = seperateCharactersTicker(listPixelListY6Digit1)
	// listCharacterPixelLists7Digit1 = seperateCharactersTicker(listPixelListY7Digit1)
	// listCharacterPixelLists8Digit1 = seperateCharactersTicker(listPixelListY8Digit1)
	// listCharacterPixelLists9Digit1 = seperateCharactersTicker(listPixelListY9Digit1)

	// // //digit 2
	// digitDelimiter0 := "date~2021~1~14~14~54~1~.png"
	// digitDelimiter1 = "date~2021~1~13~12~2~40~.png"
	// digitDelimiter2 = "date~2021~1~13~14~55~32~.png"
	// digitDelimiter3 = "date~2021~1~13~12~1~16~.png"
	// digitDelimiter4 = "date~2021~1~13~12~2~26~.png"
	// digitDelimiter5 = "date~2021~1~13~12~3~37~.png"
	// digitDelimiter6 = "date~2021~1~13~12~17~31~.png"
	// digitDelimiter7 = "date~2021~1~14~14~46~42~.png"
	// digitDelimiter8 = "date~2021~1~14~14~46~40~.png"
	// digitDelimiter9 = "date~2021~1~13~12~17~16~.png"

	// //postMarketPriceReadByFileName2(digitDelimiter0)
	// //postMarketPriceReadByFileName2(digitDelimiter1)
	// //postMarketPriceReadByFileName2(digitDelimiter2)
	// //postMarketPriceReadByFileName2(digitDelimiter3)
	// //postMarketPriceReadByFileName2(digitDelimiter4)
	// //postMarketPriceReadByFileName2(digitDelimiter5)
	// //postMarketPriceReadByFileName2(digitDelimiter6)
	// //postMarketPriceReadByFileName2(digitDelimiter7)
	// //postMarketPriceReadByFileName2(digitDelimiter8)
	// //postMarketPriceReadByFileName2(digitDelimiter9)

	// // listPixelListY0Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter0)
	// // listPixelListY1Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter1)
	// // listPixelListY2Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter2)
	// // listPixelListY3Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter3)
	// // listPixelListY4Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter4)
	// // listPixelListY5Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter5)
	// // listPixelListY6Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter6)
	// // listPixelListY7Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter7)
	// // listPixelListY8Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter8)
	// // listPixelListY9Digit2 := calculateDimensionsPixelObjectDigits2(digitDelimiter9)

	// listPixelListY0Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter0, "0BuyPriceStoreTicker2")
	// listPixelListY1Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter1, "1BuyPriceStoreTicker2")
	// listPixelListY2Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter2, "2BuyPriceStoreTicker2")
	// listPixelListY3Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter3, "3BuyPriceStoreTicker2")
	// listPixelListY4Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter4, "4BuyPriceStoreTicker2")
	// listPixelListY5Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter5, "5BuyPriceStoreTicker2")
	// listPixelListY6Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter6, "6BuyPriceStoreTicker2")
	// listPixelListY7Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter7, "7BuyPriceStoreTicker2")
	// listPixelListY8Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter8, "8BuyPriceStoreTicker2")
	// listPixelListY9Digit2 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter9, "9BuyPriceStoreTicker2")

	// listCharacterPixelLists0Digit2 = seperateCharactersTicker(listPixelListY0Digit2)
	// listCharacterPixelLists1Digit2 = seperateCharactersTicker(listPixelListY1Digit2)
	// listCharacterPixelLists2Digit2 = seperateCharactersTicker(listPixelListY2Digit2)
	// listCharacterPixelLists3Digit2 = seperateCharactersTicker(listPixelListY3Digit2)
	// listCharacterPixelLists4Digit2 = seperateCharactersTicker(listPixelListY4Digit2)
	// listCharacterPixelLists5Digit2 = seperateCharactersTicker(listPixelListY5Digit2)
	// listCharacterPixelLists6Digit2 = seperateCharactersTicker(listPixelListY6Digit2)
	// listCharacterPixelLists7Digit2 = seperateCharactersTicker(listPixelListY7Digit2)
	// listCharacterPixelLists8Digit2 = seperateCharactersTicker(listPixelListY8Digit2)
	// listCharacterPixelLists9Digit2 = seperateCharactersTicker(listPixelListY9Digit2)

	// //digit 3
	// digitDelimiter0 = "date~2021~1~14~14~53~50~.png"
	// digitDelimiter1 = "date~2021~1~13~12~15~29~.png"
	// digitDelimiter2 = "date~2021~1~13~12~2~54~.png"
	// digitDelimiter3 = "date~2021~1~14~14~50~21~.png"
	// digitDelimiter4 = "date~2021~1~14~14~50~31~.png"
	// digitDelimiter5 = "date~2021~1~14~14~50~25~.png"
	// digitDelimiter6 = "date~2021~1~14~14~55~33~.png"
	// digitDelimiter7 = "date~2021~1~13~12~2~26~.png"
	// digitDelimiter8 = "date~2021~1~13~12~3~37~.png"
	// digitDelimiter9 = "date~2021~1~14~14~3~50~.png"

	// //postMarketPriceReadByFileName2(digitDelimiter0)
	// //postMarketPriceReadByFileName2(digitDelimiter1)
	// //postMarketPriceReadByFileName2(digitDelimiter2)
	// //postMarketPriceReadByFileName2(digitDelimiter3)
	// //postMarketPriceReadByFileName2(digitDelimiter4)
	// //postMarketPriceReadByFileName2(digitDelimiter5)
	// //postMarketPriceReadByFileName2(digitDelimiter6)
	// //postMarketPriceReadByFileName2(digitDelimiter7)
	// //postMarketPriceReadByFileName2(digitDelimiter8)
	// //postMarketPriceReadByFileName2(digitDelimiter9)

	// // listPixelListY0Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter0)
	// // listPixelListY1Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter1)
	// // listPixelListY2Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter2)
	// // listPixelListY3Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter3)
	// // listPixelListY4Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter4)
	// // listPixelListY5Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter5)
	// // listPixelListY6Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter6)
	// // listPixelListY7Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter7)
	// // listPixelListY8Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter8)
	// // listPixelListY9Digit3 := calculateDimensionsPixelObjectDigits2(digitDelimiter9)

	// listPixelListY0Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter0, "0BuyPriceStoreTicker3")
	// listPixelListY1Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter1, "1BuyPriceStoreTicker3")
	// listPixelListY2Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter2, "2BuyPriceStoreTicker3")
	// listPixelListY3Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter3, "3BuyPriceStoreTicker3")
	// listPixelListY4Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter4, "4BuyPriceStoreTicker3")
	// listPixelListY5Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter5, "5BuyPriceStoreTicker3")
	// listPixelListY6Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter6, "6BuyPriceStoreTicker3")
	// listPixelListY7Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter7, "7BuyPriceStoreTicker3")
	// listPixelListY8Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter8, "8BuyPriceStoreTicker3")
	// listPixelListY9Digit3 := calculateDimensionsPixelObjectDigitsStore2(digitDelimiter9, "9BuyPriceStoreTicker3")

	// listCharacterPixelLists0Digit3 = seperateCharactersTicker(listPixelListY0Digit3)
	// listCharacterPixelLists1Digit3 = seperateCharactersTicker(listPixelListY1Digit3)
	// listCharacterPixelLists2Digit3 = seperateCharactersTicker(listPixelListY2Digit3)
	// listCharacterPixelLists3Digit3 = seperateCharactersTicker(listPixelListY3Digit3)
	// listCharacterPixelLists4Digit3 = seperateCharactersTicker(listPixelListY4Digit3)
	// listCharacterPixelLists5Digit3 = seperateCharactersTicker(listPixelListY5Digit3)
	// listCharacterPixelLists6Digit3 = seperateCharactersTicker(listPixelListY6Digit3)
	// listCharacterPixelLists7Digit3 = seperateCharactersTicker(listPixelListY7Digit3)
	// listCharacterPixelLists8Digit3 = seperateCharactersTicker(listPixelListY8Digit3)
	// listCharacterPixelLists9Digit3 = seperateCharactersTicker(listPixelListY9Digit3)

	// // fmt.Println("listCharacterPixelLists9Digit3")
	// // fmt.Println(listCharacterPixelLists9Digit3)

}

var listCharacterPixelLists0 = [][][]PixelObject{}
var listCharacterPixelLists1 = [][][]PixelObject{}
var listCharacterPixelLists2 = [][][]PixelObject{}
var listCharacterPixelLists3 = [][][]PixelObject{}
var listCharacterPixelLists4 = [][][]PixelObject{}
var listCharacterPixelLists5 = [][][]PixelObject{}
var listCharacterPixelLists6 = [][][]PixelObject{}
var listCharacterPixelLists7 = [][][]PixelObject{}
var listCharacterPixelLists8 = [][][]PixelObject{}
var listCharacterPixelLists9 = [][][]PixelObject{}

var listCharacterPixelLists0Green = [][][]PixelObject{}
var listCharacterPixelLists1Green = [][][]PixelObject{}
var listCharacterPixelLists2Green = [][][]PixelObject{}
var listCharacterPixelLists3Green = [][][]PixelObject{}
var listCharacterPixelLists4Green = [][][]PixelObject{}
var listCharacterPixelLists5Green = [][][]PixelObject{}
var listCharacterPixelLists6Green = [][][]PixelObject{}
var listCharacterPixelLists7Green = [][][]PixelObject{}
var listCharacterPixelLists8Green = [][][]PixelObject{}
var listCharacterPixelLists9Green = [][][]PixelObject{}

var listCharacterPixelLists0Digit1 = [][][]PixelObject{}
var listCharacterPixelLists1Digit1 = [][][]PixelObject{}
var listCharacterPixelLists2Digit1 = [][][]PixelObject{}
var listCharacterPixelLists3Digit1 = [][][]PixelObject{}
var listCharacterPixelLists4Digit1 = [][][]PixelObject{}
var listCharacterPixelLists5Digit1 = [][][]PixelObject{}
var listCharacterPixelLists6Digit1 = [][][]PixelObject{}
var listCharacterPixelLists7Digit1 = [][][]PixelObject{}
var listCharacterPixelLists8Digit1 = [][][]PixelObject{}
var listCharacterPixelLists9Digit1 = [][][]PixelObject{}

var listCharacterPixelLists0Digit2 = [][][]PixelObject{}
var listCharacterPixelLists1Digit2 = [][][]PixelObject{}
var listCharacterPixelLists2Digit2 = [][][]PixelObject{}
var listCharacterPixelLists3Digit2 = [][][]PixelObject{}
var listCharacterPixelLists4Digit2 = [][][]PixelObject{}
var listCharacterPixelLists5Digit2 = [][][]PixelObject{}
var listCharacterPixelLists6Digit2 = [][][]PixelObject{}
var listCharacterPixelLists7Digit2 = [][][]PixelObject{}
var listCharacterPixelLists8Digit2 = [][][]PixelObject{}
var listCharacterPixelLists9Digit2 = [][][]PixelObject{}

var listCharacterPixelLists0Digit3 = [][][]PixelObject{}
var listCharacterPixelLists1Digit3 = [][][]PixelObject{}
var listCharacterPixelLists2Digit3 = [][][]PixelObject{}
var listCharacterPixelLists3Digit3 = [][][]PixelObject{}
var listCharacterPixelLists4Digit3 = [][][]PixelObject{}
var listCharacterPixelLists5Digit3 = [][][]PixelObject{}
var listCharacterPixelLists6Digit3 = [][][]PixelObject{}
var listCharacterPixelLists7Digit3 = [][][]PixelObject{}
var listCharacterPixelLists8Digit3 = [][][]PixelObject{}
var listCharacterPixelLists9Digit3 = [][][]PixelObject{}

func identifyTickerDigits(listCharacterPixelListsInQuestion [][][]PixelObject) string {

	intTickerDelimiter := 150
	//seperateCharactersTicker

	//digit 1
	numberBaseIdentifcationListDigit1 := [][][]int{}
	listCalculation1 := [][]int{}
	for i, pixelList := range listCharacterPixelLists1Digit1[0] {
		listCalculation1 = append(listCalculation1, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation1[len(listCalculation1)-1] = append(listCalculation1[len(listCalculation1)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation1)
	listCalculation2 := [][]int{}
	for i, pixelList := range listCharacterPixelLists2Digit1[0] {
		listCalculation2 = append(listCalculation2, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation2[len(listCalculation2)-1] = append(listCalculation2[len(listCalculation2)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation2)
	listCalculation3 := [][]int{}
	for i, pixelList := range listCharacterPixelLists3Digit1[0] {
		listCalculation3 = append(listCalculation3, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation3[len(listCalculation3)-1] = append(listCalculation3[len(listCalculation3)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation3)
	listCalculation4 := [][]int{}
	for i, pixelList := range listCharacterPixelLists4Digit1[0] {
		listCalculation4 = append(listCalculation4, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation4[len(listCalculation4)-1] = append(listCalculation4[len(listCalculation4)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation4)
	listCalculation5 := [][]int{}
	for i, pixelList := range listCharacterPixelLists5Digit1[0] {
		listCalculation5 = append(listCalculation5, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation5[len(listCalculation5)-1] = append(listCalculation5[len(listCalculation5)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation5)
	listCalculation6 := [][]int{}
	for i, pixelList := range listCharacterPixelLists6Digit1[0] {
		listCalculation6 = append(listCalculation6, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation6[len(listCalculation6)-1] = append(listCalculation6[len(listCalculation6)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation6)
	listCalculation7 := [][]int{}
	for i, pixelList := range listCharacterPixelLists7Digit1[0] {
		listCalculation7 = append(listCalculation7, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation7[len(listCalculation7)-1] = append(listCalculation7[len(listCalculation7)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation7)
	listCalculation8 := [][]int{}
	for i, pixelList := range listCharacterPixelLists8Digit1[0] {
		listCalculation8 = append(listCalculation8, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation8[len(listCalculation8)-1] = append(listCalculation8[len(listCalculation8)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation8)
	listCalculation9 := [][]int{}
	for i, pixelList := range listCharacterPixelLists9Digit1[0] {
		listCalculation9 = append(listCalculation9, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation9[len(listCalculation9)-1] = append(listCalculation9[len(listCalculation9)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit1 = append(numberBaseIdentifcationListDigit1, listCalculation9)

	//fmt.Println("dig1")

	//digit 2
	numberBaseIdentifcationListDigit2 := [][][]int{}
	listCalculation0 := [][]int{}
	for i, pixelList := range listCharacterPixelLists0Digit2[1] {
		listCalculation0 = append(listCalculation0, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation0[len(listCalculation0)-1] = append(listCalculation0[len(listCalculation0)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation0)
	listCalculation1 = [][]int{}
	for i, pixelList := range listCharacterPixelLists1Digit2[1] {
		listCalculation1 = append(listCalculation1, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation1[len(listCalculation1)-1] = append(listCalculation1[len(listCalculation1)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation1)
	listCalculation2 = [][]int{}
	for i, pixelList := range listCharacterPixelLists2Digit2[1] {
		listCalculation2 = append(listCalculation2, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation2[len(listCalculation2)-1] = append(listCalculation2[len(listCalculation2)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation2)
	listCalculation3 = [][]int{}
	for i, pixelList := range listCharacterPixelLists3Digit2[1] {
		listCalculation3 = append(listCalculation3, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation3[len(listCalculation3)-1] = append(listCalculation3[len(listCalculation3)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation3)
	listCalculation4 = [][]int{}
	for i, pixelList := range listCharacterPixelLists4Digit2[1] {
		listCalculation4 = append(listCalculation4, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation4[len(listCalculation4)-1] = append(listCalculation4[len(listCalculation4)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation4)
	listCalculation5 = [][]int{}
	for i, pixelList := range listCharacterPixelLists5Digit2[1] {
		listCalculation5 = append(listCalculation5, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation5[len(listCalculation5)-1] = append(listCalculation5[len(listCalculation5)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation5)
	listCalculation6 = [][]int{}
	for i, pixelList := range listCharacterPixelLists6Digit2[1] {
		listCalculation6 = append(listCalculation6, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation6[len(listCalculation6)-1] = append(listCalculation6[len(listCalculation6)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation6)
	listCalculation7 = [][]int{}
	for i, pixelList := range listCharacterPixelLists7Digit2[1] {
		listCalculation7 = append(listCalculation7, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation7[len(listCalculation7)-1] = append(listCalculation7[len(listCalculation7)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation7)
	listCalculation8 = [][]int{}
	for i, pixelList := range listCharacterPixelLists8Digit2[1] {
		listCalculation8 = append(listCalculation8, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation8[len(listCalculation8)-1] = append(listCalculation8[len(listCalculation8)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation8)
	listCalculation9 = [][]int{}
	for i, pixelList := range listCharacterPixelLists9Digit2[1] {
		listCalculation9 = append(listCalculation9, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation9[len(listCalculation9)-1] = append(listCalculation9[len(listCalculation9)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit2 = append(numberBaseIdentifcationListDigit2, listCalculation9)

	//digit 3
	numberBaseIdentifcationListDigit3 := [][][]int{}
	listCalculation0 = [][]int{}
	for i, pixelList := range listCharacterPixelLists0Digit3[2] {
		listCalculation0 = append(listCalculation0, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation0[len(listCalculation0)-1] = append(listCalculation0[len(listCalculation0)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation0)
	listCalculation1 = [][]int{}
	for i, pixelList := range listCharacterPixelLists1Digit3[2] {
		listCalculation1 = append(listCalculation1, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation1[len(listCalculation1)-1] = append(listCalculation1[len(listCalculation1)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation1)
	listCalculation2 = [][]int{}
	for i, pixelList := range listCharacterPixelLists2Digit3[2] {
		listCalculation2 = append(listCalculation2, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation2[len(listCalculation2)-1] = append(listCalculation2[len(listCalculation2)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation2)
	listCalculation3 = [][]int{}
	for i, pixelList := range listCharacterPixelLists3Digit3[2] {
		listCalculation3 = append(listCalculation3, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation3[len(listCalculation3)-1] = append(listCalculation3[len(listCalculation3)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation3)
	listCalculation4 = [][]int{}
	for i, pixelList := range listCharacterPixelLists4Digit3[2] {
		listCalculation4 = append(listCalculation4, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation4[len(listCalculation4)-1] = append(listCalculation4[len(listCalculation4)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation4)
	listCalculation5 = [][]int{}
	for i, pixelList := range listCharacterPixelLists5Digit3[2] {
		listCalculation5 = append(listCalculation5, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation5[len(listCalculation5)-1] = append(listCalculation5[len(listCalculation5)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation5)
	listCalculation6 = [][]int{}
	for i, pixelList := range listCharacterPixelLists6Digit3[2] {
		listCalculation6 = append(listCalculation6, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation6[len(listCalculation6)-1] = append(listCalculation6[len(listCalculation6)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation6)
	listCalculation7 = [][]int{}
	for i, pixelList := range listCharacterPixelLists7Digit3[2] {
		listCalculation7 = append(listCalculation7, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation7[len(listCalculation7)-1] = append(listCalculation7[len(listCalculation7)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation7)
	listCalculation8 = [][]int{}
	for i, pixelList := range listCharacterPixelLists8Digit3[2] {
		listCalculation8 = append(listCalculation8, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation8[len(listCalculation8)-1] = append(listCalculation8[len(listCalculation8)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation8)
	listCalculation9 = [][]int{}
	for i, pixelList := range listCharacterPixelLists9Digit3[2] {
		listCalculation9 = append(listCalculation9, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor < intTickerDelimiter {
				listCalculation9[len(listCalculation9)-1] = append(listCalculation9[len(listCalculation9)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListDigit3 = append(numberBaseIdentifcationListDigit3, listCalculation9)

	//fmt.Println("hit1")
	//numberBaseIdentifcationListDigit1

	//fmt.Println("listCharacterPixelListsInQuestion")
	//fmt.Println(len(listCharacterPixelListsInQuestion))
	//fmt.Println(listCharacterPixelListsInQuestion)
	listResultsDigits := []int{}
	//for each  character do identification, and have base body calculation samples modulated for digits.
	for indexDigit, digitInQuestion := range listCharacterPixelListsInQuestion {
		listCalculationIndex := [][]int{}
		for indexPixel, pixelList := range digitInQuestion {
			listCalculationIndex = append(listCalculationIndex, []int{})
			for indexPixel1, pixel := range pixelList {
				intColor := 0
				if s, err := strconv.Atoi(pixel.RedValue); err == nil {
					intColor = s
				}
				if intColor < intTickerDelimiter {
					//fmt.Println("pixelEntering")
					//fmt.Println(pixel)
					listCalculationIndex[len(listCalculationIndex)-1] = append(listCalculationIndex[len(listCalculationIndex)-1], indexPixel1)
				}
				//indexPixel1+=1
			}
			indexPixel += 1
		}
		indexMatching := -1
		//fmt.Println("len(listCalculationIndex)")
		///fmt.Println(len(listCalculationIndex))
		//fmt.Println(listCalculationIndex)
		//fmt.Println("indexDigit")
		//fmt.Println(indexDigit)
		//bay1
		if indexDigit == 0 {
			for indexListPixelCharacter, listPixelCharacter := range numberBaseIdentifcationListDigit1 {
				//0-9

				//if indexListPixelCharacter == 0 {
				//indexListPixelCharacter+=1
				//}
				isMatchPixel := true
				//fmt.Println("indexListPixelCharacter")
				//fmt.Println(indexListPixelCharacter)
				//fmt.Println(listPixelCharacter)

				for index1, listValues := range listPixelCharacter {
					//if index1 == 0 {
					//index1+=1
					//}
					if index1 == 7 {
						break
					}
					if len(listValues) != len(listCalculationIndex[index1]) {
						isMatchPixel = false
					}
				}
				if isMatchPixel {
					//fmt.Println("match in 0")
					indexMatching = indexListPixelCharacter + 1
					break
				}
			}
		}
		if indexDigit == 1 {
			for indexListPixelCharacter, listPixelCharacter := range numberBaseIdentifcationListDigit2 {
				//0-9
				isMatchPixel := true
				//fmt.Println("indexListPixelCharacter")
				//fmt.Println(indexListPixelCharacter)
				//fmt.Println(listPixelCharacter)

				for index1, listValues := range listPixelCharacter {
					if index1 == 7 {
						break
					}
					if len(listValues) != len(listCalculationIndex[index1]) {
						isMatchPixel = false
					}
				}
				if isMatchPixel {
					//fmt.Println("match in 1")
					indexMatching = indexListPixelCharacter
					break
				}
			}
		}
		if indexDigit == 2 {
			for indexListPixelCharacter, listPixelCharacter := range numberBaseIdentifcationListDigit3 {
				//0-9
				isMatchPixel := true
				//fmt.Println("indexListPixelCharacter")
				//fmt.Println(indexListPixelCharacter)
				//fmt.Println(listPixelCharacter)

				for index1, listValues := range listPixelCharacter {
					if index1 == 7 {
						break
					}
					if len(listValues) != len(listCalculationIndex[index1]) {
						isMatchPixel = false
					}
				}
				if isMatchPixel {
					//fmt.Println("match in 2")
					indexMatching = indexListPixelCharacter
					break
				}
			}
		}

		if indexDigit == 0 && indexMatching == -1 {
			continue
		}
		if indexDigit == 1 && indexMatching == -1 {
			continue
		}
		listResultsDigits = append(listResultsDigits, indexMatching)
		//indexDigit += 1
	}
	//fmt.Println("listResultsDigits")
	//fmt.Println(listResultsDigits)

	// := "Green"

	//if isRedColorBackground {
	//colorType = "Red"
	//}
	tickerNumber := ""
	//append values into string
	if len(listResultsDigits) == 3 {
		tickerNumber = strconv.Itoa(listResultsDigits[0]) + strconv.Itoa(listResultsDigits[1]) + strconv.Itoa(listResultsDigits[2])
	}
	if len(listResultsDigits) == 2 {
		tickerNumber = strconv.Itoa(listResultsDigits[0]) + strconv.Itoa(listResultsDigits[1])
	}
	if len(listResultsDigits) == 1 {
		tickerNumber = strconv.Itoa(listResultsDigits[0])
	}
	// intTicker := 0
	// if s, err := strconv.Atoi(tickerNumber); err == nil {
	// 	tickerNumberGlobal = s
	// }
	// return intTicker
	return tickerNumber
}

//

func identifyBuyPriceDigits(listCharacterPixelListsInQuestion [][]PixelObject) string {
	isRed := "false"
	for i, v := range listCharacterPixelListsInQuestion {

		for i1, v1 := range v {
			redValue := v1.RedValue
			intColor := 0
			if s, err := strconv.Atoi(redValue); err == nil {
				intColor = s
			}
			if intColor > 150 {
				isRed = "true"
				break
			}
			//break
			i1 += 1
		}

		i += 1
	}

	stringPrice := ""
	listListQuestionValues := [][]int{}
	for i, pixelList := range listCharacterPixelListsInQuestion {
		listListQuestionValues = append(listListQuestionValues, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if isRed == "true" {
				if intColor > 150 {
					listListQuestionValues[len(listListQuestionValues)-1] = append(listListQuestionValues[len(listListQuestionValues)-1], index1)
				}
			}
			if isRed == "false" {
				if intColor > 40 {
					listListQuestionValues[len(listListQuestionValues)-1] = append(listListQuestionValues[len(listListQuestionValues)-1], index1)
				}
			}

			index1 += 1
		}
		i += 1
	}

	// fmt.Println("listListQuestionValues")
	// fmt.Println(listListQuestionValues)
	listKnownCoreValues := [][][]int{}

	if isRed == "false" {
		//listKnownCoreValues = append(listKnownCoreValues,coreDigitPixelListBuyPrice0)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice1)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice2)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice3)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice4)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice5)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice6)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice7)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice8)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPrice9)
	}

	if isRed == "true" {
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed0)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed1)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed2)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed3)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed4)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed5)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed6)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed7)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed8)
		listKnownCoreValues = append(listKnownCoreValues, coreDigitPixelListBuyPriceRed9)
	}

	indexMatching := -5000

	fmt.Println("hit")
	for i, v := range listKnownCoreValues {
		fmt.Println(v)
		i += 1
	}

	fmt.Println("then")
	for i, v := range listListQuestionValues {
		fmt.Println(v)
		i += 1
	}
	// listListQuestionValues

	if len(listListQuestionValues) != 0 {
		for indexListPixelCharacter, listPixelCharacter := range listKnownCoreValues {
			isMatchPixel := false
			for index1, listValues := range listPixelCharacter {
				isMatchPixel = false
				// fmt.Println("listValues")
				// fmt.Println(listValues)
				// fmt.Println(listListQuestionValues[index1])
				if len(listValues) == len(listListQuestionValues[index1]) {
					isMatchPixel = true
				}

				if isMatchPixel == false {
					break
				}
				index1 += 1
			}
			if isMatchPixel {
				if isRed == "false" {
					indexListPixelCharacter += 1
				}
				indexMatching = indexListPixelCharacter
				break
			}
		}
	}
	stringIndex := strconv.Itoa(indexMatching)
	if isRed == "true" {
		fmt.Println("bought ticks red")
		isBoughtPriceTicksRed = true
		if stringIndex != "0" {
			stringPrice = "-" + stringIndex
		}
		if stringIndex == "0" {
			stringPrice = stringIndex
		}
		fmt.Println("stringPrice")
		fmt.Println(stringPrice)
		// if s, err := strconv.ParseFloat(stringPrice, 64); err == nil {
		// 	boughtPriceGlobal = s
		// }
	}
	if isRed == "false" {
		fmt.Println("bought ticks green")
		stringPrice = stringIndex
		// if s, err := strconv.ParseFloat(stringPrice, 64); err == nil {
		// 	boughtPriceGlobal = s
		// }
	}
	return stringPrice
}

var isBoughtPriceTicksRed = false

func identifyPriceDigits(listCharacterPixelListsInQuestion [][][]PixelObject) string {
	//red
	// fmt.Println("hit1")
	// fmt.Println("listCharacterPixelLists0")
	// fmt.Println(listCharacterPixelLists0)
	numberBaseIdentifcationList := [][][]int{}
	listCalculation0 := [][]int{}
	for i, pixelList := range listCharacterPixelLists0[3] {
		listCalculation0 = append(listCalculation0, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation0[len(listCalculation0)-1] = append(listCalculation0[len(listCalculation0)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}

	// fmt.Println("hit1.1")
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation0)
	listCalculation1 := [][]int{}
	for i, pixelList := range listCharacterPixelLists1[3] {
		listCalculation1 = append(listCalculation1, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation1[len(listCalculation1)-1] = append(listCalculation1[len(listCalculation1)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation1)
	listCalculation2 := [][]int{}
	for i, pixelList := range listCharacterPixelLists2[3] {
		listCalculation2 = append(listCalculation2, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation2[len(listCalculation2)-1] = append(listCalculation2[len(listCalculation2)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation2)
	listCalculation3 := [][]int{}
	for i, pixelList := range listCharacterPixelLists3[3] {
		listCalculation3 = append(listCalculation3, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation3[len(listCalculation3)-1] = append(listCalculation3[len(listCalculation3)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation3)
	listCalculation4 := [][]int{}
	for i, pixelList := range listCharacterPixelLists4[3] {
		listCalculation4 = append(listCalculation4, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation4[len(listCalculation4)-1] = append(listCalculation4[len(listCalculation4)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation4)
	listCalculation5 := [][]int{}
	for i, pixelList := range listCharacterPixelLists5[3] {
		listCalculation5 = append(listCalculation5, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation5[len(listCalculation5)-1] = append(listCalculation5[len(listCalculation5)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation5)
	listCalculation6 := [][]int{}
	for i, pixelList := range listCharacterPixelLists6[3] {
		listCalculation6 = append(listCalculation6, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation6[len(listCalculation6)-1] = append(listCalculation6[len(listCalculation6)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation6)
	listCalculation7 := [][]int{}
	for i, pixelList := range listCharacterPixelLists7[3] {
		listCalculation7 = append(listCalculation7, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation7[len(listCalculation7)-1] = append(listCalculation7[len(listCalculation7)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation7)
	listCalculation8 := [][]int{}
	for i, pixelList := range listCharacterPixelLists8[3] {
		listCalculation8 = append(listCalculation8, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation8[len(listCalculation8)-1] = append(listCalculation8[len(listCalculation8)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation8)
	listCalculation9 := [][]int{}
	for i, pixelList := range listCharacterPixelLists9[3] {
		listCalculation9 = append(listCalculation9, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation9[len(listCalculation9)-1] = append(listCalculation9[len(listCalculation9)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation9)
	// fmt.Println("hit2")
	//green
	numberBaseIdentifcationListGreen := [][][]int{}
	listCalculation0Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists0Green[3] {
		listCalculation0Green = append(listCalculation0Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation0Green[len(listCalculation0Green)-1] = append(listCalculation0Green[len(listCalculation0Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation0Green)
	listCalculation1Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists1Green[3] {
		listCalculation1Green = append(listCalculation1Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation1Green[len(listCalculation1Green)-1] = append(listCalculation1Green[len(listCalculation1Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation1Green)
	listCalculation2Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists2Green[3] {
		listCalculation2Green = append(listCalculation2Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation2Green[len(listCalculation2Green)-1] = append(listCalculation2Green[len(listCalculation2Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation2Green)
	listCalculation3Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists3Green[3] {
		listCalculation3Green = append(listCalculation3Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation3Green[len(listCalculation3Green)-1] = append(listCalculation3Green[len(listCalculation3Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation3Green)
	listCalculation4Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists4Green[3] {
		listCalculation4Green = append(listCalculation4Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation4Green[len(listCalculation4Green)-1] = append(listCalculation4Green[len(listCalculation4Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation4Green)
	listCalculation5Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists5Green[3] {
		listCalculation5Green = append(listCalculation5Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation5Green[len(listCalculation5Green)-1] = append(listCalculation5Green[len(listCalculation5Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation5Green)
	listCalculation6Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists6Green[3] {
		listCalculation6Green = append(listCalculation6Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation6Green[len(listCalculation6Green)-1] = append(listCalculation6Green[len(listCalculation6Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation6Green)
	listCalculation7Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists7Green[3] {
		listCalculation7Green = append(listCalculation7Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation7Green[len(listCalculation7Green)-1] = append(listCalculation7Green[len(listCalculation7Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation7Green)
	listCalculation8Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists8Green[3] {
		listCalculation8Green = append(listCalculation8Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation8Green[len(listCalculation8Green)-1] = append(listCalculation8Green[len(listCalculation8Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation8Green)
	listCalculation9Green := [][]int{}
	for i, pixelList := range listCharacterPixelLists9Green[3] {
		listCalculation9Green = append(listCalculation9Green, []int{})
		for index1, pixel := range pixelList {
			intColor := 0
			if s, err := strconv.Atoi(pixel.RedValue); err == nil {
				intColor = s
			}
			if intColor > 200 {
				listCalculation9Green[len(listCalculation9Green)-1] = append(listCalculation9Green[len(listCalculation9Green)-1], index1)
			}
			index1 += 1
		}
		i += 1
	}
	numberBaseIdentifcationListGreen = append(numberBaseIdentifcationListGreen, listCalculation9Green)

	//numberBaseIdentifcationList = append(numberBaseIdentifcationList, listCalculation9)
	//isPictureInQuestionRed := false
	//fmt.Println(listCharacterPixelLists0Green)
	//fmt.Println("hey")
	//check coloration background of picture, if red or green
	//fmt.Println("listCharacterPixelListsInQuestion")
	//fmt.Println(listCharacterPixelListsInQuestion)
	//fmt.Println(listCharacterPixelListsInQuestion[0][0][0].RedValue)
	//fmt.Println("hey")

	// fmt.Println("hit3")
	isRedColorBackground := true
	intColor := 0
	if s, err := strconv.Atoi(listCharacterPixelListsInQuestion[0][0][0].RedValue); err == nil {
		intColor = s
	}
	if intColor == 0 {
		isRedColorBackground = false
	}
	//fmt.Println(isRedColorBackground)
	digitContainer := [][][]int{}
	//for each  character do identification, and have base body calculation samples modulated for digits.
	for indexDigit, digitInQuestion := range listCharacterPixelListsInQuestion {

		digitContainer = append(digitContainer, [][]int{})
		for indexPixel, pixelList := range digitInQuestion {
			digitContainer[len(digitContainer)-1] = append(digitContainer[len(digitContainer)-1], []int{})
			for indexPixel1, pixel := range pixelList {
				intColor := 0
				if s, err := strconv.Atoi(pixel.RedValue); err == nil {
					intColor = s
				}
				if intColor > 200 {
					//listCalculationIndex[len(listCalculationIndex)-1] = append(listCalculationIndex[len(listCalculationIndex)-1], indexPixel1)
					digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1] = append(digitContainer[len(digitContainer)-1][len(digitContainer[len(digitContainer)-1])-1], indexPixel1)
				}
			}
			indexPixel += 1
		}
		indexDigit += 1
	}

	listResultsDigits := []int{}
	for i, v := range digitContainer {
		if i == 4 {
			continue
		}
		//fmt.Println(v)
		indexMatching := -1
		if isRedColorBackground {
			// fmt.Println("red")
			for indexListPixelCharacter0, listPixelCharacter := range numberBaseIdentifcationList {
				//0-9
				isMatchPixel := true
				//fmt.Println("len(listPixelCharacter)")
				//fmt.Println(len(listPixelCharacter))
				//fmt.Println(listPixelCharacter)

				for index1, listValues := range listPixelCharacter {
					//if index1 == 5 {
					//break
					//}
					if len(listValues) != len(v[index1]) {
						isMatchPixel = false
					}
				}
				if isMatchPixel {
					indexMatching = indexListPixelCharacter0
					break
				}
			}
		}
		if isRedColorBackground == false {
			// fmt.Println("green")
			//fmt.Println("len(listCalculationIndex)")
			//fmt.Println(len(listCalculationIndex))
			//fmt.Println(listCalculationIndex)
			for indexListPixelCharacter1, listPixelCharacter := range numberBaseIdentifcationListGreen {
				//0-9
				isMatchPixel := true
				//fmt.Println("len(listPixelCharacter)")
				//fmt.Println(len(listPixelCharacter))
				//fmt.Println(listPixelCharacter)

				for index1, listValues := range listPixelCharacter {

					if len(listValues) != len(v[index1]) {
						isMatchPixel = false
					}
				}
				if isMatchPixel {
					indexMatching = indexListPixelCharacter1
					break
				}
			}
		}
		listResultsDigits = append(listResultsDigits, indexMatching)

		//i+=1
	}

	//colorType := "Green"
	//fmt.Println(listResultsDigits)
	//if isRedColorBackground {
	//colorType = "Red"
	//}

	isNoMatchFound := false
	//if any digits == -1 then
	for i, v := range listResultsDigits {
		if v == -1 {
			isNoMatchFound = true
		}
		i += 1
	}

	stringPrice := strconv.Itoa(listResultsDigits[0]) + strconv.Itoa(listResultsDigits[1]) + strconv.Itoa(listResultsDigits[2]) + strconv.Itoa(listResultsDigits[3]) + "." + strconv.Itoa(listResultsDigits[4]) + strconv.Itoa(listResultsDigits[5])

	if isNoMatchFound {
		//send email post last price,
		//
		if isLastErrorEmail {
			if isRecoveryLastErrorEmail {
				postLastPriceErrorEmail(lastCalculatedPrice)
				isLastErrorEmail = false
				isRecoveryLastErrorEmail = false
			}
		}

		stringPrice = "0"
	}
	//append values into string
	// fmt.Println("stringPrice1")
	// fmt.Println(stringPrice)
	if isNoMatchFound == false {
		lastCalculatedPrice = stringPrice
		if isRecoveryLastErrorEmail == false {
			postLastPriceErrorEmail(lastCalculatedPrice)
			isLastErrorEmail = true
			isRecoveryLastErrorEmail = true
		}
	}
	//parse into float
	//stringPrice := ""
	//fmt.Println("indexMatching")
	//fmt.Println(indexMatching)
	//fmt.Println(colorType)
	//fmt.Println(questionListCharacters)
	//fmt.Println(numberBaseIdentifcationList[8])
	//fmt.Println(numberBaseIdentifcationListGreen[9])

	//append values

	//convert price
	// floatPrice, err := strconv.Atoi(stringPrice)
	// if err != nil {
	// }
	// if s, err := strconv.ParseFloat(stringPrice, 64); err == nil {
	// 	currentPriceGlobal = s
	// }
	return stringPrice
}

var isLastErrorEmail = false
var isRecoveryLastErrorEmail = true

func seperateCharacters(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	//listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})

	for i, listPixelObjects := range listPixelListY {
		isContinue := false
		if i > 11 {
			if i == 12 || i == 19 || i == 26 || i == 33 || i == 43 || i == 50 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
			}
			if i == 40 {
				isContinue = true
				//break
			}
			if i == 43 {
				isContinue = false
				//break
			}

			if isContinue {
				continue
			}
			if i == 57 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
	}
	return listCharacterPixelLists
}

var lastCalculatedPrice = "0"

func seperateAccountCalculit0(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}

	//

	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 824 {
				break
			}
			if YValue > 814 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}

	//

	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 780 {
			if XValue == 781 || XValue == 788 || XValue == 795 || XValue == 805 || XValue == 812 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
			}
			if XValue == 802 {
				isContinue = true
			}
			if XValue == 805 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 819 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		// if XValue > 779 {
		// 	if XValue == 780 || XValue == 787 || XValue == 794 || XValue == 804 || XValue == 811 {
		// 		listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
		// 	}
		// 	if XValue == 801 {
		// 		isContinue = true
		// 	}
		// 	if XValue == 804 {
		// 		isContinue = false
		// 	}

		// 	if isContinue {
		// 		continue
		// 	}
		// 	if XValue == 818 {
		// 		break
		// 	}
		// 	listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		// }
		index += 1
	}
	return listCharacterPixelLists
}

func seperateAccountCalculit0Updated(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	//

	for index, listPixelObjects := range listPixelListY {

		//why is it appending extra lists for certain indexs?

		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})

		//here it's populating each  list...?
		// fmt.Println("listCharacterPixelLists0")
		// fmt.Println(listCharacterPixelLists0)

		//adds into columns, this creates columns of all valid values.
		// if index == 1 || index == 2 {
		// 	fmt.Println("listCharacterPixelLists0")
		// 	fmt.Println(listCharacterPixelLists0)
		// }
		// fmt.Println(index)
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 266 {
				break
			}
			if YValue > 256 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}
	// fmt.Println("listCharacterPixelLists0")
	// fmt.Println(listCharacterPixelLists0)

	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 265 {
			if XValue == 266 || XValue == 273 || XValue == 280 || XValue == 290 || XValue == 297 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})

				//here still no, tis a list of pixel values. not what I am looking for.
				// if XValue == 266 || index == 273 {
				// 	fmt.Println(index)
				// 	fmt.Println("listCharacterPixelLists")
				// 	fmt.Println(listCharacterPixelLists)
				// }
			}

			if XValue == 271 || XValue == 272 {
				continue
			}
			if XValue == 278 || XValue == 279 {
				continue
			}
			if XValue == 285 || XValue == 286 {
				continue
			}
			// if XValue == 295 { //|| XValue == 296 {
			// 	continue
			// }
			// if XValue == 298 { //|| XValue == 298 {
			// 	continue
			// }

			//

			if XValue == 287 {
				isContinue = true
			}
			if XValue == 290 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 304 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}

	return listCharacterPixelLists
}

func seperateAccountCalculit1(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 824 {
				break
			}
			if YValue > 814 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}
	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 779 {
			if XValue == 780 || XValue == 790 || XValue == 797 || XValue == 804 || XValue == 814 || XValue == 821 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
			}
			if XValue == 787 {
				isContinue = true
			}
			if XValue == 790 {
				isContinue = false
			}

			if XValue == 811 {
				isContinue = true
			}
			if XValue == 814 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 828 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	return listCharacterPixelLists
}

func seperateAccountCalculit2(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 824 {
				break
			}
			if YValue > 814 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}
	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 772 {
			if XValue == 773 || XValue == 780 || XValue == 790 || XValue == 797 || XValue == 804 || XValue == 814 || XValue == 821 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
			}
			if XValue == 787 {
				isContinue = true
			}
			if XValue == 790 {
				isContinue = false
			}

			if XValue == 811 {
				isContinue = true
			}
			if XValue == 814 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 828 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	return listCharacterPixelLists
}

func seperateCharactersAccount0(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}

	//where no commas
	for index, listPixelObjects := range listPixelListY {
		if index == 171 {
			break
		}

		if index > 112 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			//fmt.Println(len(listPixelObjects))

			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 687 {
					break
				}
				if YValue > 669 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}
	//now having pixels in range
	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false

		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 119 {
			if XValue == 120 || XValue == 127 || XValue == 134 || XValue == 141 || XValue == 148 || XValue == 151 || XValue == 158 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
				//fmt.Println("Hit")
			}
			//exlude . in calculation
			if XValue == 148 {
				isContinue = true
			}
			if XValue == 151 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 165 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	return listCharacterPixelLists
}

func seperateCharactersLastPrice(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}

	listCharacterPixelLists0 := [][]PixelObject{}
	for index, listPixelObjects := range listPixelListY {
		if index == 171 {
			break
		}

		if index > 112 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			//fmt.Println(len(listPixelObjects))

			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 687 {
					break
				}
				if YValue > 669 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}
	//now having pixels in range
	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false

		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 119 {
			if XValue == 120 || XValue == 127 || XValue == 134 || XValue == 141 || XValue == 148 || XValue == 151 || XValue == 158 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
				//fmt.Println("Hit")
			}
			//exlude . in calculation
			if XValue == 148 {
				isContinue = true
			}
			if XValue == 151 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 165 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	return listCharacterPixelLists
}

func seperateCharactersAccountIdentifer(listPixelListY [][]PixelObject) [][]PixelObject {
	listCharacterPixelLists := [][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	listCharacterPixelLists = append(listCharacterPixelLists, []PixelObject{})

	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 824 {
				break
			}
			if YValue > 814 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}

	//

	for index, listPixelObjects := range listCharacterPixelLists0 {
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 810 {

			if XValue == 818 {
				break
			}
			listCharacterPixelLists = append(listCharacterPixelLists, listPixelObjects)
		}
		index += 1
	}

	return listCharacterPixelLists
}

func seperateCharactersAccountIdentiferUpdated(listPixelListY [][]PixelObject) [][]PixelObject {
	listCharacterPixelLists := [][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	listCharacterPixelLists = append(listCharacterPixelLists, []PixelObject{})

	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 266 {
				break
			}
			if YValue > 256 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}

	for index, listPixelObjects := range listCharacterPixelLists0 {
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 265 {
			if XValue == 271 {
				break
			}
			listCharacterPixelLists = append(listCharacterPixelLists, listPixelObjects)
		}
		index += 1
	}

	return listCharacterPixelLists
}

func seperateCharactersBuyPrice(listPixelListY [][]PixelObject) [][]PixelObject {
	listCharacterPixelLists := [][]PixelObject{}
	//date~2021~1~20~18~14~37~
	listCharacterPixelLists0 := [][]PixelObject{}

	//
	listCharacterPixelLists = append(listCharacterPixelLists, []PixelObject{})

	for index, listPixelObjects := range listPixelListY {
		if index == 202 {
			break
		}

		if index > 163 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 750 {
					break
				}
				if YValue > 736 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}

	//now having pixels in range
	for index, listPixelObjects := range listCharacterPixelLists0 {
		//isContinue := false
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 172 {

			if XValue == 187 {
				break
			}
			listCharacterPixelLists = append(listCharacterPixelLists, listPixelObjects)
		}
		index += 1
	}
	return listCharacterPixelLists
}

func seperateCharactersBuyPriceUpdated(listPixelListY [][]PixelObject) [][]PixelObject {
	listCharacterPixelLists := [][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	listCharacterPixelLists = append(listCharacterPixelLists, []PixelObject{})
	for index, listPixelObjects := range listPixelListY {
		if index == 202 {
			break
		}
		if index > 163 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 748 {
					break
				}
				if YValue > 738 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}
	for index, listPixelObjects := range listCharacterPixelLists0 {
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 181 {
			if XValue == 187 {
				break
			}
			listCharacterPixelLists = append(listCharacterPixelLists, listPixelObjects)
		}
		index += 1
	}
	return listCharacterPixelLists
}
func seperateCharactersTicker(listPixelListY [][]PixelObject) [][][]PixelObject {
	listCharacterPixelLists := [][][]PixelObject{}
	//listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})

	listCharacterPixelLists0 := [][]PixelObject{}
	//fmt.Println(len(listPixelListY))
	for index, listPixelObjects := range listPixelListY {
		if index == 1209 {
			break
		}

		if index > 1186 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			//fmt.Println(len(listPixelObjects))

			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 896 {
					break
				}
				if YValue > 881 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}

	//fmt.Println("listCharacterPixelLists0")
	//fmt.Println(listCharacterPixelLists0)

	//now having pixels in range, 4 cross
	for index, listPixelObjects := range listCharacterPixelLists0 {
		//isContinue := false

		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 1188 {
			if XValue == 1189 || XValue == 1195 || XValue == 1201 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
			}
			if XValue == 1194 {
				continue
			}
			if XValue == 1200 {
				continue
			}
			if XValue == 1206 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}

	return listCharacterPixelLists
}

//have be recursive,
var isSnip = true

func callSnip() {
	if isSnip {
		postSnipCapture()
	}
}

func calculateStructureObjectsDimensionsNumbers(listPixelListY [][]PixelObject) []StructureObject {
	listStructureObject := []StructureObject{StructureObject{}}
	isAddedPixels := false
	//if for x coordinate if list not
	for indexPixelObject, listPixelObject := range listPixelListY {
		if len(listPixelObject) > 0 {
			//add all values to structure object
			for indexPixelObject, pixelObject := range listPixelObject {
				listStructureObject[len(listStructureObject)-1].ListPixelObject = append(listStructureObject[len(listStructureObject)-1].ListPixelObject, pixelObject)
				indexPixelObject += 1
			}
			isAddedPixels = true
		}
		if len(listPixelObject) == 0 {
			if isAddedPixels {
				listStructureObject = append(listStructureObject, StructureObject{})
				isAddedPixels = false
			}
		}
		indexPixelObject += 1
	}
	return listStructureObject
}

func callbackReadCSVPixels() {
	//parse structures
}

func callbackReadCSVBlackArrow() {

}

func postClickSelectByPixelLocation(pixelValue string) string {
	json := "{ \"requestType\": \"clickPixel\",\"pixelValue\": \"" + pixelValue + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func postDeleteScreenshots() string {
	json := "{ \"request_type\": \"deleteScreenshots\"}"
	url := "http://localhost:3000/api/brokerage"
	response := post(url, json)
	return response
}
func postFixSucceedEmail() string {
	json := "{ \"request_type\": \"fixSucceedEmail\"}"
	url := "http://localhost:3000/api/brokerage"
	response := post(url, json)
	return response
}
func postLastPriceErrorEmail(lastPrice string) string {
	json := `{
		"request_type": "lastPriceErrorEmail",
		"lastPrice":"` + lastPrice + `"
		}`
	url := "http://localhost:3000/api/brokerage"
	response := post(url, json)
	return response
}

func postToPyFixUpdate() string {
	json := "{ \"requestType\": \"fuvrFixUpdate\"}"
	url := "http://0.0.0.0:12002/databaseQuery"
	response := post(url, json)
	return response
}

func postToPyFixMonitor() string {
	json := "{ \"requestType\": \"fuvrFixMonitor\"}"
	url := "http://0.0.0.0:4462/api"
	response := post(url, json)
	return response
}

func postSnipCapture() string {
	json := "{ \"requestType\": \"snipCapture\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}
func postDoRecycle() string {
	json := "{ \"requestType\": \"doRecycle\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func queryRenameFile(fileName string) string {
	json := `{
		"request_type": "renameFile",
		"fileName":"` + fileName + `"
		}`
	url := "http://localhost:3000/api/brokerage"
	response := post(url, json)
	return response
}

func postTakePictureSnip(coordinate1 string, coordinate2 string, filename string) string {
	json := "{ \"requestType\": \"takePictureSnip\",\"coordinate1\": \"" + coordinate1 + "\",\"coordinate2\": \"" + coordinate2 + "\",\"filename\": \"" + filename + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func postTakePictureSnipUpdated(fileName string) string {
	json := "{ \"requestType\": \"takePictureSnip\",\"fileName\": \"" + fileName + "\",\"index\": \"" + strconv.Itoa(indexPostMarketPriceReadByFileName3) + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func postDeletePricePictures() string {
	json := "{ \"requestType\": \"deletePricePictures\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func postMarketPriceReadByFileName(fileName string) string {
	json := "{ \"requestType\": \"marketPriceReadByFileName\",\"fileName\": \"" + fileName + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func postMarketPriceReadByFileName1(fileName string) string {
	json := "{ \"requestType\": \"marketPriceReadByFileName1\",\"fileName\": \"" + fileName + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func postMarketPriceReadByFileName2(fileName string) string {
	json := "{ \"requestType\": \"marketPriceReadByFileName2\",\"fileName\": \"" + fileName + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

//,\"index\": \"" + strconv.Itoa(indexPostMarketPriceReadByFileName3) + "\"
func postMarketPriceReadByFileName3(fileName string) string {
	json := "{ \"requestType\": \"marketPriceReadByFileName3\",\"fileName\": \"" + fileName + "\",\"index\": \"" + strconv.Itoa(indexPostMarketPriceReadByFileName3) + "\"}"
	url := "http://0.0.0.0:4461/api"
	response := post(url, json)
	return response
}

func storeUpdateEmail() string {
	json := "{ \"request_type\": \"storeUpdateEmail\"}"
	url := "http://localhost:3000/api/brokerage"
	response := post(url, json)
	return response
}

func post(url string, json string) string {
	bodyStringed := ""
	var jsonStr = []byte(json)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		// panic(err)
		fmt.Println(err)
		bodyStringed = "error received"
	}
	if err == nil {
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.Status)
		fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)
		bodyStringed = string(body)
	}
	// fmt.Println("response Body:", string(body))
	return bodyStringed
}

// listNumbersCategory := []int{10, 3, 2, 9, 4, 6, 5, 0, 1}
// pixelList := []PixelObject{PixelObject{YValue: "10"}, PixelObject{YValue: "3"}, PixelObject{YValue: "2"}, PixelObject{YValue: "4"}}
// sortedpixelList := sortPixelListLowToHigh(pixelList)
// fmt.Println(sortedpixelList)

// func calculateDimensionsPixelObjectDigits(fileName string) [][]PixelObject {
// 	// stringPixelRaw := readCSVOfPixelStringByFileName(fileName)
// 	// //need all pixels in y list black and white
// 	listPixelListY := behavioralCalculatePixelsYDigits(fileName)
// 	//listPixelListY := [][]PixelObject{}
// 	return listPixelListY
// }

// func calculateDimensionsPixelObjectDigits1(fileName string) [][]PixelObject {
// 	// stringPixelRaw := readCSVOfPixelStringByFileName1(fileName)
// 	listPixelListY := behavioralCalculatePixelsYDigits(fileName)
// 	//listPixelListY := [][]PixelObject{}
// 	return listPixelListY
// }
// func calculateDimensionsPixelObjectDigits2(fileName string) [][]PixelObject {
// 	// stringPixelRaw := readCSVOfPixelStringByFileName2(fileName)
// 	fileName1 := "C:\\Users\\Bayliss Carr\\Pictures\\identifierStore\\" + fileName

// 	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)
// 	//listPixelListY := [][]PixelObject{}
// 	return listPixelListY
// }
func calculateDimensionsPixelObjectDigits2(fileName string) [][]PixelObject {
	// stringPixelRaw := readCSVOfPixelStringByFileName2(fileName)
	// fileName1 := "C:\\Users\\Bayliss Carr\\Pictures\\identifierStore\\" + fileName

	///Users/CommanderCarr/coding/go/fuvr
	//Wallaces-Air:identifierStore CommanderCarr$ pwd
	// /Users/CommanderCarr/coding/go/fuvr/identifierStore

	//C:\Users\Bayliss Carr\Coding\fuvr>
	// fileName1 := "/Users/CommanderCarr/coding/go/fuvr/identifierStore/" + fileName

	// fileName1 := `C:\Users\Bayliss Carr\Coding\fuvr\identifierStore\` + fileName
	fileName1 := `/Users/CommanderCarr/coding/go/fuvr/identifierStore/` + fileName
	// fileName1 := `C:\Users\Bayliss Carr\Coding\fuvr\identifierStore\` + fileName
	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)
	//listPixelListY := [][]PixelObject{}

	//only needs to be done once
	// for
	return listPixelListY
}
func calculateDimensionsPixelObjectDigitsStore2(fileName string, fileToStore string) [][]PixelObject {
	fileName1 := `C:\Users\Bayliss Carr\Coding\fuvr\identifierStore\` + fileName
	// fmt.Println("oh0")
	// fileName1 := `/Users/CommanderCarr/coding/go/fuvr/identifierStore/` + fileName
	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)

	listCharacterPixelLists := [][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	listCharacterPixelLists = append(listCharacterPixelLists, []PixelObject{})
	for index, listPixelObjects := range listPixelListY {
		if index == 202 {
			break
		}
		if index > 163 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 748 {
					break
				}
				if YValue > 738 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}
	for index, listPixelObjects := range listCharacterPixelLists0 {
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 181 {
			if XValue == 187 {
				break
			}
			listCharacterPixelLists = append(listCharacterPixelLists, listPixelObjects)
		}
		index += 1
	}

	stringToStore := ""
	for i, listPixelObject := range listCharacterPixelLists {
		columnString := ""
		for i1, pixelObject := range listPixelObject {
			pixelObjectString := pixelObject.BlueValue + "~" + pixelObject.ColorationIdentification + "~" + pixelObject.GreenValue + "~" + pixelObject.RedValue + "~" + pixelObject.XValue + "~" + pixelObject.YValue
			columnString += pixelObjectString
			if i != (len(listPixelObject) - 1) {
				columnString += ","
			}
			i1++
		}
		if i != (len(listPixelListY) - 1) {
			columnString += "$"
		}
		stringToStore += columnString
		i++
	}
	fileNameToStore := fileToStore + ".txt"

	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, fileNameToStore, stringToStore)
	// writeToFile(`/Users/CommanderCarr/coding/go/fuvr`, fileNameToStore, stringToStore)
	//
	return listCharacterPixelLists
}
func calculateDimensionsPixelObjectDigitsStore2Account(fileName string, fileToStore string) [][]PixelObject {
	fileName1 := `C:\Users\Bayliss Carr\Coding\fuvr\identifierStore\` + fileName
	// fmt.Println("oh0")
	// fileName1 := `/Users/CommanderCarr/coding/go/fuvr/identifierStore/` + fileName
	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)

	listCharacterPixelLists := [][]PixelObject{}
	listCharacterPixelLists0 := [][]PixelObject{}
	listCharacterPixelLists = append(listCharacterPixelLists, []PixelObject{})
	for index, listPixelObjects := range listPixelListY {
		listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
		for index1, value := range listPixelObjects {
			YValue, err := strconv.Atoi(value.YValue)
			if err != nil {
			}
			if YValue == 266 {
				break
			}
			if YValue > 256 {
				listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
			}
			index1 += 1
		}
		index += 1
	}

	for index, listPixelObjects := range listCharacterPixelLists0 {
		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 265 {
			if XValue == 271 {
				break
			}
			listCharacterPixelLists = append(listCharacterPixelLists, listPixelObjects)
		}
		index += 1
	}

	stringToStore := ""
	for i, listPixelObject := range listCharacterPixelLists {
		columnString := ""
		for i1, pixelObject := range listPixelObject {
			pixelObjectString := pixelObject.BlueValue + "~" + pixelObject.ColorationIdentification + "~" + pixelObject.GreenValue + "~" + pixelObject.RedValue + "~" + pixelObject.XValue + "~" + pixelObject.YValue
			columnString += pixelObjectString
			if i != (len(listPixelObject) - 1) {
				columnString += ","
			}
			i1++
		}
		if i != (len(listPixelListY) - 1) {
			columnString += "$"
		}
		stringToStore += columnString
		i++
	}
	fileNameToStore := fileToStore + ".txt"

	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr\`, fileNameToStore, stringToStore)
	// writeToFile(`/Users/CommanderCarr/coding/go/fuvr`, fileNameToStore, stringToStore)
	//
	return listCharacterPixelLists
}

func calculateDimensionsPixelObjectDigitsStore23Dimension(fileName string, fileToStore string) [][][]PixelObject {
	fileName1 := `C:\Users\Bayliss Carr\Coding\fuvr\identifierStore\` + fileName
	// fmt.Println("oh0")
	// fileName1 := `/Users/CommanderCarr/coding/go/fuvr/identifierStore/` + fileName
	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)

	listCharacterPixelLists := [][][]PixelObject{}

	listCharacterPixelLists0 := [][]PixelObject{}
	for index, listPixelObjects := range listPixelListY {
		if index == 171 {
			break
		}

		if index > 112 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			//fmt.Println(len(listPixelObjects))

			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 687 {
					break
				}
				if YValue > 669 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}
	//now having pixels in range
	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false

		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 119 {
			if XValue == 120 || XValue == 127 || XValue == 134 || XValue == 141 || XValue == 148 || XValue == 151 || XValue == 158 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
				//fmt.Println("Hit")
			}
			//exlude . in calculation
			if XValue == 148 {
				isContinue = true
			}
			if XValue == 151 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 165 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	// return listCharacterPixelLists

	stringToStore := ""
	for i, listPixelObject := range listCharacterPixelLists {
		greaterString := ""
		for i2, v := range listPixelObject {
			columnString := ""
			for i1, pixelObject := range v {
				pixelObjectString := pixelObject.BlueValue + "~" + pixelObject.ColorationIdentification + "~" + pixelObject.GreenValue + "~" + pixelObject.RedValue + "~" + pixelObject.XValue + "~" + pixelObject.YValue
				columnString += pixelObjectString
				if i != (len(listPixelObject) - 1) {
					columnString += ","
				}
				i1++
			}
			if i2 != (len(listPixelListY) - 1) {
				columnString += "$"
			}
			greaterString += columnString
			//
			i2++
		}
		if i != (len(listPixelListY) - 1) {
			greaterString += "!"
		}
		stringToStore += greaterString
		i++
	}
	// fileNameToStore := fileToStore + ".txt"

	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr\`, fileToStore, stringToStore)
	// writeToFile(`/Users/CommanderCarr/coding/go/fuvr`, fileToStore, stringToStore)
	//
	return listCharacterPixelLists
}

func calculateDimensionsPixelObjectDigitsStore23DimensionTick(fileName string, fileToStore string) [][][]PixelObject {
	// fileName1 := `C:\Users\Bayliss Carr\Coding\fuvr\` + fileName
	// fmt.Println("oh0")
	fileName1 := `/Users/CommanderCarr/coding/go/fuvr/identifierStore/` + fileName
	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)

	listCharacterPixelLists := [][][]PixelObject{}

	listCharacterPixelLists0 := [][]PixelObject{}
	for index, listPixelObjects := range listPixelListY {
		if index == 171 {
			break
		}

		if index > 112 {
			listCharacterPixelLists0 = append(listCharacterPixelLists0, []PixelObject{})
			//fmt.Println(len(listPixelObjects))

			for index1, value := range listPixelObjects {
				YValue, err := strconv.Atoi(value.YValue)
				if err != nil {
				}
				if YValue == 687 {
					break
				}
				if YValue > 669 {
					listCharacterPixelLists0[len(listCharacterPixelLists0)-1] = append(listCharacterPixelLists0[len(listCharacterPixelLists0)-1], value)
				}
				index1 += 1
			}
		}
	}
	//now having pixels in range
	for index, listPixelObjects := range listCharacterPixelLists0 {
		isContinue := false

		XValue, err := strconv.Atoi(listPixelObjects[0].XValue)
		if err != nil {
		}
		if XValue > 119 {
			if XValue == 120 || XValue == 127 || XValue == 134 || XValue == 141 || XValue == 148 || XValue == 151 || XValue == 158 {
				listCharacterPixelLists = append(listCharacterPixelLists, [][]PixelObject{})
				//fmt.Println("Hit")
			}
			//exlude . in calculation
			if XValue == 148 {
				isContinue = true
			}
			if XValue == 151 {
				isContinue = false
			}

			if isContinue {
				continue
			}
			if XValue == 165 {
				break
			}
			listCharacterPixelLists[len(listCharacterPixelLists)-1] = append(listCharacterPixelLists[len(listCharacterPixelLists)-1], listPixelObjects)
		}
		index += 1
	}
	// return listCharacterPixelLists

	stringToStore := ""
	for i, listPixelObject := range listCharacterPixelLists {
		greaterString := ""
		for i2, v := range listPixelObject {
			columnString := ""
			for i1, pixelObject := range v {
				pixelObjectString := pixelObject.BlueValue + "~" + pixelObject.ColorationIdentification + "~" + pixelObject.GreenValue + "~" + pixelObject.RedValue + "~" + pixelObject.XValue + "~" + pixelObject.YValue
				columnString += pixelObjectString
				if i != (len(listPixelObject) - 1) {
					columnString += ","
				}
				i1++
			}
			if i2 != (len(listPixelListY) - 1) {
				columnString += "$"
			}
			greaterString += columnString
			//
			i2++
		}
		if i != (len(listPixelListY) - 1) {
			greaterString += "!"
		}
		stringToStore += greaterString
		i++
	}
	// fileNameToStore := fileToStore + ".txt"

	// writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, fileNameToStore, stringToStore)
	writeToFile(`/Users/CommanderCarr/coding/go/fuvr`, fileToStore, stringToStore)
	//
	return listCharacterPixelLists
}

//

func calculateDimensionsPixelObjectDigits3() [][]PixelObject {
	fileName1 := "./saveCapture1.png"
	// fileName1 := "./saveCapture33.png"
	// fileName1 := "/Users/CommanderCarr/coding/go/fuvr/identifierStore/-4BuyPrice.png"
	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)
	return listPixelListY
}

var globalBuyProcessVRItterate = ""

func itterateItterateCacheValue() {
	valueRead := retrieveLastItterateFromItterateCache()

	intValueRead := 0
	if s, err := strconv.Atoi(valueRead); err == nil {
		intValueRead = s
	}
	updatedIntValueRead := intValueRead + 1
	//
	globalBuyProcessVRItterate = strconv.Itoa(updatedIntValueRead)
	updatedStringRead := strconv.Itoa((intValueRead + 1))

	writeToItterateCache(updatedStringRead)
}

var globalCloseOrOpenError = ""

func itterateCloseOrOpenErrorCacheValue() {
	valueRead := retrieveLastItterateFromItterateCache()

	intValueRead := 0
	if s, err := strconv.Atoi(valueRead); err == nil {
		intValueRead = s
	}
	updatedIntValueRead := intValueRead + 1
	//
	globalCloseOrOpenError = strconv.Itoa(updatedIntValueRead)
	updatedStringRead := strconv.Itoa((intValueRead + 1))

	writeToItterateCache(updatedStringRead)
}

func retrieveLastItterateFromItterateCache() string {
	// fileString := readFile("/Users/CommanderCarr/coding/go/fuvr/itterateCache.txt")
	fileString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\itterateCache.txt`)
	return fileString
}
func writeToItterateCache(stringValue string) {
	// writeToFile(`/Users/CommanderCarr/coding/go/fuvr/`, "itterateCache.txt", stringValue)
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr`, "itterateCache.txt", stringValue)
}

func writeToBuyAnalysisFailOrSucceedFile(stringValue string) {
	year, month, day := getDate()
	stringDay := strconv.Itoa(day)
	stringMonth := strconv.Itoa(month)
	stringYear := strconv.Itoa(year)

	hour := getCurrentHour()
	minute := getCurrentMinute()
	// second := getCurrentSecond()
	stringHour := strconv.Itoa(hour)
	stringMinute := strconv.Itoa(minute)

	//handle on time stamp
	stringToAppend := "," + stringDay + "~" + stringMonth + "~" + stringYear + "~" + stringHour + "~" + stringMinute + "~" + stringValue
	// fileString := readFile("/Users/CommanderCarr/coding/go/fuvr/itterateCache.txt")
	fileString := readFile(`C:\Users\Bayliss Carr\Coding\fuvr\buyAnalysisFailOrSucceedFile.txt`)
	// `C:\Users\Bayliss Carr\Coding\fuvr
	fileString += stringToAppend
	writeToFile(`C:\Users\Bayliss Carr\Coding\fuvr\`, "buyAnalysisFailOrSucceedFile.txt", fileString)
}
func calculateDimensionsPixelObjectDigits3BuyProcedureForAccountIdentify() [][]PixelObject {
	//well this will be handled the file name created, should have an index of saved file....

	///should have an index of cache of operation....
	//not needed can start index from a known variable and go from there,
	// should read from cache of last save value

	//yea do that have read from cache and formulate name of file to draw,
	//this can be set in a global variable.

	//

	//set to static for development testing
	// fileName1 := "./identifierStore/222C.png"
	// fileName1 := "./meow.png"
	// fileName1 := "./pictureBuyReviewStore/saveCaptureStore"
	// fileName1 := "./pictureBuyReviewStore/" + "saveCaptureStore" + globalBuyProcessVRItterate + ".png"
	// fileName1 := "./pictureBuyReviewStore/" + "saveCaptureStore" + globalBuyProcessVRItterate + ".png"
	// fileName1 := "./saveCapture88.png"
	fileName1 := "./saveCapture1.png"

	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)
	return listPixelListY
}
func calculateDimensionsPixelObjectDigits3BuyProcedureForAccountIdentify1() [][]PixelObject {
	//well this will be handled the file name created, should have an index of saved file....

	///should have an index of cache of operation....
	//not needed can start index from a known variable and go from there,
	// should read from cache of last save value

	//yea do that have read from cache and formulate name of file to draw,
	//this can be set in a global variable.

	//

	//set to static for development testing
	// fileName1 := "./identifierStore/222C.png"
	// fileName1 := "./meow.png"
	// fileName1 := "./pictureBuyReviewStore/saveCaptureStore"
	// fileName1 := "./pictureBuyReviewStore/" + "saveCaptureStore" + globalBuyProcessVRItterate + ".png"
	// fileName1 := "./pictureBuyReviewStore/" + "saveCaptureStore" + globalBuyProcessVRItterate + ".png"
	// fileName1 := "./saveCapture88.png"
	fileName1 := "./saveCapture11.png"

	listPixelListY := behavioralCalculatePixelsYDigits(fileName1)
	return listPixelListY
}

// fmt.Println(listPixelListY[12])
// listStructureObjectWhite := calculateStructureObjectsDimensions(listPixelListY)
func behavioralCalculatePixelsYDigits(fileName string) [][]PixelObject {

	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	file, err := os.Open(fileName)

	if err != nil {
		fmt.Println("Error: File could not be opened")
		os.Exit(1)
	}

	defer file.Close()

	// pixels, err := getPixels(file)

	// if err != nil {
	// 	fmt.Println("Error: Image could not be decoded")
	// 	os.Exit(1)
	// }

	img, _, err := image.Decode(file)

	if err != nil {
		// return nil, err/
		fmt.Println(err)
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y

	// fmt.Println(width)
	// fmt.Println(height)
	var listPixels []PixelObject

	//for every x value, to lim, store in list

	//listPixels
	for y := 0; y < height; y++ {
		// var row []PixelObject
		for x := 0; x < width; x++ {
			// row = append(row, rgbaToPixel(img.At(x, y).RGBA(), x, y))
			// row = append(row, rgbaToPixel(img, x, y))
			value1, v2, v3, v4 := img.At(x, y).RGBA()

			listPixels = append(listPixels, rgbaToPixel(value1, v2, v3, v4, x, y))
			// row = append(row, rgbaToPixel(value1,v2,v3,v4,x,y))
			// break
		}
		// pixels = append(pixels, row)
		// break
	}

	//add pixels to appropriate pixel containers.
	xLim := width
	// for i, v := range listPixels {
	// 	currentValue := 0
	// 	if s, err := strconv.Atoi(v.XValue); err == nil {
	// 		currentValue = s
	// 	}
	// 	if i == 0 {
	// 		xLim = currentValue
	// 		continue
	// 	}
	// 	if currentValue > xLim {
	// 		xLim = currentValue
	// 	}
	// }

	//coordinate x values into pixel value containers.
	listPixelObjectList := [][]PixelObject{}
	sum := 0
	for sum <= xLim {
		listPixelObjectList = append(listPixelObjectList, []PixelObject{})
		sum += 1
	}

	for indexPixel, pixelObject := range listPixels {
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}

		listPixelObjectList[intXValue] = append(listPixelObjectList[intXValue], pixelObject)
		indexPixel += 1
	}
	returningMatrix := listPixelObjectList

	return returningMatrix
}

func calculateDimensionsPixelObjectRedGreen() {

	stringPixelRaw := readCSVOfPixelStringNonColorSpecific()

	//red candles
	// listPixelListYRed := behavioralCalculateRedPixelsY(stringPixelRaw)
	// listStructureObjectRed := calculateStructureObjectsDimensions(listPixelListYRed)
	// fmt.Println(listStructureObjectRed[1])

	//green candles
	listPixelListYGreen := behavioralCalculateGreenPixelsY(stringPixelRaw)
	listStructureObjectGreen := calculateStructureObjectsDimensions(listPixelListYGreen)
	fmt.Println(listStructureObjectGreen[0])

	//

	//
}

// continuumsYDirectionRed := calculateContinuumsYDirectionRed(pixelObjectMatrixXDirectionRed)

// fmt.Println(len(listPixelListY))

// fmt.Println("listPixelListY")
// fmt.Println(listPixelListY)

// for i, v := range listPixelListY {
// 	fmt.Println(v)
// 	if i == 10 {
// 		break
// 	}
// }
// fmt.Println(listPixelListY[2])
// for indexContinuum, continuum := range structureContinuumsYDirectionRed {
// 	// tagSeeking := "2 257"
// 	for indexFeatureSet, featureSet := range continuum.ListFeatureSetIdentified {
// 		// for indexPixelObject, pixelObject := range featureSet.ListPixelObject {
// 		// 	tag := pixelObject.XValue + " " + pixelObject.YValue
// 		// 	if tag == tagSeeking {
// 		// 		fmt.Println("featureSet")
// 		// 		fmt.Println(featureSet)
// 		// 		fmt.Println("indexFeatureSet")
// 		// 		fmt.Println(indexFeatureSet)
// 		// 	}
// 		// 	indexPixelObject += 1
// 		// }
// 		fmt.Println(featureSet)
// 		indexFeatureSet += 1
// 	}
// 	// fmt.Println(continuum)
// 	indexContinuum += 1
// }

// fmt.Println(calculateStructureContinuumsYDirectionRed[716])
//
// sectoredPixelObjectMatrixGreen := behavioralCalculateGreenPixels(stringPixelRaw)
// listStructureObjectGreen := calculateStructureObjectsDimensions(sectoredPixelObjectMatrixGreen)

// pixelObjectMatrixXDirectionRed := behavioralCalculateRedPixels(stringPixelRaw)
// //list x, where index is list of all values at y.
// continuumsYDirectionRed := calculateContinuumsYDirectionRed(pixelObjectMatrixXDirectionRed)
//
// listStructureObjectRed := calculateStructureObjectsDimensions(pixelObjectMatrixXDirectionRed, structureContinuumsYDirectionRed)

// fmt.Println(listStructureObjectRed)
// fmt.Println(len(continuumsYDirectionRed))

var listTagCalculatedPixelConnectedCandle = []string{}

func calculateStructureObjectsDimensions(listPixelListY [][]PixelObject) []StructureObject {
	listStructureObject := []StructureObject{StructureObject{}}
	isAddedPixels := false
	//if for x coordinate if list not
	for indexPixelObject, listPixelObject := range listPixelListY {
		if len(listPixelObject) > 0 {
			//add all values to structure object
			for indexPixelObject, pixelObject := range listPixelObject {
				listStructureObject[len(listStructureObject)-1].ListPixelObject = append(listStructureObject[len(listStructureObject)-1].ListPixelObject, pixelObject)
				indexPixelObject += 1
			}
			isAddedPixels = true
		}
		if len(listPixelObject) == 0 {
			if isAddedPixels {
				listStructureObject = append(listStructureObject, StructureObject{})
				isAddedPixels = false
			}
		}
		indexPixelObject += 1
	}
	return listStructureObject
}

// firstIndexReached = true

//

// fmt.Println("listPixelObject")
// fmt.Println(listPixelObject)
// fmt.Println("pixelObjectMatrixXDirection")
// fmt.Println(pixelObjectMatrixXDirection[1])

//

// for indexPixelObject, pixelObject := range listPixelObject {
// 	pixelObjectTag := pixelObject.XValue + " " + pixelObject.YValue
// 	isPixelObjectAlreadyCalculated := false
// 	for indexCandleTag, candleTag := range listTagCalculatedPixelConnectedCandle {
// 		if candleTag == pixelObjectTag {
// 			isPixelObjectAlreadyCalculated = true
// 			break
// 		}
// 		indexCandleTag += 1
// 	}
// if isPixelObjectAlreadyCalculated == false {
// 	structureObject := StructureObject{Classification: "red"}
// 	listStructureObject = append(listStructureObject, structureObject)
// 	//find pixelobject x index in y list, and add each value, indicating that the first option found is 0 index of available list

// 	//
// 	for indexContinuumObject, continuumObject := range structureContinuumsYDirectionRed {
// 		for indexFeatureSetIdentified, featureSetIdentified := range continuumObject.ListFeatureSetIdentified {
// 			for indexPixelObjectY, pixelObjectY := range featureSetIdentified.ListPixelObject {
// 				//
// 				//compare pixel objects from both lists.
// 				//
// 				if pixelObjectY.XValue == pixelObject.XValue {
// 					//for every value in y list, append and tag to calculated
// 					for indexPixelObjectInternal, pixelObjectInternal := range featureSetIdentified.ListPixelObject {
// 						tagInternal := pixelObjectInternal.XValue + " " + pixelObjectInternal.YValue
// 						//

// 						//check if in calculated
// 						isPixelObjectAlreadyCalculatedInternal := false
// 						for indexPixelCalculatedTag, pixelCalculatedTag := range listTagCalculatedPixelConnectedCandle {
// 							if pixelCalculatedTag == tagInternal {
// 								isPixelObjectAlreadyCalculatedInternal = true
// 								break
// 							}
// 							indexPixelCalculatedTag += 1
// 						}
// 						//
// 						// fmt.Println("pixelObject")
// 						// fmt.Println(pixelObject)
// 						// fmt.Println("pixelObjectY")
// 						// fmt.Println(pixelObjectY)
// 						// fmt.Println(featureSetIdentified.ListPixelObject)
// 						if isPixelObjectAlreadyCalculatedInternal == false {
// 							listTagCalculatedPixelConnectedCandle = append(listTagCalculatedPixelConnectedCandle, tagInternal)
// 							listStructureObject[len(listStructureObject)-1].ListPixelObject = append(listStructureObject[len(listStructureObject)-1].ListPixelObject, pixelObjectInternal)
// 						}
// 						indexPixelObjectInternal += 1
// 					}
// 				}
// 				//then break, already added rest of y list
// 				// break
// 				indexPixelObjectY += 1
// 			}
// 			indexFeatureSetIdentified += 1
// 		}
// 		indexContinuumObject += 1
// 	}
// }
// indexPixelObject += 1

// if indexPixelObject == 3 {
// 	break
// }
// break

// indexPixelObject += 1

// if len(listPixelObject) == 0 {
// }

// if index == 100 {
// 	break
// }
// if firstIndexReached {
// 	break
// }
// }
// listStructureObject := StructureObject{}

func behavioralCalculateGreenPixelsY(stringPixelRaw string) [][]PixelObject {
	listCommaResults := strings.Split(stringPixelRaw, ",")
	listPixelObjects := []PixelObject{}
	for index, v := range listCommaResults {
		valueParse := strings.Split(v, "_")
		xValue := valueParse[0]
		if index == len(listCommaResults)-1 {
			fmt.Println(v)
			break
		}
		pixelObject := PixelObject{XValue: xValue, YValue: valueParse[1], RedValue: valueParse[2], ColorationIdentification: "red"}
		listPixelObjects = append(listPixelObjects, pixelObject)
	}

	listGreenPixels := []PixelObject{}
	for i, v := range listPixelObjects {
		intColor := 0
		if s, err := strconv.Atoi(v.RedValue); err == nil {
			intColor = s
		}
		if intColor < 10 {
			listGreenPixels = append(listGreenPixels, v)
		}
		i += 1
	}

	//add pixels to appropriate pixel containers.
	xLim := 0
	for i, v := range listGreenPixels {
		currentValue := 0
		if s, err := strconv.Atoi(v.XValue); err == nil {
			currentValue = s
		}
		if i == 0 {
			xLim = currentValue
			continue
		}
		if currentValue > xLim {
			xLim = currentValue
		}
	}

	//coordinate x values into pixel value containers.
	listPixelObjectList := [][]PixelObject{}
	sum := 0
	for sum <= xLim {
		listPixelObjectList = append(listPixelObjectList, []PixelObject{})
		sum += 1
	}

	for indexPixel, pixelObject := range listGreenPixels {
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}

		listPixelObjectList[intXValue] = append(listPixelObjectList[intXValue], pixelObject)
		indexPixel += 1
	}
	returningMatrix := listPixelObjectList
	return returningMatrix
}

// for i, v := range listPixelObjects {
// 	tag := v.XValue + " " + v.YValue
// 	if tag == "2 258" {
// 		fmt.Println("oopsiedaisy")
// 		fmt.Println(v.RedValue)
// 		fmt.Println(v.XValue)
// 		fmt.Println(v.YValue)
// 	}
// 	i += 1
// }
func behavioralCalculateRedPixelsY(stringPixelRaw string) [][]PixelObject {
	listCommaResults := strings.Split(stringPixelRaw, ",")
	listPixelObjects := []PixelObject{}
	for index, v := range listCommaResults {
		valueParse := strings.Split(v, "_")
		xValue := valueParse[0]
		if index == len(listCommaResults)-1 {
			fmt.Println(v)
			break
		}
		pixelObject := PixelObject{XValue: xValue, YValue: valueParse[1], RedValue: valueParse[2], ColorationIdentification: "red"}
		listPixelObjects = append(listPixelObjects, pixelObject)
	}

	// for i, v := range listPixelObjects {
	// 	tag := v.XValue + " " + v.YValue
	// 	if tag == "2 258" {
	// 		fmt.Println("oopsiedaisy")
	// 		fmt.Println(v.RedValue)
	// 		fmt.Println(v.XValue)
	// 		fmt.Println(v.YValue)
	// 	}
	// 	i += 1
	// }
	listRedPixels := []PixelObject{}
	for i, v := range listPixelObjects {
		intColor := 0
		if s, err := strconv.Atoi(v.RedValue); err == nil {
			intColor = s
		}
		//red coloration
		if intColor > 200 {
			listRedPixels = append(listRedPixels, v)
		}
		i += 1
	}

	//add pixels to appropriate pixel containers.
	xLim := 0
	for i, v := range listRedPixels {
		currentValue := 0
		if s, err := strconv.Atoi(v.XValue); err == nil {
			currentValue = s
		}

		if i == 0 {
			xLim = currentValue
			continue
		}

		if currentValue > xLim {
			xLim = currentValue
		}
	}

	// fmt.Println("xLim red")
	// fmt.Println(xLim)

	//coordinate x values into pixel value containers.
	listPixelObjectList := [][]PixelObject{}
	sum := 0
	for sum <= xLim {
		listPixelObjectList = append(listPixelObjectList, []PixelObject{})
		sum += 1
	}

	// fmt.Println("len(listPixelObjectList)")
	// fmt.Println(len(listPixelObjectList))
	for indexPixel, pixelObject := range listRedPixels {
		//xValue to int
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}

		listPixelObjectList[intXValue] = append(listPixelObjectList[intXValue], pixelObject)
		indexPixel += 1
	}

	//
	// fmt.Println("listPixelObjectList[2]")
	// fmt.Println(listPixelObjectList[2])
	// //arrange each pixel in x container by y value descending.
	// sortedSectorList := [][]PixelObject{}
	// for indexContainer, pixelContainer := range listPixelObjectList {
	// 	sortedPixelList := sortPixelListLowToHigh(pixelContainer)
	// 	sortedSectorList = append(sortedSectorList, sortedPixelList)
	// 	indexContainer += 1
	// }

	returningMatrix := listPixelObjectList //sortedSectorList
	return returningMatrix
}

// tag := pixelObject.XValue + " " + pixelObject.YValue
// if tag == "2 258" {
// 	fmt.Println("oopsiedaisy")
// 	fmt.Println(pixelObject.RedValue)
// 	fmt.Println(pixelObject.XValue)
// 	fmt.Println(pixelObject.YValue)
// }

func calculateContinuumsYDirectionRed(sectoredPixelObjectMatrix [][]PixelObject) []ContinuumObject {
	listContinuumObjects := []ContinuumObject{ContinuumObject{ListFeatureSetIdentified: []FeatureSetIdentified{}}}
	sum := 0
	for sum < len(sectoredPixelObjectMatrix) {
		listContinuumObjects = append(listContinuumObjects, ContinuumObject{ListFeatureSetIdentified: []FeatureSetIdentified{}})
		sum += 1
	}

	for indexPixelObjectContainer, pixelObjectContainer := range sectoredPixelObjectMatrix {
		isContinuation := false
		for indexPixelObject, pixelObject := range pixelObjectContainer {
			colorValue := pixelObject.RedValue
			intColorValue := 0
			if s, err := strconv.Atoi(colorValue); err == nil {
				intColorValue = s
			}

			if isContinuation {
				if intColorValue < 210 {
					isContinuation = false
					continue
				}
			}

			if intColorValue > 210 {
				if isContinuation == false {
					listContinuumObjects[indexPixelObjectContainer].ListFeatureSetIdentified = append(listContinuumObjects[indexPixelObjectContainer].ListFeatureSetIdentified, FeatureSetIdentified{})
				}
				isContinuation = true
			}

			if isContinuation {
				listContinuumObjects[indexPixelObjectContainer].ListFeatureSetIdentified[len(listContinuumObjects[indexPixelObjectContainer].ListFeatureSetIdentified)-1].ListPixelObject = append(
					listContinuumObjects[indexPixelObjectContainer].ListFeatureSetIdentified[len(listContinuumObjects[indexPixelObjectContainer].ListFeatureSetIdentified)-1].ListPixelObject,
					pixelObject)
			}

			indexPixelObject += 1
		}
	}
	return listContinuumObjects
}

func behavioralCalculateGreenPixels(stringPixelRaw string) [][]PixelObject {

	listCommaResults := strings.Split(stringPixelRaw, ",")
	listPixelObjects := []PixelObject{}
	for index, v := range listCommaResults {
		valueParse := strings.Split(v, "_")
		xValue := valueParse[0]
		if index == len(listCommaResults)-1 {
			fmt.Println(v)
			break
		}
		pixelObject := PixelObject{XValue: xValue, YValue: valueParse[1], RedValue: valueParse[2], ColorationIdentification: "green"}
		listPixelObjects = append(listPixelObjects, pixelObject)
	}

	listGreenPixels := []PixelObject{}
	for i, v := range listPixelObjects {
		intColor := 0
		if s, err := strconv.Atoi(v.RedValue); err == nil {
			intColor = s
		}
		if intColor < 10 {
			listGreenPixels = append(listGreenPixels, v)
		}
		i += 1
	}

	//add pixels to appropriate pixel containers.
	highestXVal := 0
	for i, v := range listGreenPixels {
		currentValue := 0
		if s, err := strconv.Atoi(v.XValue); err == nil {
			currentValue = s
		}

		if i == 0 {
			highestXVal = currentValue
			continue
		}

		if currentValue > highestXVal {
			highestXVal = currentValue
		}
	}

	listMatriceXList := [][]PixelObject{}
	sum := 0
	for sum <= highestXVal {
		listMatriceXList = append(listMatriceXList, []PixelObject{})
		sum += 1
	}

	for indexPixel, pixelObject := range listGreenPixels {
		//xValue to int
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}
		listMatriceXList[intXValue] = append(listMatriceXList[intXValue], pixelObject)
		indexPixel += 1
	}

	// //arrange each pixel in x container by y value descending.
	sortedSectorList := [][]PixelObject{}
	for indexContainer, pixelContainer := range listMatriceXList {
		sortedPixelList := sortPixelListLowToHigh(pixelContainer)
		sortedSectorList = append(sortedSectorList, sortedPixelList)
		indexContainer += 1
	}
	returningMatrix := sortedSectorList
	// }
	return returningMatrix
}

func behavioralCalculateRedPixels(stringPixelRaw string) [][]PixelObject {
	listCommaResults := strings.Split(stringPixelRaw, ",")
	listPixelObjects := []PixelObject{}
	for index, v := range listCommaResults {
		valueParse := strings.Split(v, "_")
		xValue := valueParse[0]
		if index == len(listCommaResults)-1 {
			fmt.Println(v)
			break
		}
		pixelObject := PixelObject{XValue: xValue, YValue: valueParse[1], RedValue: valueParse[2], ColorationIdentification: "red"}
		listPixelObjects = append(listPixelObjects, pixelObject)
	}

	listRedPixels := []PixelObject{}
	for i, v := range listPixelObjects {
		intColor := 0
		if s, err := strconv.Atoi(v.RedValue); err == nil {
			intColor = s
		}
		//red coloration
		if intColor > 210 {
			listRedPixels = append(listRedPixels, v)
		}
		i += 1
	}

	//add pixels to appropriate pixel containers.
	highestXVal := 0
	for i, v := range listRedPixels {
		currentValue := 0
		if s, err := strconv.Atoi(v.XValue); err == nil {
			currentValue = s
		}

		if i == 0 {
			highestXVal = currentValue
			continue
		}

		if currentValue > highestXVal {
			highestXVal = currentValue
		}
	}

	fmt.Println("highestXVal red")
	fmt.Println(highestXVal)

	//coordinate x values into pixel value containers.
	listMatriceXList := [][]PixelObject{}
	sum := 0
	for sum <= highestXVal {
		listMatriceXList = append(listMatriceXList, []PixelObject{})
		sum += 1
	}

	// fmt.Println("len(listMatriceXList)")
	// fmt.Println(len(listMatriceXList))
	for indexPixel, pixelObject := range listRedPixels {
		//xValue to int
		intXValue := 0
		if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
			intXValue = s
		}
		listMatriceXList[intXValue] = append(listMatriceXList[intXValue], pixelObject)
		indexPixel += 1
	}

	//

	// //arrange each pixel in x container by y value descending.
	sortedSectorList := [][]PixelObject{}
	for indexContainer, pixelContainer := range listMatriceXList {
		sortedPixelList := sortPixelListLowToHigh(pixelContainer)
		sortedSectorList = append(sortedSectorList, sortedPixelList)
		indexContainer += 1
	}

	returningMatrix := sortedSectorList
	return returningMatrix
}

// fmt.Println("listDarkPixelObjects")
// fmt.Println(len(listDarkPixelObjects))

// darkResult := strings.Split(dataObjectPixelsRawString, ",;")[0]
// lightResult := strings.Split(dataObjectPixelsRawString, ",;")[1]

// listDarkCommaResults := strings.Split(darkResult, ",")
// listLightCommaResults := strings.Split(lightResult, ",")

// listDarkPixelObjects := []PixelObject{}
// listLightPixelObjects := []PixelObject{}

// for i, v := range listDarkCommaResults {
// 	valueParse := strings.Split(v, "_")
// 	xValue := valueParse[0]
// 	if i == 0 {
// 		xValue = strings.Split(valueParse[0], " ")[1]
// 	}

// 	pixelObject := PixelObject{XValue: xValue, YValue: valueParse[1], RedValue: valueParse[2], ColorationIdentification: "Dark"}
// 	listDarkPixelObjects = append(listDarkPixelObjects, pixelObject)

// }

// for i, v := range listLightCommaResults {
// 	valueParse := strings.Split(v, "_")

// 	xValue := valueParse[0]
// 	if i == 0 {
// 		xValue = strings.Split(valueParse[0], " ")[1]
// 	}

// 	pixelObject := PixelObject{XValue: xValue, YValue: valueParse[1], RedValue: valueParse[2], ColorationIdentification: "Light"}

// 	listLightPixelObjects = append(listLightPixelObjects, pixelObject)
// 	i += 1
// }
// // //
// fmt.Println("len(listDarkPixelObjects)")
// fmt.Println(len(listDarkPixelObjects))

// listAllPixelObjects := []PixelObject{}
// for i, v := range listDarkPixelObjects {
// 	listAllPixelObjects = append(listAllPixelObjects, v)
// 	i += 1
// }
// for i, v := range listLightPixelObjects {
// 	listAllPixelObjects = append(listAllPixelObjects, v)
// 	i += 1
// }
// sectoredPixelList := listAllPixelObjects
// //place in categories by x
// //highest x values, populate containers then fill by xvalues, append pixels.
// highestXVal := 0
// for i, v := range sectoredPixelList {
// 	currentValue := 0
// 	if s, err := strconv.Atoi(v.XValue); err == nil {
// 		currentValue = s
// 	}

// 	if i == 0 {
// 		highestXVal = currentValue
// 		continue
// 	}

// 	if currentValue > highestXVal {
// 		highestXVal = currentValue
// 	}
// }

// //coordinate x values into pixel value containers.
// listMatriceXList := [][]PixelObject{}
// sum := 0
// for sum <= highestXVal {
// 	sum += 1
// 	listMatriceXList = append(listMatriceXList, []PixelObject{})
// }

// for indexPixel, pixelObject := range sectoredPixelList {
// 	//xValue to int
// 	intXValue := 0
// 	if s, err := strconv.Atoi(pixelObject.XValue); err == nil {
// 		intXValue = s
// 	}
// 	listMatriceXList[intXValue] = append(listMatriceXList[intXValue], pixelObject)
// 	indexPixel += 1
// }

//

// activeMemoryObject := calculationDeliverableMatrix.LivingDataMatrixComposite.ListLivingDataMatrix[0]
// metaMemoryObject := activeMemoryObject.MemoryComposite.ListMemoryLine[0].ListMemoryShift[0].ListMemoryLink[0].ListMemoryObject[0].ListMetaMemoryObject[0]

// dataObjectLinkList := metaMemoryObject.ListMemoryComposite[0].ListMemoryLine[0].ListMemoryShift[0].ListMemoryLink
// dataObjectOption := dataObjectLinkList[3].ListMemoryObject[0].ListDataString[0]

// // dataObjectPixelCoordinates := dataObjectLinkList[2].ListMemoryObject[0].ListDataString

// dataObjectPixelsRawString := dataObjectLinkList[1].ListMemoryObject[0].ListDataString[0]

// returningMatrix := [][]PixelObject{}
// if dataObjectOption == "option1" {
// work data, parse pixels into objects

// x1 := dataObjectPixelCoordinates[0]
// y1 := dataObjectPixelCoordinates[1]

// x2 := dataObjectPixelCoordinates[2]
// y2 := dataObjectPixelCoordinates[3]

// // filter values by coordinates
// sectoredPixelList := []PixelObject{}
// //sector pixels if x and y values within range of created square from x1 to x2, and y1 to y2
// for indexPixel, pixel := range listAllPixelObjects {
// 	if pixel.XValue >= x1 && pixel.XValue <= x2 {
// 		if pixel.YValue >= y1 && pixel.YValue <= y2 {
// 			sectoredPixelList = append(sectoredPixelList, pixel)
// 		}
// 	}
// 	indexPixel += 1
// }

func sortPixelListLowToHigh(pixelObjectList []PixelObject) []PixelObject {
	updatedList := []PixelObject{}
	indexNumberSeeking := 0
	for indexNumberSeeking < len(pixelObjectList) {
		if indexNumberSeeking == 0 {
			// previousValue
			//have previous value index and value if less or greater.
			// append to new list
			updatedList = append(updatedList, pixelObjectList[0])
			indexNumberSeeking += 1
			continue
		}
		if indexNumberSeeking == 1 {
			pixel1Value := 0
			pixel2Value := 0
			if s, err := strconv.Atoi(pixelObjectList[1].XValue); err == nil {
				pixel1Value = s
			}
			if s, err := strconv.Atoi(pixelObjectList[0].XValue); err == nil {
				pixel2Value = s
			}
			//covert to int value
			if pixel1Value > pixel2Value {
				updatedList = append(updatedList, pixelObjectList[1])
				indexNumberSeeking += 1
				continue
			}
			//covert to int value
			if pixel1Value < pixel2Value {
				//do shift
				updatedList = updateListAtInsertIndex(updatedList, 0, pixelObjectList[1])
				indexNumberSeeking += 1
				continue
			}
		}

		//

		if indexNumberSeeking > 1 {
			seekingReversalIndex := indexNumberSeeking - 1
			for seekingReversalIndex >= 0 {

				pixel1Value := 0
				pixel2Value := 0
				if s, err := strconv.Atoi(pixelObjectList[indexNumberSeeking].XValue); err == nil {
					pixel1Value = s
				}
				if s, err := strconv.Atoi(updatedList[seekingReversalIndex].XValue); err == nil {
					pixel2Value = s
				}

				//covert to int value
				if pixel1Value > pixel2Value {
					if seekingReversalIndex+1 == len(updatedList) {
						updatedList = append(updatedList, pixelObjectList[indexNumberSeeking])
						break
					}
					updatedList = updateListAtInsertIndex(updatedList, seekingReversalIndex+1, pixelObjectList[indexNumberSeeking])
					break
				}

				if seekingReversalIndex == 0 {
					updatedList = updateListAtInsertIndex(updatedList, 0, pixelObjectList[indexNumberSeeking])
					break
				}
				seekingReversalIndex -= 1
			}
			indexNumberSeeking += 1
		}
	}
	return updatedList
}

func updateListAtInsertIndex(listToUpdate []PixelObject, indexToUpdateAtIteration int, pixelObjectToInsert PixelObject) []PixelObject {
	//cannot append at last index, only exception need toappend value normally for last index.
	// instanceListToUpdate := listToUpdate
	updatedList := []PixelObject{}

	for i, v := range listToUpdate {
		updatedList = append(updatedList, v)
		i += 1
	}

	updatedList = append(updatedList, PixelObject{YValue: "0"})
	// fmt.Println("instanceListToUpdate")
	// fmt.Println(instanceListToUpdate)

	// fmt.Println("updatedList")
	// fmt.Println(updatedList)
	for i, v := range listToUpdate {
		indexToUpdate := i + 1
		// fmt.Println("v")
		// fmt.Println(v)
		if i >= indexToUpdateAtIteration {
			if i == indexToUpdateAtIteration {
				updatedList[i] = pixelObjectToInsert
			}
			updatedList[indexToUpdate] = v
			// break
		}
	}
	// fmt.Println("instanceListToUpdate")
	// fmt.Println(instanceListToUpdate)
	// fmt.Println("updatedList")
	// fmt.Println(updatedList)
	return updatedList
}
